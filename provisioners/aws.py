"""AWS provisioner."""
import collections
import dataclasses
from functools import cached_property
import os
import subprocess
import typing

import boto3
import botocore.exceptions
from cki_lib import misc
from ruamel.yaml.scalarstring import PreservedScalarString
import yaml

from plumbing.interface import ProvisionerCore
from restraint_wrap.restraint_file.job_element import RestraintJob
from upt.const import EC2_INSTANCE_RUNNING
from upt.logger import LOGGER
from upt.misc import fixup_or_delete_tasks_without_fetch

OverrideVariable = collections.namedtuple('OverrideVariable', 'key env_name')


OVERRIDE_VARIABLES = [
    OverrideVariable('LaunchTemplate/LaunchTemplateName', 'AWS_UPT_LAUNCH_TEMPLATE_NAME'),
    OverrideVariable('ImageId', 'AWS_UPT_IMAGE_ID'),
    OverrideVariable('InstanceType', 'AWS_UPT_INSTANCE_TYPE'),
]


@dataclasses.dataclass
class MachineImage:
    """Represents an AMI."""

    image_id: str
    instance_type: typing.Optional[str]


class AWS(ProvisionerCore):
    """AWS provisioner."""

    session = boto3.Session(aws_access_key_id=os.environ.get('AWS_UPT_ACCESS_KEY_ID'),
                            aws_secret_access_key=os.environ.get('AWS_UPT_SECRET_ACCESS_KEY'))

    @cached_property
    def ec2(self):
        # pylint: disable=no-self-use
        """Get AWS's session ec2."""
        return AWS.session.resource('ec2')

    def hosts(self):
        """Iterate through all AWS hosts."""
        for resource_group in self.rgs:
            for host in resource_group.hosts():
                yield host

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        self.keycheck = kwargs.get('keycheck')

        for resource_group in self.rgs:
            # We currently don't have a use for resource_id with AWS, so just use an increasing unique number.
            resource_group.resource_id = str(self.resource_id_monotonic.get())
            recipe_set = resource_group.recipeset
            restraint_job = RestraintJob.create_from_string(recipe_set.restraint_xml)
            restraint_recipes = restraint_job.get_all_recipes()

            assert len(restraint_recipes) == len(
                recipe_set.hosts), "Invalid data; must have 1 host per 1 recipe"

            for j, host in enumerate(recipe_set.hosts):
                instance_id = host.misc.get('instance_id')
                if not instance_id:
                    host.recipe_id = self.host_recipe_id_monotonic.get()
                    host.instance = self.provision_host(host)[0]
                    host.misc['instance_id'] = host.instance.id
                else:
                    host.instance = self.ec2.Instance(instance_id)

                restraint_recipes[j].id = str(host.recipe_id)

            fixup_or_delete_tasks_without_fetch(restraint_job)
            recipe_set.restraint_xml = PreservedScalarString(str(restraint_job))

    def find_image(
        self,
        owner: typing.Optional[str],
        architecture_filter: typing.Optional[str],
        name_filter: typing.Optional[str],
        architecture_mapping: dict[str, str],
    ) -> MachineImage:
        """Use DescribeImages to lookup the newest image matching a certain owner/name."""
        filters = []
        if architecture_filter:
            filters.append({'Name': 'architecture', 'Values': [architecture_filter]})
        if name_filter:
            filters.append({'Name': 'name', 'Values': [name_filter]})
        image = sorted(self.session.client('ec2').describe_images(
            Filters=filters, Owners=[owner] if owner else [],
        )['Images'], key=lambda i: i['CreationDate'], reverse=True)[0]
        LOGGER.info('Using image %s', image['Name'])
        return MachineImage(image['ImageId'], architecture_mapping.get(image['Architecture']))

    def provision_host(self, host):
        """Provision a single host."""
        kwargs = {
            'TagSpecifications': [
                {
                    'ResourceType': 'instance',
                    'Tags': [
                        {'Key': 'Name',
                         'Value': f'{os.environ["AWS_UPT_INSTANCE_PREFIX"]}.{host.recipe_id}'},
                        {'Key': 'CkiGitLabPipelineId',
                         'Value': os.environ.get('CI_PIPELINE_ID', '')},
                        {'Key': 'CkiGitLabJobId',
                         'Value': os.environ.get('CI_JOB_ID', '')},
                    ]
                }
            ],
            'MaxCount': 1,
            'MinCount': 1,
        }
        owner = os.environ.get('AWS_UPT_IMAGE_OWNER')
        architecture_filter = os.environ.get('AWS_UPT_IMAGE_ARCHITECTURE_FILTER')
        name_filter = os.environ.get('AWS_UPT_IMAGE_NAME_FILTER')
        architecture_mapping = yaml.safe_load(
            os.environ.get('AWS_UPT_IMAGE_ARCHITECTURE_MAPPING', '{}'))
        if owner or architecture_filter or name_filter:
            image = self.find_image(owner, architecture_filter, name_filter, architecture_mapping)
            kwargs['ImageId'] = image.image_id
            if image.instance_type:
                kwargs['InstanceType'] = image.instance_type

        if subnet_id := os.environ.get('AWS_UPT_NETWORK_SUBNET_ID'):
            kwargs['NetworkInterfaces'] = [{
                'DeviceIndex': 0,
                'SubnetId': subnet_id,
                'Groups': os.environ['AWS_UPT_NETWORK_SECURITY_GROUP_IDS'].split()
            }]

        for variable in OVERRIDE_VARIABLES:
            if value := os.environ.get(variable.env_name):
                misc.set_nested_key(kwargs, variable.key, value)

        return self.ec2.create_instances(**kwargs)

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        provisioned, _ = self.get_provisioning_state(resource_group)
        return provisioned

    def get_provisioning_state(self, resource_group):
        """Get current state of provisioning.

        NOTE: this method is not a required part of the provisioner interface.
        Each provisioner implements and uses this method in its own way,
        although the method signature is very similar each time.
        """
        provisioned = True
        erred = []

        for host in resource_group.hosts():
            if not host.instance:
                host.instance = self.ec2.Instance(host.misc.get('instance_id'))
            try:
                host.instance.reload()
            # should be resolved by eventual consistency
            except botocore.exceptions.ClientError:
                provisioned = False
                LOGGER.debug('Waiting for host %s to be created', host.instance.id)
                continue

            if host.instance.state['Code'] < EC2_INSTANCE_RUNNING:
                provisioned = False
                LOGGER.debug('Waiting for host %s to reach running state', host.instance.id)
                continue

            if host.instance.state['Code'] > EC2_INSTANCE_RUNNING:
                provisioned = False
                LOGGER.debug('Host %s failed to read running state', host.instance.id)
                erred.append(host)
                continue

            # check if machine is reachable via ssh
            if not host.reachable_via_ssh:
                process = subprocess.run([
                    'ssh',
                    '-o', 'PubkeyAcceptedKeyTypes=+ssh-rsa',
                    '-o', f'StrictHostKeyChecking={self.keycheck}',
                    f'root@{host.instance.public_dns_name or host.instance.private_ip_address}',
                    '[', '$(whoami)', '=', 'root', ']'
                ], capture_output=True, check=False)
                if process.stderr.strip():
                    LOGGER.debug("stderr: %s", process.stderr)
                if process.stdout.strip():
                    LOGGER.debug("stdout: %s", process.stdout)
                if process.returncode:
                    LOGGER.debug('Waiting for host %s to finish UserData script', host.instance.id)
                    provisioned = False
                else:
                    host.reachable_via_ssh = True

        return provisioned, erred

    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed."""
        for host in resource_group.hosts():
            if host.instance.state['Code'] > EC2_INSTANCE_RUNNING:
                resource_group.erred_rset_ids.add(host.misc.get('instance_id'))
                host.instance.terminate()
                del host.instance
                host.reachable_via_ssh = False
                # We just removed an instance that is somehow broken (hopefully equivalent of Beaker "aborted"), so
                # we will provision it again.
                host.instance = self.provision_host(host)

    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""
        for host in resource_group.hosts():
            if not host.instance:
                host.instance = self.ec2.Instance(host.misc.get('instance_id'))
            host.instance.terminate()

    def heartbeat(self, resource_group, recipe_ids_dead):
        """
        Check if resource is OK (provisioned, not broken/aborted).

        Add all not OK recipe_id's to recipe_ids_dead
        """
        _, erred = self.get_provisioning_state(resource_group)
        # no op if empty set
        recipe_ids_dead |= {host.recipe_id for host in erred}
        # AWS has no concept of EWD, however, if the host becomes unresponsive, we can still apply the same actions
        # to it as if it suffered EWD.

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """
        for host in resource_group.hosts():
            host.hostname = host.instance.public_dns_name or host.instance.private_ip_address

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        return [host.misc.get('instance_id') for host in self.hosts() if host.misc.get('instance_id')]
