#!/usr/bin/env python3
"""Main entry point, command-line interface for upt."""
import argparse

from cki_lib.misc import sentry_init
import sentry_sdk

from upt import misc

from . import cmd_test


def main():
    # pylint: disable=no-value-for-parameter
    """Do all."""
    sentry_init(sentry_sdk)
    common_parser = argparse.ArgumentParser(add_help=False)
    parser = argparse.ArgumentParser(description="Run test using restraint's standalone mode.", add_help=True)
    cmds_parser = parser.add_subparsers(title="Command", dest="command")
    cmd_test.build(cmds_parser, common_parser)

    args = parser.parse_args()

    if args.command is not None:
        misc.setup_args(args)

    commands = {
        'help': [parser.print_help],
        'test': [cmd_test.main, args],
    }

    command_name = args.command if args.command is not None else "help"
    function, *function_args = commands[command_name]
    function(*function_args)


if __name__ == '__main__':
    main()
