"""Misc utility methods."""
from datetime import datetime
import os
import pathlib
import typing
from urllib.parse import quote

from twisted.internet import error


def add_directory_suffix(output, instance_no):
    """Add numerical .done.XY suffix to directory."""
    dirname = f'{output}.done.{instance_no:02d}'
    assert not os.path.isdir(dirname)
    return dirname


def attempt_reactor_stop(reactor):
    """If twisted reactor is running, try to stop it."""
    if reactor.running:
        try:
            reactor.callFromThread(reactor.stop)
        except error.ReactorNotRunning:
            pass


def attempt_heartbeat_stop(heartbeat_loop):
    """Attempt to stop heartbeat looping call."""
    try:
        heartbeat_loop.stop()
    except (AssertionError,  AttributeError):
        pass


def convert_path_to_link(path: typing.Union[str, pathlib.Path], is_file: bool) -> str:
    """Get a quoted url to path, or just path if we're not running in pipeline."""
    if prefix := os.environ.get(f'UPT_ARTIFACT_{"FILE" if is_file else "DIRECTORY"}_URL_PREFIX'):
        return prefix + quote(str(path))
    return str(path)


def get_most_recent_task_result(recipe_id, task_id, task_results):
    """Get the most recent task state transition."""
    for task_result in reversed(task_results):
        if task_result.task_id == task_id and task_result.recipe_id == recipe_id:
            return task_result
    return None


def get_first_start_time(recipe_id, task_id, task_results):
    """Find start_time (1st TaskResult with recipe_id:task_id) in task_results list, or use current date & time."""
    try:
        return next(filter(lambda task_result: task_result.recipe_id == recipe_id and task_result.task_id ==
                           task_id and task_result.start_time, task_results)).start_time
    except StopIteration:
        return f'{datetime.utcnow()}+00:00'.replace(' ', 'T', 1)
