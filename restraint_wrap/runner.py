#!/usr/bin/env python3
"""Run tests using restraint in standalone mode."""
import glob
import itertools
import os
import pathlib

from cki.kcidb.utils import upload_file
from cki_lib.kcidb.file import KCIDBFile
from twisted.internet import reactor
from twisted.internet import task

from plumbing.format import ProvisionData
from plumbing.interface import ProvisionerCore
from provisioners.beaker import Beaker
from restraint_wrap.misc import attempt_reactor_stop
from restraint_wrap.misc import convert_path_to_link
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.testplan import TestPlan
from upt.logger import LOGGER


class Runner:
    # pylint: disable=too-many-instance-attributes,no-member
    """Manage restraint protocols that process restraint output."""

    def __init__(self, provisioners, **kwargs):
        """Create the object."""
        # cmd-line arguments
        self.kwargs = kwargs
        # path to output directory
        self.output = kwargs['output']
        # number of reruns a task can have
        self.reruns = kwargs['reruns']
        # If True, output kcidb data.
        self.dump = kwargs['dump']

        # All twisted restraint client protocols
        self.protocols = []
        # Deserialize yaml, create provisioners, prepare them for use and add their protocols.
        self.provisioners = ProvisionData.prepare(provisioners, self.add_protocol,
                                                  self.on_task_result, **kwargs)
        # Build and maintain a plan of tests to run using different providers (provisioners).
        self.testplan = TestPlan(self.provisioners, **kwargs)

        # was cleanup handler completed?
        self.cleanup_done = False

    def on_task_result(self, resource_group, task_result, run_suffix, fname):
        """Call when a result of a task is ready."""
        self.testplan.on_task_result(resource_group, task_result, run_suffix, fname)

    def start_protocols(self):
        """Start restraint protocols one by one."""
        for proto in self.protocols:
            if not proto.proc:
                LOGGER.debug('Starting protocol for resource_id %s...', proto.resource_group.resource_id)
                # Create & start protocol's processes, register cleanup handlers
                # and let user know.
                proto.start_all()

                # Start one by one
                break
            if proto.proc and not proto.run_suffix:
                # Don't start until directory is created
                break

    def wait_on_protocols(self):
        """Wait for hosts to finish; protocols set flags in hosts."""
        self.start_protocols()

        all_rgs = itertools.chain(*[provisioner.rgs for provisioner in self.provisioners])
        if ProvisionerCore.all_recipes_finished(all_rgs) and self.protocols:
            # If processing of all hosts is done, then stop reactor, allowing
            # the script to exit.
            attempt_reactor_stop(reactor)

    def add_protocol(self, provisioner, resource_group, adapter, **kwargs):
        """Add a restraint client wrapper (twisted protocol)."""
        # Ensure hosts stay up for expected time
        for host in provisioner.find_objects([resource_group], lambda obj: obj if hasattr(obj,
                                                                                          'recipe_id') else None):
            if provisioner.set_reservation_duration(host):
                LOGGER.error('Failed to set reservation for %s', host.recipe_id)

        # restraint client protocol and process
        proto = RestraintClientProcessProtocol(provisioner, resource_group, self.add_protocol, adapter, **kwargs)

        # Add to the list of protocols.
        self.protocols.append(proto)

        return proto

    def run(self):
        # pylint: disable=no-member
        """Run restraint client shell cmd."""
        reactor.addSystemEventTrigger('before', 'shutdown',
                                      self.cleanup_handler)

        LOGGER.debug('Runner waiting for processes to finish...')

        if self.dump:
            self.testplan.dump_testplan()

        protocols_wait_loop = task.LoopingCall(self.wait_on_protocols)
        protocols_defer = protocols_wait_loop.start(5.0)
        protocols_defer.addErrback(self.handle_runner_failure)
        protocols_defer.addCallback(self.handle_runner_failure)
        reactor.run()

        # Some provisioners may have separate console logs to download.
        rc_data = self.kwargs['rc_data']
        # If there's no rc file, the console logs will not be sanitized.
        kernel_version = rc_data.state.kernel_version if rc_data else None
        downloaded_files = self.download_logs(self.provisioners, self.output, kernel_version)

        if downloaded_files:
            name_url_output_files = self.prepare_logs(
                self.testplan.adapter, downloaded_files, upload=self.kwargs['upload']
            )
            self.update_kcidb_dumpfiles(self.output, name_url_output_files)

    @classmethod
    def update_kcidb_dumpfiles(cls, output, results):
        """Update all json dumpfiles with urls to system/console logfiles."""
        for ffile in glob.glob(f'{output}/results_*/*.json'):
            host_counter = str(pathlib.Path(ffile).parent.relative_to(output))
            # Add the same set of references to each file.
            name_url_output_files = [x[1] for x in results if x[0].rstrip('/') == host_counter]

            kcidb_file = KCIDBFile(ffile)
            for test in kcidb_file.data.get('tests', []):
                test['output_files'] = test.get('output_files', []) + name_url_output_files
            kcidb_file.save()

    @classmethod
    def prepare_logs(cls, adapter, host_fpath_tups, upload=False):
        """Upload system logs to s3 or link from gitlab artifacts."""
        # dict objects with 'name' and 'url' keys to add to kcidb dumpfiles.
        results = []
        for host_counter, fpath in host_fpath_tups:
            relative_fpath = fpath.relative_to(adapter.output)
            if upload:
                upload_destination = pathlib.Path(adapter.artifacts_path, str(adapter.instance_no),
                                                  relative_fpath)
                url = upload_file(adapter.visibility, upload_destination, fpath.name,
                                  source_path=str(fpath))
            else:
                url = convert_path_to_link(fpath, True)

            output_file = {'name': fpath.name, 'url': url}
            results.append((host_counter, output_file))

        return results

    @classmethod
    def download_logs(cls, provisioners, output, kernel_version):
        """Download Beaker logs."""
        downloaded_files = []
        for prov in provisioners:
            # Beaker: download console logs!
            if isinstance(prov, Beaker):
                for resource_group in prov.rgs:
                    downloaded_files += prov.download_logs(output, resource_group.recipeset.hosts,
                                                           kernel_version)
        return downloaded_files

    @classmethod
    def handle_runner_failure(cls, failure):
        """Print failure that occured in runner."""
        LOGGER.error('FAILURE in deferred: %s', failure)

    def detect_abnormal_exit(self):
        """Detect abnormal exit in primary instance and ensure termination."""
        # It is highly suspicious when cleanup_handler runs and the hosts
        # aren't marked as "done_processing" yet.
        abnormal_exit = False
        for provisioner in self.provisioners:
            for host in provisioner.get_all_hosts(provisioner.rgs):
                # Force processing to end
                if not host.done_processing:
                    host.done_processing = True
                    abnormal_exit = True

        if abnormal_exit:
            print('* Abnormal exit!')

    @staticmethod
    def part_fname(fname):
        """Key files as (resource_id, recipe_id, task_id)."""
        # resource_id, attempt, _, recipe_id, _, task_id
        _, resource_id, _, _, recipe_id, _, task_id = fname.split('_')[:7]
        return (resource_id, recipe_id, task_id)

    @staticmethod
    def part_fdir(fdir_name):
        """Key dirs as (resource_id, recipe_id, task_id)."""
        # LOGS|RESULTS, resource_id, attempt, _, recipe_id, _, task_id
        _, resource_id, _, _, recipe_id, _, task_id = fdir_name.split('_')[:7]
        return (resource_id, recipe_id, task_id)

    def cleanup_handler(self):
        """Do cleanup and evaluate retcodes from hosts."""
        if self.cleanup_done:
            return

        LOGGER.debug('* Runner cleanup handler runs...')

        # Detect abnormal exit and print if it occurred.
        self.detect_abnormal_exit()

        self.cleanup_done = True

    def do_main(self):
        """Do all."""
        os.makedirs(self.output, exist_ok=True)

        self.run()
