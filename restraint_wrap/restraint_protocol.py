"""Restraint client protocol; wrapper around restraint client binary."""
import glob
import os
import pathlib
import shlex
import shutil
import traceback
import xml.etree.ElementTree as ET

from cki_lib import misc
import lxml.etree
from twisted.internet import defer
from twisted.internet import error
from twisted.internet import protocol
from twisted.internet import reactor
from twisted.internet import task as twisted_task

from plumbing.interface import ProvisionerCore
from restraint_wrap import const as restraint_const
from restraint_wrap.actions import ActionOnResult
from restraint_wrap.misc import attempt_heartbeat_stop
from restraint_wrap.misc import attempt_reactor_stop
from restraint_wrap.misc import convert_path_to_link
from restraint_wrap.misc import get_most_recent_task_result
from restraint_wrap.restraint_file.job_element import RestraintJob
from restraint_wrap.restraint_host import RestraintHost
from restraint_wrap.restraint_shell import ShellWrap
from restraint_wrap.task_result import TaskResult
from restraint_wrap.watcher import Watcher
from upt import const
from upt.logger import COLORS
from upt.logger import LOGGER
from upt.logger import colorize

# Disable invalid warning related to twisted.
# pylint: disable=no-member


class RestraintClientProcessProtocol(protocol.ProcessProtocol):
    # pylint: disable=too-many-instance-attributes,too-many-locals,too-many-arguments,too-many-public-methods
    """Restraint binary processor."""

    def __init__(self, provisioner, resource_group, add_protocol, on_task_result, **kwargs):
        """Create the object."""
        self.restraint_binary = 'restraint'
        self.provisioner = provisioner  # type: ProvisionerCore
        self.resource_group = resource_group

        self.add_protocol = add_protocol

        self.kwargs = kwargs
        self.dump = kwargs['dump']
        # Global output directory.
        self.output = kwargs['output']

        # Optional method to call when a task has finished executing.
        self.on_task_result = on_task_result

        # Subdirectory created by restraint client, e.g './job.01'
        self.run_suffix = None

        self.cleanup_done = False

        # Generate shell commands and possibly ssh aliases for restraint
        # client. These are to be kept in a temporary directory, to allow
        # inspection and debugging.
        self.wrap = ShellWrap(resource_group, **self.kwargs)

        # Here are hosts that we're processing input from.
        # Load expected hosts to improve parsing of restraint output
        self.rst_hosts = RestraintHost.from_line(self.wrap.restraint_commands)

        # craft output file prefix
        self.xml_name = const.RSTRNT_JOB_XML.replace('.xml', '')

        # How many times can we re-run a task
        self.reruns = kwargs['reruns']

        # This watches subtask results.
        self.watcher = Watcher(self.resource_group)

        self.sem_lwd_sync = self.watcher.sem_lwd_sync

        self.recipe_ids_dead = set()

        # A set of tuples (recipe_id, task_id) of lwd hits that weren't
        # processed
        self.unprocessed_lwd_hits = set()
        # A set of tuples (recipe_id, task_id, testname, result, score) of
        # subtask results that were not processed
        self.unprocessed_subtask_results = set()
        self.result_actions = self._prepare_actions()

        # twisted restraint client process
        self.proc = None
        self.heartbeat_loop = None

        # Tuple of (src, dst) filenames that were copied to result directories.
        self.all_copied_files = []

        # The last task result that was processed, may belong to any host in resource group.
        self.last_task_result = None

        # Save yet unprocessed restraint output until we receive a full line
        self.unprocessed_out_data = ''

    def host_heartbeat(self):
        """Possibly stop processing on all hosts that failed heartbeat."""
        # Get a list of hostnames that provisioner thinks are "dead".
        self.provisioner.heartbeat(self.resource_group, self.recipe_ids_dead)

        for host in self.resource_group.recipeset.hosts:
            if host.recipe_id in self.recipe_ids_dead or host.panicked:
                LOGGER.info('* Marking %i as dead -> restraint client process will end', host.recipe_id)
                # We only care about setting this to hosts that aren't finished processing. If the host is done
                # processing, no changes will be made.
                self.evaluate_host_abort(host)

                self.handle_failure()
                attempt_heartbeat_stop(self.heartbeat_loop)

        return self.recipe_ids_dead

    def outReceived(self, data):
        """Process incoming stdout data from restraint client."""
        data = data.decode('utf-8')
        LOGGER.debug(data)

        if not self.heartbeat_loop:
            self.heartbeat_loop = twisted_task.LoopingCall(self.host_heartbeat)
            self.heartbeat_loop.start(60.0)

        # run heartbeat check on connectivity issues
        defer_heartbeat = defer.Deferred()
        defer_heartbeat.addErrback(self.handle_failure)
        defer_heartbeat.callback(data)

        self.unprocessed_out_data += data

        lines = []
        if self.unprocessed_out_data.endswith('\n'):
            # We have full line(s), nothing is left over
            lines = self.unprocessed_out_data.splitlines()
            self.unprocessed_out_data = ''
        else:
            # The last part is not a full line and we need to keep it around
            try:
                last_newline = self.unprocessed_out_data.rindex('\n')
            except ValueError:
                # Not a single finished line, nothing to do
                return

            lines = self.unprocessed_out_data[:last_newline].splitlines()
            self.unprocessed_out_data = self.unprocessed_out_data[last_newline + 1:]

        for line in lines:
            # Handle a case when a host becomes unresponsive
            match = RestraintHost.rgx_disconnect.match(line)

            # Last ssh connection retry implies issues => do heartbeat, if heartbeat fails,
            # evaluate last task as if it aborted using actions in _prepare_actions that codify
            # rules used to evaluate results.
            if match and match.group(1) == match.group(2):
                self.host_heartbeat()
            else:
                self.find_host_lines(line)

    def handle_failure(self, failure=None):
        """Log exception and terminate restraint client."""
        # pylint: disable=no-member
        if failure:
            LOGGER.warning("Fatal exception caught %s", failure.getTraceback())
        else:
            LOGGER.warning('Host died, killing restraint process!')
        # Don't stop reactor, that would stop all the instances. Terminate the
        # restraint client process, if it isn't already.
        try:
            self.proc.signalProcess('TERM')
        except error.ProcessExitedAlready:
            pass

    def _check_stderr(self, data):
        """Check restraint client stderr messages for error indications."""
        result = restraint_const.RGX_LAB_WATCHDOG.findall(data)
        for hit_recipe_id in result:
            LOGGER.print_result_in_color(f'*** Recipe {hit_recipe_id} hit lab watchdog!')
            self.recipe_ids_dead.add(int(hit_recipe_id))
            # Find the host object that hit lab watchdog.
            host = self.provisioner.find_host_object([self.resource_group], int(hit_recipe_id))
            self.evaluate_host_abort(host)
            return

        for err in restraint_const.ERROR_INDICES:
            if err.findall(data):
                LOGGER.error("Exiting because: %s", data)

                attempt_reactor_stop(reactor)

    def errReceived(self, data):
        """Process incoming stderr data from restraint client."""
        data = data.decode('utf-8')
        if data.strip():
            # Log all stderr data
            LOGGER.debug(data.strip())

        defer_stderr_handling = defer.Deferred()
        defer_stderr_handling.addCallback(self._check_stderr)
        defer_stderr_handling.addErrback(self.handle_failure)
        # check stderr output for fatal errors
        defer_stderr_handling.callback(data)

    def identify_host(self, line):
        """Identify host output in restraint client."""
        match_result = RestraintHost.rgx_host.fullmatch(line)
        if match_result:
            self.rst_hosts.append(RestraintHost(*match_result.groups()))
            return True
        return False

    def filter_output(self, line):
        """Get an output line that belongs to a certain hostname."""
        # look for starting lines about hostname/recipe_ids
        if self.identify_host(line):
            # "Connecting to" line; nothing else do be done
            return None

        new_run = restraint_const.RGX_NEW_RUN.match(line)
        if new_run:
            self.run_suffix = new_run.group(1)

            path2watch = os.path.join(f'{os.path.abspath(self.kwargs["output"])}',
                                      os.path.relpath(self.run_suffix))
            # Start watcher with exact path
            reactor.callInThread(self.watcher.run, path2watch)

        # try to figure out to which host the output line belongs
        match_result = RestraintHost.rgx_host_line.findall(line)
        if not match_result:
            # one more line to filter out: info about where output is saved
            if 'Disconnected..' in line:
                LOGGER.info(line.strip())
                return None
            if 'for job run' in line:
                # Debug-logged by outReceived.
                return None

            # unknown output
            LOGGER.info('!!! Received unknown output "%s"', line)
            return None

        return match_result

    def split_hostline_match(self, host, hostline):
        """Parse a line from a hostname."""
        idx = RestraintHost.in_list(host, self.rst_hosts)
        if idx is not None:
            hostobj = self.rst_hosts[idx]  # type: RestraintHost
            hostobj.lines.append(hostline)
            try:
                regex2use = RestraintHost.rgx_state_change
                task_id, testname, status, result = regex2use.findall(
                    hostline)[0]
                # Sanitize output !
                task_id = int(task_id)
            except (AttributeError, TypeError, IndexError):
                # This could be score line details like:
                # 81955317 [sanity                          ] PASS Score: 0
                if 'Listening' in hostline:
                    print(f'{hostobj.hostname:<40} {hostline}')
            else:
                return (hostobj, task_id, testname.strip(), status, result)
        else:
            LOGGER.info('!!! Received line from unknown host %s', host)
            LOGGER.info('* line: "%s"', hostline)

        return None

    def find_host_lines(self, line):
        """Process restraint output."""
        match_result = self.filter_output(line)
        if not match_result:
            return

        for hostobj, hostline in match_result:
            tup = self.split_hostline_match(hostobj, hostline)
            if not tup:
                continue
            hostobj, task_id, _, status, result = tup
            # Ignore Abort that could cause a re-run.
            if status != 'Aborted' or hostobj.recipe_id not in self.host_heartbeat():
                LOGGER.debug('*** New restraint client output ***')
                host = self.provisioner.find_host_object([self.resource_group], hostobj.recipe_id)
                # Pass params to ActionOnResult objects, so we keep evaluating
                # result and do further actions, such as re-running some tasks.
                self.process_task(host, task_id, status, result)

    def safe_copy(self, path2fname, dst_dir):
        """Copy a path2fname file to dst_dir; number the file in dst_dir if file already exists."""
        # Take filename from source path.
        dst_path = dst_dir.joinpath(path2fname.name)
        # If it already exists, we have to handle it.
        if dst_path.is_file():
            counter = 0
            while dst_path.is_file():
                # Generate new filename by putting -'{counter} (e.g. '-0') before last extension.
                if path2fname.suffixes:
                    newfname = (
                        str(path2fname).split(path2fname.suffixes[-1], maxsplit=1)[0] +
                        f'-{counter}' +
                        path2fname.suffixes[-1]
                    )
                else:
                    newfname = f'{path2fname}-{counter}'
                dst_path = dst_dir.joinpath(pathlib.Path(newfname).name)
                counter += 1

        self.all_copied_files.append((path2fname, dst_path))
        shutil.copy(path2fname, dst_path)

    @classmethod
    def get_dumpfile_name(cls, task_result, attempts_made, resource_id, prefix=''):
        """Get kcidb dumpfile name given task_result."""
        # This is not to be used for anything else; it is not .path attribute.
        test_ident = (task_result.universal_id if task_result.universal_id else
                      f'redhat_{task_result.testname.lstrip("/")}').replace('/', '.')

        original_taskid = task_result.task_id if attempts_made == 0 else \
            task_result.host.rerun_recipe_tasks_from_index + task_result.task_id - 1

        return f'{prefix}{resource_id}_{attempts_made}_R_{task_result.recipe_id}_T_' \
               f'{original_taskid}_test-{test_ident}'

    def get_logs_results_dst_dir(self, task_result, dirname):
        """Return pathlib path for a task results/logs."""
        fname = self.get_dumpfile_name(task_result,
                                       self.resource_group.recipeset.attempts_made,
                                       self.resource_group.resource_id)

        dst_dir = pathlib.Path(self.output, task_result.host.counter.path,
                               f'{dirname.upper()}_{fname}')
        return dst_dir

    def copy_dir(self, path2dirname, dirname, task_result):
        """Copy contents of results or logs directory, changing the paths."""
        # Base destination directory like
        # 'run.done/results_0001/LOGS_1_0_R_1_T_1_test-redhat_distribution.command'
        # 'run.done/results_0001/RESULTS_1_0_R_1_T_1_test-redhat_distribution.command'
        dst_dir = self.get_logs_results_dst_dir(task_result, dirname)
        dst_dir.mkdir(parents=True)

        # Go through results or logs directory.
        subtask_dirs = []
        for item in glob.glob(f'{path2dirname}/*'):
            path2item = pathlib.Path(item)
            if path2item.is_file():
                # Just files in results/logs directories. Simple.
                self.safe_copy(path2item, dst_dir)
            if path2item.is_dir():
                # Subtask directory structure - handle separately.
                subtask_dirs.append(path2item)

        if subtask_dirs:
            # Handle subtasks.
            self.copy_subtask_dirs(task_result, subtask_dirs, dst_dir)

    def copy_subtask_dirs(self, task_result, subtask_dirs, dst_dir):
        """Copy contents of subtask directories to results_XXXX directories."""
        xml = pathlib.Path(self.output, self.run_suffix, 'job.xml').read_text(encoding='utf8')
        restraint_job = RestraintJob.create_from_string(xml)
        restraint_recipe = restraint_job.get_recipe_by_id(task_result.recipe_id)

        for subtask_directory in subtask_dirs:
            recipe_subtask_result = restraint_recipe.get_result_by_id(subtask_directory.name)
            # Use main task name if task aborted.
            tst_name = recipe_subtask_result.path.lstrip('/').replace('/', '.') if \
                recipe_subtask_result else task_result.testname
            # Subtask results still have 'logs' directory, hence */*. Should this change, safe_copy
            # makes sure files aren't overwritten by accident.
            for subtask_item in glob.glob(f'{subtask_directory}/*/*'):
                # Append subtask name to destination directory.
                new_dst_dir = dst_dir.joinpath(tst_name)
                new_dst_dir.mkdir(parents=True, exist_ok=True)
                # We don't consider any more directories here.
                self.safe_copy(pathlib.Path(subtask_item), new_dst_dir)

    def copy_results_and_logs(self, hosts):
        """Copy results and logs directories to new destinations."""
        # For each host...
        for host in hosts:
            # Find a recipe that run on it in actual "run_restraint_job" fed to restraint.
            restraint_recipe = self.wrap.run_restraint_job.get_recipe_by_id(host.recipe_id)
            for idx_n, _ in enumerate(restraint_recipe.tasks, 1):
                base_path = pathlib.Path(self.output, self.run_suffix,
                                         f'recipes/{host.recipe_id}/tasks/{idx_n}/')
                # Each task has 'logs' and 'results' directories
                for dirname in ('logs', 'results'):
                    path2dirname = base_path.joinpath(dirname)
                    if path2dirname.is_dir():
                        # Take last N tasks from global task_result storage.
                        task_result = get_most_recent_task_result(host.recipe_id,
                                                                  idx_n,
                                                                  host.task_results)
                        self.copy_dir(path2dirname, dirname, task_result)

            # When done copying host files, create fixed index.
            try:
                self.create_fixed_index(host)
            except FileNotFoundError:
                LOGGER.info('restraint did not produce index.html for %s, likely because of issues'
                            ' on the host', host.hostname)

    def create_fixed_index(self, host):
        """Write index.html to results_XXXX directories with tweaked links."""
        path2index = pathlib.Path(self.output, self.run_suffix, 'index.html')
        index_content = path2index.read_text(encoding='utf8')
        index_tree = ET.fromstring(index_content, parser=lxml.etree.HTMLParser())
        for a_element in index_tree.findall('../a'):
            relpath = pathlib.Path(self.output, self.run_suffix)
            relpath_dst = pathlib.Path(self.output, host.counter.path)
            for src, dst in self.all_copied_files:
                try:
                    if str(pathlib.Path(src).relative_to(relpath)) == a_element.get('href'):
                        a_element.set('href', str(pathlib.Path(dst).relative_to(relpath_dst)))
                except ValueError:
                    pass

        # Save modified index file.
        pathlib.Path(self.output, host.counter.path, 'index.html') \
            .write_text(ET.tostring(index_tree, encoding='unicode'), encoding='utf8')

    def cleanup_handler(self):
        # pylint: disable=bare-except
        """Ensure related threads stop."""
        if self.cleanup_done:
            return

        # NOTE: stop watcher first, any issue in handler above could leave the process stuck!
        self.watcher.observer.stop()

        # Client process ended, nothing to do. Reruns will have to be done by another restraint protocol object.
        LOGGER.debug('restraint protocol cleanup runs (rid: %s)...', self.resource_group.resource_id)

        recipeset = self.resource_group.recipeset
        recipeset.tests_finished = True
        do_rerun = False

        for host in recipeset.hosts:
            if not host.done_processing:
                self.evaluate_host_abort(host)

        # Always copy-out all results into user-friendly path.
        try:
            self.copy_results_and_logs(recipeset.hosts)
        except:  # noqa: E722
            traceback.print_exc()

        if recipeset.waiting_for_rerun:
            # Increase the counter of attempts. Entire recipeSet has to be re-run.
            recipeset.attempts_made += 1
            do_rerun = recipeset.attempts_made <= self.reruns
            # Careful! Clear the flag AFTER evaluating re-run condition.
            recipeset.waiting_for_rerun = False

        if do_rerun:
            # Add a new protocol
            proto = self.add_protocol(self.provisioner, self.resource_group,
                                      self.on_task_result, **self.kwargs.copy())
            proto.start_all()
        else:
            # No rerun.
            # If we're not rerunning anything, then recipeset is finished.
            for host in recipeset.hosts:
                host.done_processing = True

                # Dump the remaining tasks w/ SKIP kcidb_status after processing a host.
                if self.dump:
                    self.dump_remaining_tasks_with_skip_status(host)

            LOGGER.info('Releasing resources')
            self.provisioner.release_resources()

        attempt_heartbeat_stop(self.heartbeat_loop)

        self.cleanup_done = True

    def processEnded(self, reason):
        # pylint: disable=no-member
        """Cleanup on process exit, handle retcode if it was caused by user."""
        # Log that restraint client process ended
        result = [f'R:{r.recipe_id}' for r in self.provisioner.find_objects([self.resource_group], lambda obj: obj if
                                                                            hasattr(obj, 'recipe_id') else None)]

        was_sigpiped = 'ended by signal 13' in str(reason.value)
        LOGGER.info('* restraint protocol (%s) %s retcode: %s reason: %s', result, 'was SIGPIPEd!' if
                    was_sigpiped else 'ended.', reason.value.exitCode, reason.value)
        if was_sigpiped:
            lasttr = self.last_task_result
            msg = f'{COLORS.RED}restraint protocol was killed by SIGPIPE, the test may be misbehaving{COLORS.RESET}'
            rich_msg = ActionOnResult.format_msg(
                lasttr.recipe_id, lasttr.task_id, msg, '*', lasttr.testname) if lasttr else msg
            print(rich_msg)

        self.cleanup_handler()

    def rerun_from_task(self, **kwargs):
        """Ensure task(s) are re-run for a host."""
        host = kwargs['host']
        recipeset = self.resource_group.recipeset

        if any(rs_host.done_processing for rs_host in recipeset.hosts):
            # Multihost testing cannot be rerun if any of the hosts is dead!
            LOGGER.info(
                'At least one of the hosts of the recipeset is dead, no reruns can be done!'
            )

            # Mark all hosts as done since we can't reliably run anything now.
            for rs_host in recipeset.hosts:
                rs_host.done_processing = True

            # Explicitly mark the recipeset as not waiting for reruns in case
            # the value was set previously.
            recipeset.waiting_for_rerun = False

            return

        # Set waiting_for_rerun and let tests finish, if they can.
        if not recipeset.waiting_for_rerun:
            recipeset.waiting_for_rerun = True
            # Restraint numbers task_id from 1.
            host.rerun_recipe_tasks_from_index = kwargs['task_id'] if host.rerun_recipe_tasks_from_index is None \
                else host.rerun_recipe_tasks_from_index + kwargs['task_id'] - 1

            task_result = kwargs['task_result']

            statusresult = f'Making a re-run attempt ' \
                           f'{recipeset.attempts_made + 1}/{self.kwargs["reruns"]} for the' \
                           f' entire recipeSet'
            msg = ActionOnResult.format_msg(task_result.recipe_id, task_result.task_id,
                                            statusresult, '*', task_result.testname)
            LOGGER.info(msg)

    def add_task_result(self, host, restraint_task, task_id, result, status):
        """Save task results to a RestraintHost object."""
        # Save the information that task hit LWD into task itself. This is important so we can make sure that recipe
        # isn't marked as done, task isn't rerun, yet testing finishes.
        lwd_hit = self.is_lwd_hit(host.recipe_id, task_id)
        ewd_hit = self.is_ewd_hit(host.recipe_id, status)
        task_result = TaskResult(host, restraint_task, task_id, result, status, lwd_hit=lwd_hit,
                                 ewd_hit=ewd_hit)

        # Load all results, regardless of whether it's for this result
        self.unprocessed_subtask_results = self.unprocessed_subtask_results.union(self.watcher.get_subtask_results())

        to_remove = set()
        # Link subtask results
        for s_recipe_id, s_task_id, s_testname, s_result, s_score in self.unprocessed_subtask_results:
            if s_recipe_id == host.recipe_id and s_task_id == task_id:
                task_result.subtask_results.append((s_testname, s_result, s_score))
                if task_result.status == 'Completed' and not task_result.result and \
                        s_result == 'SKIP':
                    task_result.result = s_result

                to_remove.add((s_recipe_id, s_task_id, s_testname, s_result, s_score))

        # Remove processed tuples from unprocessed_subtask_results
        self.unprocessed_subtask_results = self.unprocessed_subtask_results - to_remove

        host.task_results.append(task_result)

        return task_result

    @classmethod
    def cond_print_notification(cls, task_result, dirs):
        """When a test didn't pass, print carefully colorized info."""
        # "ERROR", "FAIL", "PASS", "DONE", "SKIP"
        if task_result.kcidb_status in ("ERROR", "FAIL"):
            color = COLORS.RED if task_result.kcidb_status == "FAIL" and not task_result.waived \
                else COLORS.YELLOW

            maintainers = [f'{entry["name"]} <{entry["email"]}>' for entry in
                           task_result.test_maintainers]
            gitlab_nicks = [f'{entry["gitlab"]}' for entry in task_result.test_maintainers if
                            entry["gitlab"]]
            gitlab_nicks = " ".join(gitlab_nicks) if gitlab_nicks else "<NOT SET!>"
            maintainers = " ".join(maintainers) if task_result.test_maintainers else \
                "<NOT SET!>"
            waive_info = 'waived' if task_result.waived else 'NOT WAIVED'
            LOGGER.printc(f'Test "{task_result.testname}" {task_result.kcidb_status}ed'
                          f' (result={task_result.result}, status={task_result.status})'
                          f' and it is {waive_info}. Please check with following test maintainers'
                          f' if you have trouble figuring out the failure: {maintainers}.'
                          f' gitlab.com usernames of the maintainers are {gitlab_nicks}',
                          color=color)

            msg_dirs = "".join([f'\n* {link}' for link in [convert_path_to_link(x, False) for x
                                                           in dirs]])
            LOGGER.printc(f'Test logs will be in: {msg_dirs}', color=COLORS.BLUE)

    def mark_recipe_done(self, **kwargs):
        """Find host by recipe_id and mark it as done processing.

        Whenever this method is called, it will mark host as 'done_processing' and no more
        tests will be run on the host.
        """
        task_result = kwargs['task_result']
        recipe_id = task_result.recipe_id

        if task_result.recipe_id != kwargs['recipe_id']:
            raise RuntimeError('broken code: invalid data provided')

        host = self.provisioner.find_host_object([self.resource_group], recipe_id)
        if not host.done_processing:
            host.done_processing = True

            msg = ActionOnResult.format_msg(host.recipe_id, task_result.task_id,
                                            'recipe is done processing [mark_recipe_done]', '*',
                                            task_result.testname)
            LOGGER.debug(msg)

        # Ensure no reruns are attempted if this function is called as a result
        # of aborted or panicked hosts. Those are broken and the reruns will
        # immediately fail. If this function is called as part of a finished
        # run cleanup, then the following assignment is a noop.
        self.resource_group.recipeset.waiting_for_rerun = False

    def mark_lwd_hit(self, **kwargs):
        """Mark LWD hit as processed."""
        task_result = kwargs['task_result']
        # New LWD hit detected (and consumed by this task)
        self.unprocessed_lwd_hits.remove((task_result.recipe_id,
                                          task_result.task_id))

    def _prepare_actions(self):
        # Determines what actions to make on a specific result combination.
        # See example use comment in the object itself.
        return [
            ActionOnResult('FAIL', [self.mark_recipe_done],
                           '(EWD hit during kernel boot means kernel boot failure.'
                           ' Further testing would be invalid -> recipe processing finished.)',
                           ewd_hit={True}, testname={'Boot test'}),

            # Panic detection comes from Beaker and NOT from restraint, thus it uses Beaker's
            # result format with only the first letter capitalized!
            ActionOnResult('FAIL', [self.mark_recipe_done],
                           '(Panic -> recipe processing finished.)',
                           result={'Panic'}, status={'Aborted'}),

            ActionOnResult('FAIL', [],
                           '(EWD was hit, but the test reported failure. No reruns will be done.)',
                           ewd_hit={True}, status={'Aborted'}, result={'FAIL'}),

            ActionOnResult('ERROR', [self.mark_recipe_done],
                           '(EWD hit, but no Panic is an infrastructure issue.'
                           ' No reruns will be done. Infrastructure issue -> recipe processing'
                           ' finished.)',
                           ewd_hit={True}, status={'Aborted'}),

            ActionOnResult('ERROR', [self.mark_lwd_hit],
                           '(LWD hit. This type of issue is ignored completely.)',
                           lwd_hit={True}),

            ActionOnResult('ERROR', [self.rerun_from_task],
                           '(A task aborted. Re-running task to confirm infrastructure issue.)',
                           status={'Aborted'}, out_of_reruns={False},
                           waiting_for_rerun={False}),

            ActionOnResult('ERROR', [],
                           '(A task aborted but a rerun was already scheduled. Infrastructure'
                           ' issue.)',
                           result={'WARN'}, status={'Aborted'}),

            ActionOnResult('FAIL', [],
                           '(A task has warnings. Kernel testing failed.)',
                           result={'WARN'}),

            ActionOnResult('FAIL', [],
                           '(A task failed. Kernel testing failed.)',
                           result={'FAIL'}),

            ActionOnResult('PASS', [],
                           '(A task passed.)',
                           result={'PASS'}),

            ActionOnResult('SKIP', [],
                           '(A task was skipped.)',
                           result={'SKIP'}),
            ActionOnResult('SKIP', [],
                           '(A task aborted without running, likely the whole run was aborted.)',
                           status={'Aborted'}),
            ActionOnResult(None, [],
                           '',  # No explanation to log, format_msg is enough
                           status={'Running'})
        ]

    def is_ewd_hit(self, recipe_id, status):
        """Check if a given recipe_id had an EWD hit."""
        LOGGER.debug('is_ewd_hit: %i %s %s', recipe_id, status, self.recipe_ids_dead)
        return recipe_id in self.recipe_ids_dead

    def is_lwd_hit(self, recipe_id, task_id):
        """Check if a given recipe_id:task_id had a LWD hit."""
        # Load new LWD hits and add them to the unprocessed ones.
        self.unprocessed_lwd_hits = self.unprocessed_lwd_hits.union(
            self.watcher.get_lwd_hits())

        LOGGER.debug('LWD hit check: is %i %s in %s?', recipe_id, task_id,
                     self.unprocessed_lwd_hits)
        return (int(recipe_id), int(task_id)) in self.unprocessed_lwd_hits

    def is_last_host_task(self, recipe_id, task_id):
        """Return True if this is the last task that the host should run according to its restraint_xml.

        Arguments:
            recipe_id - int, the id that uniquely identifies the host
        """
        restraint_job = RestraintJob.create_from_string(
            self.resource_group.recipeset.restraint_xml
        )
        restraint_recipe = restraint_job.get_recipe_by_id(recipe_id)

        assert restraint_recipe is not None, "Invalid data or broken code. Cannot find recipe_id specified."

        return len(restraint_recipe.tasks) == int(task_id)

    def dump_remaining_tasks_with_skip_status(self, host):
        """Dump remaining tasks with SKIP status."""
        attempts_made = self.resource_group.recipeset.attempts_made
        resource_id = self.resource_group.resource_id
        for task in host.planned_tests:
            task_result = next((result for result in host.task_results
                                if result.cki_name == task.cki_name), None)
            if task_result:
                # This task already ran and was dumped
                continue

            task.status = 'Cancelled'
            task.kcidb_status = 'SKIP'
            fname = self.get_dumpfile_name(task,
                                           attempts_made,
                                           resource_id,
                                           prefix='kcidb_') + '.json'
            LOGGER.debug('R: %s T: %s name: %s skipped',
                         task.host.recipe_id,
                         task.task_id,
                         task.testname)
            self.on_task_result(self.resource_group, task, self.run_suffix, fname)

    def use_action_rules(self, task_result, host, ac_kwargs):
        """Determine what should happen based on the results and data about hosts we have."""
        with self.sem_lwd_sync:
            # Go through predefined actions based on results
            for action in self.result_actions:
                # Check if conditions are fulfilled.
                if action.eval(**ac_kwargs):
                    # Debug: store data about what the conditions were
                    LOGGER.debug(str(action))

                    # Set sanitized kcidb test status/result!
                    task_result.kcidb_status = action.kcidb_status
                    # Format and print result.
                    msg = action.format_msg(host.recipe_id, task_result.task_id,
                                            colorize(f'{task_result.statusresult:<20}'),
                                            '*',
                                            task_result.testname, base=action.msg_string)
                    print(msg)

                    # If so, run all methods that must run in this case.
                    for func in action.lst_actions:
                        func(**ac_kwargs)

                    if task_result.kcidb_status:  # Do we have a finished task?
                        results_dir = self.get_logs_results_dst_dir(task_result, 'results')
                        logs_dir = self.get_logs_results_dst_dir(task_result, 'logs')

                        # On error: do notification with highlighting and maintainer info print.
                        self.cond_print_notification(task_result, [results_dir, logs_dir])

                    # DO NOT PROCESS OTHER ACTIONS IN THE LIST
                    break

            if task_result.kcidb_status:
                # Dump json file(s) and upload artifacts anytime there's new task done!
                if self.dump:
                    fname = self.get_dumpfile_name(
                        task_result,
                        self.resource_group.recipeset.attempts_made,
                        self.resource_group.resource_id, prefix='kcidb_'
                    ) + '.json'
                    self.on_task_result(self.resource_group, task_result, self.run_suffix, fname)

    def evaluate_task_result(self, task_result, host):
        """Evaluate the result of task_result in relation to a host."""
        ac_kwargs = {'recipe_id': task_result.recipe_id,
                     'task_id': task_result.task_id,
                     'testname': task_result.testname,
                     'task': task_result.task,
                     'status': task_result.status,
                     'result': task_result.result,
                     'waived': task_result.waived,
                     'lwd_hit': task_result.lwd_hit,
                     'ewd_hit': task_result.ewd_hit,
                     'host': host,
                     'task_result': task_result,
                     'out_of_reruns': (self.resource_group.recipeset.attempts_made >= self.reruns),
                     'is_last_host_task': self.is_last_host_task(task_result.recipe_id, task_result.task_id),
                     'waiting_for_rerun': self.resource_group.recipeset.waiting_for_rerun}

        # Save what ran last.
        self.last_task_result = task_result

        if self.is_last_host_task(task_result.recipe_id, task_result.task_id) and \
                task_result.status and task_result.result and \
                not self.resource_group.recipeset.waiting_for_rerun:
            task_result.host.done_processing = True
            self.mark_recipe_done(**ac_kwargs)

        if not task_result.is_cki_test:
            statusresult = task_result.status if not task_result.result else\
                f'{task_result.status}: {task_result.result}'
            status_result_in_color = colorize(f'{statusresult:<20}')
            print(ActionOnResult.format_msg(host.recipe_id, task_result.task_id,
                                            status_result_in_color, '*', task_result.testname))

            print(f'{task_result.testname} is a setup task - not evaluating as test.')
            return

        # Go through a list rules and determine what should happen based on the results and data
        # about hosts we have.
        self.use_action_rules(task_result, host, ac_kwargs)

    def get_restraint_xml_task(self, recipe_id, task_id):
        """Get task of a recipe by recipe_id and task_id."""
        restraint_job = self.wrap.run_restraint_job
        restraint_recipe = restraint_job.get_recipe_by_id(recipe_id)
        try:
            return restraint_recipe.tasks[task_id - 1]
        except (IndexError, AttributeError):
            LOGGER.error('task T:%i not found in recipe %s!', task_id, recipe_id)
        return None

    def evaluate_host_abort(self, host):
        """Evaluate the result of the last task again, but override its status to 'Aborted'.

        This is done because restraint gives us output on task state transition only and we need to re-evaluate
        conditions, when the host suddenly hits EWD. This method is to be called in such cases.
        """
        forced_status = 'Aborted'
        forced_result = 'Panic' if host.panicked else None
        if not host.task_results:
            # If nothing was run, get a task that was supposed to be run. Here we have some chance that the distro
            # failed to install.
            restraint_task = self.get_restraint_xml_task(host.recipe_id, 1)
            if not restraint_task:
                LOGGER.error('💀 Unhandled case: cannot find task_id 1!')
                return

            # NOTE-hack: we quietly assume that the task Aborted along with the system, but restraint wasn't able to
            # provide this info.
            task_result = self.add_task_result(host, restraint_task, 1, forced_result, forced_status)
        else:
            # Get last task that was run and its task_result. If the task started running at all and there was
            # an output from restraint, then the result should be available.
            task_result = host.task_results[-1]
            task_result.status = forced_status
            task_result.result = forced_result or task_result.result
            task_result.statusresult = forced_status if not task_result.result else \
                f'{forced_status}: {task_result.result}'
            task_result.lwd_hit = self.is_lwd_hit(task_result.recipe_id, task_result.task_id)
            task_result.ewd_hit = self.is_ewd_hit(task_result.recipe_id, task_result.status)

        # Run method to evaluate conditions and run appropriate action.
        self.evaluate_task_result(task_result, host)

    def process_task(self, host, task_id, status, result):
        # pylint: disable=too-many-arguments,too-many-locals
        """Evaluate the result of the task and how it affects how we go on.

        This method is to be called immediately after we've received restraint client output.
        """
        # Get the task from provisioner data
        restraint_task = self.get_restraint_xml_task(host.recipe_id, task_id)
        if not restraint_task:
            LOGGER.error('%i: %d to rerun was not found!', host.recipe_id,
                         task_id)
            return None

        # Save task result in natural order per recipe_id
        task_result = self.add_task_result(host, restraint_task, task_id, result, status)

        return self.evaluate_task_result(task_result, host)

    def start_all(self):
        """Create & start process, register cleanup handlers."""
        cmds = self.wrap.restraint_commands
        print(f'* Running "{cmds}"...')

        # change directory, return on context end
        with misc.enter_dir(self.output):
            self.proc = reactor.spawnProcess(self, self.restraint_binary, shlex.split(cmds), env=os.environ)

            reactor.addSystemEventTrigger('before', 'shutdown', self.cleanup_handler)
