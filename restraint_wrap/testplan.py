"""A plan of tests to run."""
from itertools import chain
import os
import pathlib

from kcidb_io.schema.v4 import JSON_VERSION_MAJOR
from kcidb_io.schema.v4 import JSON_VERSION_MINOR

from plumbing.interface import ProvisionerCore
from plumbing.objects import ResourceGroup
from restraint_wrap.kcidb_adapter import KCIDBTestAdapter
from restraint_wrap.kcidb_adapter import RestraintStandaloneTest
from restraint_wrap.misc import convert_path_to_link
from restraint_wrap.restraint_file.job_element import RestraintJob
from restraint_wrap.task_result import TaskResult
from upt.logger import LOGGER
from upt.misc import OutputDirCounter


class TestPlan:
    """A plan of tests to run."""

    # Prevent pytest from trying to collect TestPlan as tests
    __test__ = False

    def __init__(self, provisioners, **kwargs):
        """Create the object."""
        self.kcidb_version = {'major': JSON_VERSION_MAJOR, 'minor': JSON_VERSION_MINOR}

        # Command-line arguments.
        self.kwargs = kwargs

        # Global output directory.
        self.output = kwargs.get('output')

        # Provisioner objects we got from yaml.
        self.provisioners = provisioners

        # Optional kcidb adapter when we want to dump kcidb data.
        self.adapter = KCIDBTestAdapter(**kwargs) if kwargs.get('dump') else None

        for recipeset in chain(*[ProvisionerCore.find_objects(
            chain(*[prov.rgs for prov in self.provisioners]),
            lambda rg: rg.recipeset if isinstance(rg, ResourceGroup) else None
        )]):
            self.create_testplan(self.output, recipeset)

    @classmethod
    def create_testplan(cls, output, recipeset):
        """Populate planned_tests in hosts in this recipeset with TaskResult objects that match tests to run."""
        # Restraint xml contains "raw", _complete_ specification of tasks to run.
        restraint_job = RestraintJob.create_from_string(recipeset.restraint_xml)

        # Process everything in this recipeset.
        for host in recipeset.hosts:
            # NOTE: empty planned_tests first in case we ran this twice!
            host.planned_tests = []

            # We build testplan based on input data and adhere to it. The counter is initially
            # assigned only here. Any code be adjusting the number of hosts in testplan should go
            # through TestPlan class.
            host.counter = OutputDirCounter()
            path2host = pathlib.Path(output, host.counter.path)
            LOGGER.printc(f'Results for {host.hostname} will be present in the job artifacts in: '
                          f'{path2host}')

            link = convert_path_to_link(path2host.joinpath('index.html'), True)
            LOGGER.printc(f'Please see the index.html file for results when the job finishes: '
                          f'{link}')

            restraint_recipe = restraint_job.get_recipe_by_id(host.recipe_id)
            assert restraint_recipe is not None, "Invalid data or broken code. Cannot find recipe_id specified."
            tasks_of_this_host = restraint_recipe.tasks

            # Restraint indexes tasks starting with 1
            for index, restraint_task in enumerate(tasks_of_this_host, start=1):
                task_result = TaskResult(host, restraint_task, index, None, None)
                task_result.start_time = None

                if task_result.is_cki_test:
                    # Append a placeholder object into planned_tests attribute.
                    host.planned_tests.append(task_result)
                else:
                    LOGGER.debug('%s is a setup task - not part of testplan.',
                                 task_result.testname)

    def on_task_result(self, resource_group, task_result, run_suffix, fname):
        """Create a kcidb Test object when a result is available.

        Arguments:
            resource_group: resource group object from provisioning request to identify origin of this test
            task_result:    TaskResult object that contains result of a test that finished.
            run_suffix:     str, subdirectory created by restraint client, e.g './job.01'
            fname:          str, a name of the file to write results to
        """
        # Check if task is finished
        if task_result.status in ['Aborted', 'Cancelled', 'Completed']:
            LOGGER.debug('Task R:%i T:%i is done and being dumped.', task_result.recipe_id, task_result.task_id)

            original_taskid = task_result.task_id if resource_group.recipeset.attempts_made == 0 \
                else task_result.host.rerun_recipe_tasks_from_index + task_result.task_id - 1

            # We create a kcidb Test object when the test is finished. This is considered to be incremental result,
            # because there may be other tests queued for this host & kernel. We don't include state transitions
            # of the tests in the results.
            new_test = RestraintStandaloneTest(self.adapter, task_result,
                                               original_task_id=original_taskid,
                                               run_suffix=run_suffix,
                                               rerun_index=resource_group.recipeset.attempts_made + 1)

            kcidb_data = {
                'version': self.kcidb_version,
                'checkouts': [],
                'builds': [],
                'tests': [new_test.render()]
            }

            # Dump data to global output directory /{counter_number:04d}/
            fpath_out = pathlib.Path(self.output, task_result.host.counter.path)
            # Ensure directory exists
            fpath_out.mkdir(exist_ok=True, parents=True)
            KCIDBTestAdapter.dump(kcidb_data, str(fpath_out.joinpath(fname)))

    def dump_testplan(self):
        """Dump planned tests into a single kcidb-data json file."""
        tests2dump = []

        for host in chain(*ProvisionerCore.find_objects(
            chain(*[prov.rgs for prov in self.provisioners]),
            lambda rg: rg.recipeset.hosts if isinstance(rg, ResourceGroup) else None
        )):
            for task_result in host.planned_tests:
                # Don't try to get files that tests created, the tests didn't even run!
                tests2dump.append(RestraintStandaloneTest(self.adapter, task_result, testplan=True).render())

        kcidb_data = {
            'version': self.kcidb_version,
            'checkouts': [],
            'builds': [],
            'tests': tests2dump
        }

        # Dump testplan to global output directory.
        KCIDBTestAdapter.dump(kcidb_data, os.path.join(self.output, 'kcidb_testplan.json'))
