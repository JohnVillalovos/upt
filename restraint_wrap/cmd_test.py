"""The "test" command."""
import argparse
import itertools
import os

from cki_lib.logger import logger_add_fhandler

from plumbing.format import ProvisionData
from restraint_wrap.misc import add_directory_suffix
from restraint_wrap.runner import Runner
from upt import cmd_misc
from upt.logger import LOGGER


def get_prov_data(input_fname):
    """Sanity check the provisioner data and return them."""
    prov_data = ProvisionData.deserialize_file(input_fname)
    rgs = list(itertools.chain(*[provisioner.rgs for provisioner in prov_data.provisioners]))
    if not rgs:
        raise RuntimeError("Input file doesn't have any resources to test on.")

    return prov_data.instance_no, prov_data.provisioners


def build(cmds_parser, common_parser):
    """Build the argument parser for the domain command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "test",
        help_message='Run tests using restraint.',
        add_subparser=False,
    )

    cmd_parser.add_argument('-i', '--input', default='c_req.yaml',
                            help='Path to input file (provisioning request). '
                            'Default: c_req.yaml.')
    cmd_parser.add_argument('-o', '--output', default='run',
                            help='Path to output directory. For "run", a "run.done.01" directory will be'
                            ' created. Please check that the directory does not already exist.')
    cmd_parser.add_argument('--keycheck', default=False, action=argparse.BooleanOptionalAction,
                            help='Do strict ssh host keychecking.')
    cmd_parser.add_argument('--reruns', default=1, type=int,
                            help='How many times to re-run tasks that are to be rerun based on '
                            'rules (e.g. EWD hits).')
    cmd_parser.add_argument('--rc', default=os.environ.get('RC_FILE', 'rc'),
                            help='Path to rc file. Defaults to RC_FILE env. '
                            'variable or rc.')
    cmd_parser.add_argument('--dump', default=False, action=argparse.BooleanOptionalAction,
                            help='If True, dump kcidb files.')
    cmd_parser.add_argument('--upload', default=False, action=argparse.BooleanOptionalAction,
                            help='If True, upload files to s3 buckets.')


def test(args):
    """Run tests using restraint."""
    # Ensure we're ready to roll (do kernel testing) and save instance number for --async.
    args.instance_no, provisioners = get_prov_data(args.input)
    output = args.output = add_directory_suffix(args.output, args.instance_no)
    logger_add_fhandler(LOGGER, 'info.log', output)
    kwargs = vars(args)
    runner = Runner(provisioners, **kwargs)

    runner.do_main()


test.__test__ = False


def main(args):
    """Run tests using restraint."""
    test(args)
