"""Restraint Recipe Element."""

from __future__ import annotations

import copy
import typing
import xml.etree.ElementTree as ET

from .common_element import CommonXMLElement
from .restraint_dataclasses import RestraintRecipeDiff
from .result_element import RestraintResult
from .task_element import RestraintTask


class RestraintRecipe(CommonXMLElement):
    """Recipe class.

    This class contains a list of tasks.
    You can see an example in examples.py file (recipe_example).
    """

    _attributes = ['id', 'job_id', 'result', 'status', 'system']
    _name = 'recipe'
    _delegated_items = ['task']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.tasks = self._process_tasks()
        self._clear()

    def _process_tasks(self) -> typing.List[RestraintTask]:
        """Process tasks."""
        return [RestraintTask(task) for task in self.element.findall('.//task')]

    def generate_xml_object(self) -> ET.Element:
        """Generate the object."""
        # clean the element
        self._clear()
        # Generate and add the tasks
        for task in self.tasks:
            self.element.append(task.generate_xml_object())
        return copy.copy(self.element)

    def get_result_by_id(self, result_id: str) -> typing.Optional[RestraintResult]:
        """Get RestraintResult by id.

        Args:
            result_id - str, the id that uniquely identifies the RestraintResult.
        """
        for task in self.tasks:
            if result := task.get_result_by_id(result_id):
                return result
        return None

    def diff(self, other: RestraintRecipe) -> typing.Optional[RestraintRecipeDiff]:
        """Generate a diff between two recipe, including task diff.

        A RestraintRecipe never should change the number of tasks,
        that's why we don't compare the number of tasks and its id,
        in the same RestraintRecipe.
        """
        if not isinstance(other, RestraintRecipe):
            raise TypeError('Both objects must be RestraintRecipe')
        if self.id is None or other.id is None:
            raise ValueError('id must be defined in both RestraintRecipes')
        if self.id != other.id:
            raise ValueError('RestraintRecipes do not have the same id')

        changes = self.get_diff_attributes(other)

        tasks_changes = [
            task_changes
            for task, other_task in zip(self.tasks, other.tasks)
            if (task_changes := task.diff(other_task))
        ]

        if changes or tasks_changes:
            return RestraintRecipeDiff(self.id, changes, tasks_changes)

        return None
