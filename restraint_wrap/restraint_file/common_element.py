"""Restraint xml common element."""

from __future__ import annotations

import copy
import typing
import xml.etree.ElementTree as ET

from .restraint_dataclasses import XMLElementAttributesDiff

T = typing.TypeVar('T', bound='CommonXMLElement')


class CommonXMLElement:
    """
    Common XML Element.

    This is the base class for all XML elements. The main idea is try to use
    the pythonist way to manage the XML data. We want to be able to manage XML
    attributes as object properties.

    Attributes:
        _attributes:        The list of xml attributes that the object accepts.
        _name:              The name of the element, this is necessary when we want
                            to create an object from scratch.
        _delegated_items:   The list of xml tags (or elements) delegated to another classes,
                            so our model never should store it.
        element:            The XML element used as a backend.
    """

    _attributes: typing.List[str] = []
    _name: str = ''
    _delegated_items: typing.List[str] = []

    def __init__(self, element: ET.Element):
        """
        Initialize.

        Args:
            element:    An ElementTree object copied used as backend, we copy the element
                        to avoid change in the backend by external modification.
        """
        self.element = copy.copy(element)

    def __getattr__(self, item: str) -> typing.Optional[str]:
        """
        Check in the list of xml attributes if we don't find in the python object.

        When we try to get any property of a object, python calls to __getattribute__
        (magic) method. When __getattribute__ can't find the property, it calls
        to __getattr__.
        """
        if item in self._attributes:
            return self.element.get(item)
        raise AttributeError

    def __setattr__(self, item: str, value: str) -> None:
        """
        Set attribute, if item is in the `_attributes` list will be saved as xml attribute.

        This is the magic method used when we try to save a property.
        First check if ITEM is an XML attribute to be saved in the
        XML backend (element), otherwise save it as a python property.
        """
        if item in self._attributes:
            self.element.set(item, value)
        else:
            object.__setattr__(self, item, value)

    @classmethod
    def create_from_string(cls: typing.Type[T], xml_content: str) -> T:
        """
        Create a XML Element from a XML string.

        Sometimes we have the XML as a string and we want to avoid using XML libraries to create the object
        """
        element = ET.fromstring(xml_content)
        return cls(element)

    @classmethod
    def create_from_scratch(cls: typing.Type[T]) -> T:
        """
        Create a XML Element from scratch.

        Method to create an XML element from scratch, this method used `_name`, to get the
        name of the XML Element
        """
        if not cls._name:
            return cls.create_from_string(f'<{None} />')
        return cls.create_from_string(f'<{cls._name} />')

    def generate_xml_object(self) -> ET.Element:
        """
        Generate the xml object.

        In this base class, this method should be a dummy function, but must be used by child
        classes to (re) generate the backend with all changes. A child class should
        only manages its xml element, and delegate child elements to other classes.
        """
        # You should clear your backend before returning it.
        self._clear()
        return copy.copy(self.element)

    def get_diff_attributes(self, other: CommonXMLElement) -> typing.List[XMLElementAttributesDiff]:
        """Get changes in attributes between two CommonXMLElement."""
        if not isinstance(other, CommonXMLElement):
            raise TypeError('Both objects must be CommonXMLElement')
        return [
            XMLElementAttributesDiff(attr, old_value, new_value)
            for attr in self._attributes
            if (old_value := self.element.get(attr)) != (new_value := other.element.get(attr))
        ]

    def _clear(self) -> None:
        """Clear our xml backend, we have to remove all delegated_items."""
        xml_content = ET.canonicalize(ET.tostring(self.element, encoding='unicode'))
        tree = ET.fromstring(xml_content)
        for item in self._delegated_items:
            for parent in tree.findall('.//' + item + '/..'):
                for element_to_remove in parent.findall('./' + item):
                    parent.remove(element_to_remove)
        self.element = ET.fromstring(ET.tostring(tree, encoding='unicode'))

    def __str__(self) -> str:
        """Get user friendly description."""
        _ = self.generate_xml_object()
        return ET.canonicalize(ET.tostring(self.element, encoding='unicode'))
