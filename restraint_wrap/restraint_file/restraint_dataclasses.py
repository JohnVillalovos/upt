"""Restraint dataclasses."""
from dataclasses import dataclass
import typing


@dataclass
class XMLElementAttributesDiff:
    """Dataclass to represent a change in the attributes of a XML element."""

    key: str
    old_value: typing.Optional[str]
    new_value: typing.Optional[str]


@dataclass
class RestraintTaskDiff:
    """Dataclass to represent a change in a RestraintTask."""

    # pylint: disable=invalid-name
    id: str
    changes: typing.List[XMLElementAttributesDiff]


@dataclass
class RestraintRecipeDiff:
    """Dataclass to represent a change in a RestraintRecipe."""

    # pylint: disable=invalid-name
    id: str
    changes: typing.List[XMLElementAttributesDiff]
    tasks: typing.List[RestraintTaskDiff]


@dataclass
class RestraintRecipeSetDiff:
    """Dataclass to represent a change in a RestraintRecipeSet."""

    # pylint: disable=invalid-name
    id: typing.Optional[str]
    changes: typing.List[XMLElementAttributesDiff]
    recipes: typing.List[RestraintRecipeDiff]


@dataclass
class RestraintJobDiff:
    """Dataclass to represent a change in a RestrainJob."""

    group: typing.Optional[str]
    changes: typing.List[XMLElementAttributesDiff]
    recipesets: typing.List[RestraintRecipeSetDiff]
