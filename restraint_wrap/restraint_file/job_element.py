"""Restraint Job Element."""

from __future__ import annotations

import copy
import itertools
from typing import List
from typing import Optional
import xml.etree.ElementTree as ET

from .common_element import CommonXMLElement
from .recipe_element import RestraintRecipe
from .recipeset_element import RestraintRecipeSet
from .restraint_dataclasses import RestraintJobDiff


class RestraintJob(CommonXMLElement):
    """Job class.

    This class contains a list of recipeSets.
    You can see an example in examples.py file (job_example).
    """

    _attributes = ['group']
    _name = 'job'
    _delegated_items = ['recipeSet']

    def __init__(self, element: ET.Element):
        """Initialize."""
        super().__init__(element)
        self.recipesets = self._process_recipesets()
        self._clear()

    def _process_recipesets(self) -> List[RestraintRecipeSet]:
        """Process recipesets."""
        return [RestraintRecipeSet(recipeset) for recipeset in self.element.findall('.//recipeSet')]

    def generate_xml_object(self) -> ET.Element:
        """Generate the object."""
        # clean the element
        self._clear()
        # Generate and add recipesets
        for recipeset in self.recipesets:
            self.element.append(recipeset.generate_xml_object())
        return copy.copy(self.element)

    def get_recipe_by_id(self, recipe_id: int) -> Optional[RestraintRecipe]:
        """Get RestraintRecipe by id.

        Args:
            recipe_id - int, the id that uniquely identifies the RestraintRecipe.
        """
        for recipeset in self.recipesets:
            if recipe := recipeset.get_recipe_by_id(recipe_id):
                return recipe
        return None

    def get_all_recipes(self) -> List[RestraintRecipe]:
        """Get all RestraintRecipe associated to this object.

        Returns:
           list - A list with the recipes for all its RestraintRecipeSet.
        """
        return list(itertools.chain(*[recipeset.recipes for recipeset in self.recipesets]))

    def diff(self, other: RestraintJob) -> Optional[RestraintJobDiff]:
        """Generate a diff between two jobs, including recipeset diff.

        A RestraintJob never should change the number of recipesets,
        that's why we don't compare the number of recipesets and its id,
        in the same Job.

        RecipeJob only has group attribute, so we can't compare its attribute, we only
        can compare recipeset (delegated item).

        RestraintJob usually does not have group field in job.xml files, so we don't check
        the group field.
        """
        if not isinstance(other, RestraintJob):
            raise TypeError('Both objects must be RestraintJob')

        recipesets_changes = [
            recipeset_changes
            for recipeset, other_recipeset in zip(self.recipesets, other.recipesets)
            if (recipeset_changes := recipeset.diff(other_recipeset))
        ]

        if recipesets_changes:
            return RestraintJobDiff(self.group, [], recipesets_changes)

        return None
