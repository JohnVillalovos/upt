"""Create kcidb data."""
from datetime import datetime
from datetime import timedelta
import json
import os
import pathlib

from cki.kcidb.utils import is_empty
from cki.kcidb.utils import upload_file
from cki_lib import trigger_variables
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.misc import get_env_bool
from cki_lib.misc import strtobool
from cki_lib.session import get_session
from dateutil.parser import parse as date_parse
from kcidb_io import schema

from restraint_wrap.misc import convert_path_to_link
from upt.logger import LOGGER

SESSION = get_session('upt.restraint_wrap.kcidb_adapter')


class RestraintStandaloneTest:
    # pylint: disable=too-many-instance-attributes,too-many-arguments
    """KCIDB test object adapter for test result from restraint client in standalone mode."""

    def __init__(self, adapter, task_result, run_suffix=None, original_task_id=None,
                 testplan=False, rerun_index=1):
        """Create the object.

        Arguments:
            adapter          - kcidbtestadapter
            task_result      - TaskResult, a test result from restraint runner wrapper around
                               restraint client
            run_suffix       - str, subdirectory created by restraint client, e.g './job.01'
            original_task_id - int, a task_id that matches order of the test in testplan
            testplan         - bool, if True, then this is a test that hasn't run yet, if False,
                               it is an incremental result
            rerun_index      - int, how many times the test was retried
        """
        self.adapter = adapter
        self.task_result = task_result
        self.run_suffix = run_suffix
        self.original_task_id = original_task_id or task_result.task_id
        self.rerun_index = rerun_index

        self.testplan = testplan

        # Compute how long the test ran. In seconds. If computation is 0, remove this
        duration = int((date_parse(f'{datetime.utcnow().isoformat()}Z') -
                        date_parse(task_result.start_time)).total_seconds()) if task_result.start_time else 0
        self.duration = duration or None

        # Start time as ISO.
        self.start_time = task_result.start_time

        # Finish time as ISO.
        self.finish_time = (
            (date_parse(self.task_result.start_time) + timedelta(seconds=self.duration)).isoformat()
            if self.task_result.start_time and self.duration else None
        )

        self.targeted_tests_list = (
            pathlib.Path(
                os.environ['CI_PROJECT_DIR'],
                self.adapter.rc_data.state.targeted_tests_list)
            .read_text(encoding='utf8').splitlines()
            if self.adapter.rc_data.state.targeted_tests == 1 else []
        )

    @property
    def output_files(self):
        """Get a list of test outputs: logs, dumps, etc."""
        output_files = []
        if not self.testplan:
            path_prefix = str(pathlib.Path(self.adapter.output, self.run_suffix))
            for file in self.task_result.output_files_from_path(path_prefix):
                relative_dir_path = pathlib.Path(file['path']).relative_to(self.adapter.output)
                if self.adapter.upload:
                    upload_destination = pathlib.Path(self.adapter.artifacts_path,
                                                      str(self.adapter.instance_no),
                                                      self.task_result.host.counter.path,
                                                      relative_dir_path)
                    LOGGER.debug('uploading file (%s, %s, %s)',
                                 upload_destination, file['name'], file['path'])
                    output_files.append(
                        {'name': file['name'],
                         'url': upload_file(self.adapter.visibility, upload_destination, file['name'],
                                            source_path=str(pathlib.Path(file['path'], file['name'])))}
                    )
                else:
                    output_files.append({
                        'name': file['name'],
                        'url': convert_path_to_link(os.path.join(file['path'], file['name']), True),
                    })

        return output_files

    def render(self):
        """Create dict object with data to dump."""
        data = self._render().copy()

        # Don't dump misc field for testplan.
        if not self.testplan:
            misc = {'provenance': self.provenance_data}
            data.setdefault('misc', {}).update(misc)
        data['origin'] = 'redhat'
        return {
            key: value for key, value in data.items() if not is_empty(value)
        }

    @property
    def provenance_data(self):
        """Return information about systems involved in this test execution."""
        provenance = []
        if get_env_bool('GITLAB_CI', False):
            provenance.append(
                {
                    'function': 'coordinator',
                    'url': os.environ['CI_JOB_URL'],
                    'environment': trigger_variables.pipeline_vars_from_env(),
                    'service_name': 'gitlab',
                }
            )
        if os.environ.get('BEAKER_URL'):
            provenance.append(
                {
                    'function': 'provisioner',
                    'url': f'{os.environ["BEAKER_URL"]}/recipes/{self.task_result.recipe_id}',
                    'service_name': 'beaker',
                }
            )

        return provenance

    def _render(self):
        """Return the KCIDB compatible data."""
        task_result = self.task_result
        rendered_data = {
            # build_id: Assign id of the build we're testing.
            'build_id': self.adapter.build['id'],
            # id: A globally unique id within kcidb, always prefixed with 'redhat'.
            'id': f'{self.adapter.build["id"]}_upt_{task_result.cki_id}',
            'comment': task_result.testname,
            'start_time': task_result.start_time,
            'output_files': self.output_files,
        }
        if not self.testplan:
            rendered_data['misc'] = self.misc
            # NOTE: We should fill in the environment the test ran in. E.g. a host, a set of hosts, or a lab;
            # amount of memory/storage/CPUs, for each host; process environment variables, etc.
            rendered_data['environment'] = {'comment': self.task_result.host.hostname}
            rendered_data['waived'] = task_result.waived
            rendered_data['duration'] = self.duration

        # "ERROR", "FAIL", "PASS", "DONE", "SKIP"
        rendered_data['status'] = task_result.kcidb_status

        # We only specify kcidb path attribute if we know it, it can be empty
        if task_result.universal_id:
            rendered_data['path'] = task_result.universal_id.replace('/', '.')

        return {
            key: value for key, value in rendered_data.items() if not is_empty(value)
        }

    @property
    def misc(self):
        """Miscellaneous extra data about the test."""
        data = {
            'debug': strtobool('false' if not self.adapter.rc_data.state.debug_kernel else
                               self.adapter.rc_data.state.debug_kernel),
            'targeted': self.task_result.testname in self.targeted_tests_list,
            'fetch_url': self.task_result.fetch_url,
            'beaker': {
                'task_id': self.original_task_id,
                'recipe_id': self.task_result.recipe_id
            },
            'maintainers': self.task_result.test_maintainers,
            'rerun_index': self.rerun_index
        }
        if self.finish_time:
            data['beaker']['finish_time'] = self.finish_time

        return data


class KCIDBTestAdapter:
    # pylint: disable=too-many-instance-attributes,too-few-public-methods
    """Dump restraint runner test results as kcidb data files as soon as the test is finished running."""

    def __init__(self, **kwargs):
        """Create the object."""
        self.kwargs = kwargs
        self.instance_no = kwargs['instance_no']
        # Common project output directory
        self.output = kwargs['output']
        # Rc-file data
        self.rc_data = kwargs['rc_data']
        self.upload = kwargs['upload']

        self.job_id = os.environ['CI_JOB_ID']

        self.instance_url = os.environ['CI_SERVER_URL']

        self.project_id = os.environ['CI_PROJECT_ID']
        self.path_with_namespace = os.environ['CI_PROJECT_PATH']

        kcidb_job_pathname = os.path.join(
            os.environ['CI_PROJECT_DIR'], os.environ['KCIDB_DUMPFILE_NAME'])
        self.kcidb_job_file = KCIDBFile(kcidb_job_pathname)
        self.checkout = self.kcidb_job_file.get_checkout(os.environ['KCIDB_CHECKOUT_ID'])
        self.build = self.kcidb_job_file.get_build(os.environ['KCIDB_BUILD_ID'])

        # A relative path that should be prepended to the bucket destination path.
        self.artifacts_path = (
            f'{os.environ.get("CI_JOB_YMD")}/'
            f'{self.checkout["id"]}/'
            f'build_{self.build["architecture"]}_{self.build["id"]}/'
            'tests/'
        )

        # Get artifact visibility.
        self.visibility = 'public' if os.environ.get('kernel_type') == 'upstream' else 'private'

    @staticmethod
    def dump(kcidb_data, to_where):
        """Dump kcidb_data to a file."""
        schema.V4.validate(kcidb_data)

        with open(to_where, 'w', encoding='utf-8') as fhandle:
            json.dump(kcidb_data, fhandle, indent=4)
