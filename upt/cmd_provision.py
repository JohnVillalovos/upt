"""The "provision" command."""
from plumbing.format import ProvisionData
from upt import cmd_misc
from upt.glue import ProvisionerGlue


def build(cmds_parser, common_parser):
    """Build the argument parser for the domain command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "provision",
        help_message='Provision resources according to --request_file.',
        add_subparser=False,
    )
    cmd_parser.add_argument('-d', '--dry-run', default=False, nargs='?', const=True,
                            help='Dry run, i.e. do not provision resources, just validate the input.'
                            ' Default: False')
    cmd_parser.add_argument('-r', '--request-file', default='reg.yaml',
                            help='Path to provisioner data. Default: req.yaml.')
    cmd_parser.add_argument('--pipeline', default='',
                            help='Run provisioning and testing asynchronously, if used.'
                            '  Specifies params to pass to test runner.')


def main(args):
    """Execute the `provision` command."""
    print('Provisioning resources...')
    provisioner_data = ProvisionData.deserialize_file(args.request_file)
    kwargs = vars(args)
    glue = ProvisionerGlue(provisioner_data, **kwargs)
    glue.run_provisioners()
