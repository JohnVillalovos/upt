<?xml version="1.0" encoding="UTF-8"?>
<job retention_tag="60days" group="cki">
   <whiteboard>{
   "name": "UPT Smoke Test (Warn) Arch: {{ arch_config|default('x86_64', true) }}",
   "jod_id": "GITLAB_JOB_ID_PLACEHOLDER"
   }</whiteboard>
   <!-- WARN -->
   <recipeSet priority="Normal">
      <recipe whiteboard="UPT Smoke test - WARN" role="None" ks_meta="redhat_ca_cert harness='restraint-rhts beakerlib-redhat' selinux=--permissive" kernel_options="" kernel_options_post="">
         <autopick random="false" />
         <watchdog panic="ignore" />
         <packages />
         <ks_appends>
            <ks_append><![CDATA[%post
set -x

# workaround for https://github.com/restraint-harness/restraint/issues/273
echo '\bOops\b' > /usr/share/rhts/failurestrings
echo '\bBUG\b' >> /usr/share/rhts/failurestrings
echo 'NMI appears to be stuck' >> /usr/share/rhts/failurestrings
echo 'Badness at' >> /usr/share/rhts/failurestrings

sed -i 's/^#DefaultTasksMax.*/DefaultTasksMax=50%/' /etc/systemd/system.conf
# Patch restraint's localwatchdog plugin to abort the recipe instead of warning,
# when a task goes over KILLTIMEOVERRIDE. See FASTMOVING-886 and FASTMOVING-1224.
sed -i 's|rstrnt-reboot|rstrnt-abort --server $RSTRNT_RECIPE_URL/tasks/$TASKID/status\nrstrnt-reboot|' /usr/share/restraint/plugins/localwatchdog.d/99_reboot

# Show information the test aborted due to localwatchdog
cat >> /usr/share/restraint/plugins/localwatchdog.d/10_localwatchdog << EOF
echo "\${RSTRNT_TASKNAME} hit test timeout, aborting it..." >> /dev/kmsg
EOF

# add restraint plugins to check if test is running on expected kernel
# https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db/-/issues/56
cat > /usr/share/restraint/plugins/task_run.d/20_check_kernel_version << EOF
#!/bin/sh
# Don't run from PLUGINS
if [ -n "\$RSTRNT_NOPLUGINS" ]; then
    exec "\$@"
    exit 0
fi
if [ -e /var/opt/cki/kernel_version ] && [ -n "\${RSTRNT_TASKID}" ]; then
    kernel_version=\$(uname -r)
    cki_kernel_version=\$(cat /var/opt/cki/kernel_version)
    if [ "\${kernel_version}" != "\${cki_kernel_version}" ]; then
        echo "aborting as system is running kernel \$kernel_version and not the expected \$cki_kernel_version"
        rstrnt-report-result "\${RSTRNT_TASKNAME}" "WARN"
        rstrnt-abort -t recipe
        exit 1
    fi
fi
exec "\$@"
EOF
chmod +x /usr/share/restraint/plugins/task_run.d/20_check_kernel_version

# add restraint plugins to get console log for each task
# https://restraint.readthedocs.io/en/latest/plugins.html
cat > /usr/share/restraint/plugins/task_run.d/30_init_test_console_log << EOF
#!/bin/sh
if [ -z "\${LAB_CONTROLLER}" ] || [ -z "\${RSTRNT_RECIPEID}" ] || [ -z "\${RSTRNT_TASKID}" ]; then
    # not automated job in beaker
    exec "\$@"
    exit 0
fi
# Don't run from PLUGINS
if [ -n "\$RSTRNT_NOPLUGINS" ]; then
    exec "\$@"
    exit 0
fi
# clean up dmesg before running test, as some tests check dmesg to search for errors
# https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/782
# restraint already has report_result.d/30_dmesg_clear that do the same,
# but as it runs in the end of a task, it might not run in case task gets aborted
dmesg -C
# Add test information to console.log, when running with UPT this information is not there
echo "Running test [R:\${RSTRNT_RECIPEID} T:\${RSTRNT_TASKID} - \${RSTRNT_TASKNAME:-unknown} - Kernel: \$(uname -r)]" > /dev/kmsg
mkdir -p /var/opt/cki/\${RSTRNT_TASKID}
_cki_init_file=/var/opt/cki/\${RSTRNT_TASKID}/init_console.log
if [ ! -e \${_cki_init_file} ]; then
    # save the log the first time the task starts. Don't override the file in case of reboot.
    curl --silent --show-error --retry 5 "http://\${LAB_CONTROLLER}:8000/recipes/\${RSTRNT_RECIPEID}/logs/console.log" -o \${_cki_init_file}
fi
exec "\$@"
EOF
chmod +x /usr/share/restraint/plugins/task_run.d/30_init_test_console_log

# plugins under completed.d should run even in case of localwatchdog
cat > /usr/share/restraint/plugins/completed.d/80_finish_console_log << EOF
#!/bin/sh
if [ -z "\${LAB_CONTROLLER}" ] || [ -z "\${RSTRNT_RECIPEID}" ] || [ -z "\${RSTRNT_TASKID}" ]; then
    # not automated job in beaker
    exit 0
fi
_cki_init_file=/var/opt/cki/\${RSTRNT_TASKID}/init_console.log
_cki_end_file=/var/opt/cki/\${RSTRNT_TASKID}/end_console.log
_cki_console_file=/var/opt/cki/\${RSTRNT_TASKID}/test_console.log
if [ ! -e \${_cki_init_file} ]; then
    exit 0
fi
# there is some delay on console.log
sleep 15
curl --silent --show-error --retry 5 "http://\${LAB_CONTROLLER}:8000/recipes/\${RSTRNT_RECIPEID}/logs/console.log" -o \${_cki_end_file}
diff --changed-group-format='%>' --unchanged-group-format='' \${_cki_init_file} \${_cki_end_file} > \${_cki_console_file}
# if the file is empty restraint will skip the upload
rstrnt-report-log -l "\${_cki_console_file}"
EOF
chmod +x /usr/share/restraint/plugins/completed.d/80_finish_console_log

# Patch restraintd service to continue after OOM, some tests generate OOM and restraintd should continue to run
sed -i '/OOMScoreAdjust=.*/a OOMPolicy=continue' /usr/lib/systemd/system/restraintd.service
mkdir -p /usr/share/rhts/
cat >/usr/share/rhts/falsestrings <<EOF
BIOS BUG
DEBUG
mapping multiple BARs.*IBM System X3250 M4
MAX_LOCKDEP_CHAIN_HLOCKS
BUG: KASAN: use-after-free in string_nocheck
watchdog: BUG: soft lockup - CPU
EOF

%end]]></ks_append>
         </ks_appends>
         <repos />
         <distroRequires>
            <and>
               <distro_family op="=" value="CentOSStream9" />
               <distro_variant op="=" value="BaseOS" />
               <distro_arch op="=" value="{{ arch_config|default('x86_64', true) }}" />
            </and>
         </distroRequires>
         <hostRequires>
            <or>
               <labcontroller op="=" value="lab-02.rhts.eng.bos.redhat.com"/>
               <labcontroller op="=" value="lab-02.rhts.eng.rdu.redhat.com"/>
               <labcontroller op="=" value="lab-02.rhts.eng.brq.redhat.com"/>
               <labcontroller op="=" value="beaker.ofa.iol.unh.edu"/>
            </or>
            <hypervisor op="!=" value="" />
            <arch op="=" value="{{ arch_config|default('x86_64', true) }}" />
         </hostRequires>
         <partitions />
         <task name="/distribution/check-install" role="STANDALONE">
            <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.zip#check-install"/>
            <params/>
         </task>
         <task name="UPT smoke tests ending with WARN result" role="STANDALONE">
             <fetch url="https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/archive/main/kernel-test-main.zip#upt_smoke_tests/warn"/>
             <params>
                 <param name="CKI_ID" value="10001"/>
                 <param name="CKI_NAME" value="UPT Smoke Test WARN"/>
                 <param name="CKI_UNIVERSAL_ID" value="upt_smoke_tests_warn"/>
                 <param name="KILLTIMEOVERRIDE" value="60"/>
             </params>
         </task>
      </recipe>
   </recipeSet>
</job>
