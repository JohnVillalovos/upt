"""The "legacy" command."""
import os
import pathlib

from cki_lib.logger import logger_add_fhandler
from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.format import ProvisionData
from plumbing.objects import Host
from plumbing.objects import RecipeSet
from plumbing.objects import ResourceGroup
from restraint_wrap.restraint_file.job_element import RestraintJob
from restraint_wrap.restraint_file.recipe_element import RestraintRecipe
from upt import cmd_misc
from upt import const
from upt import misc
from upt.logger import LOGGER


def build_convert(parser):
    """Build the argument parser for the "legacy convert" command."""
    parser.description = 'Convert standard kpet beaker.xml to UPT provisioning request.'
    global_group = parser.add_argument_group('Options')
    # execution_group = parser.add_argument_group('execution parameters')
    global_group.add_argument('-i', '--input_path', required=False,
                              help='Path to beaker.xml to convert. Default: beaker.xml',
                              default='beaker.xml')
    global_group.add_argument('-r', '--request-file', default='c_req.yaml',
                              help='Path to output (converted) file. Default: c_req.yaml.')
    global_group.add_argument('-g', '--group', default=None,
                              help='Optional. Beaker group override. When running using files'
                              ' from the pipeline (cki group), use "" to override group'
                              ' and submit jobs using your own account.')
    global_group.add_argument('-p', '--provisioner', default='beaker',
                              help='Which provisioner will be used with this request, so the '
                              'information is saved into the resulting yaml. Default: beaker')
    global_group.add_argument('--priority', default='normal',
                              help='Priority of the submitted test runs, defaults to "normal"')


def build(cmds_parser, common_parser):
    """Build the argument parser for the domain command."""
    cmd_parser, action_subparser = cmd_misc.build(
        cmds_parser,
        common_parser,
        "legacy",
        help_message='Commands for legacy pipeline.',
    )
    cmd_parser.description = 'Commands for legacy pipeline.'
    build_convert(action_subparser.add_parser(
        "convert",
        help='Convert standard kpet beaker.xml to UPT provisioning request.',
        parents=[common_parser],
    ))


def adjust_job_element(restraint_job, job_group):
    """Keep, set or remove group attribute in job element."""
    if job_group == '':
        restraint_job.group = None
    elif job_group is not None:
        restraint_job.group = job_group


def init_resource_group(xml, **kwargs):
    """Create a new resource group for each recipeSet.

    Restraint client has a nasty habit of exiting on EWD, even though that affects only 1 recipeSet, not the whole job.
    Because of this, each resource_group currently has only 1 recipeSet.
    """
    resource_group = ResourceGroup()
    restraint_job = RestraintJob.create_from_string(xml)

    adjust_job_element(restraint_job, kwargs.get('group'))

    # make restraint_job empty to create a template
    restraint_job.recipesets = []

    # save this in yaml in pretty format
    job_content = str(restraint_job).replace('\n', '').strip()
    resource_group.job = PreservedScalarString(job_content)

    resource_group.priority = kwargs['priority'].capitalize()

    return resource_group


def append_recipeset(xml, bkr, restraint_recipeset, **kwargs):
    """Add a recipeset to Beaker provisioner."""
    resource_group = init_resource_group(xml, **kwargs)
    bkr.rgs.append(resource_group)

    new_restraint_job = RestraintJob.create_from_string(const.JOB_XML_STUB)
    new_restraint_recipeset = new_restraint_job.recipesets[0]

    new_recipeset = RecipeSet()

    for restraint_recipe in restraint_recipeset.recipes:
        new_restraint_recipe = RestraintRecipe.create_from_scratch()
        # Move tasks from restraint_recipe to new_restraint_recipe.
        new_restraint_recipe.tasks = restraint_recipe.tasks
        restraint_recipe.tasks = []
        new_restraint_recipeset.recipes.append(new_restraint_recipe)

        new_host = Host({'hostname': '', 'recipe_id': None, 'recipe_fill': ''})
        # Duration force by cmd-line args takes precendece. Otherwise use
        # 120% of added-up task KILLTIMEOVERRIDE. If that's still 0, use
        # 8hours.
        host_tasks_duration = misc.compute_tasks_duration(new_restraint_recipe.tasks)
        force_host_duration = kwargs['force_host_duration']
        new_host.duration = force_host_duration if force_host_duration \
            else int(host_tasks_duration / 100 * 120 if host_tasks_duration
                     else const.DEFAULT_RESERVESYS_DURATION)

        # override recipe_fill
        new_host.recipe_fill = PreservedScalarString(str(restraint_recipe))
        new_recipeset.hosts.append(new_host)

    # remove and warn about tasks without fetch element; restraint cannot
    # run these
    misc.fixup_or_delete_tasks_without_fetch(new_restraint_job)

    # convert: write restraint xml
    new_recipeset.restraint_xml = PreservedScalarString(str(new_restraint_job))
    resource_group.recipeset = new_recipeset


def convert_xml(xml, **kwargs):
    """Convert Beaker XML file into a provisioning request."""
    # Create main data object, this will serialize into converted provisioning
    # request (c_req.yaml).
    prov_data = ProvisionData()

    # create a new restraint_job, because the previous was decomposed
    restraint_job = RestraintJob.create_from_string(xml)

    recipesets = restraint_job.recipesets
    if not recipesets:
        return prov_data

    provisioner = prov_data.get_provisioner(kwargs['provisioner'], create=True)
    # Go through all recipesets in original XML and split them to resource groups.
    for recipeset in recipesets:
        append_recipeset(xml, provisioner, recipeset, **kwargs)

    # Make sure we don't run on certain hosts.
    excluded_hosts_path = pathlib.Path(kwargs.get('excluded_hosts'))
    if excluded_hosts_path.is_file():
        hostnames = excluded_hosts_path.read_text(encoding='utf8')
        prov_data.get_provisioner(kwargs['provisioner']).exclude_hosts(hostnames.splitlines())

    return prov_data


def convert(args):
    """Convert standard kpet beaker.xml to UPT provisioning request."""
    kwargs = vars(args)

    # read xml
    xml = pathlib.Path(args.input_path).read_text(encoding='utf8')

    print(f'Converting {args.input_path} to {args.request_file}...')
    provisioner_data = convert_xml(xml, **kwargs)
    # write everything to file
    provisioner_data.serialize2file(args.request_file)


def main(args):
    """Commands for legacy pipeline."""
    if args.action == 'convert':
        # put log file info.log into current directory.
        logger_add_fhandler(LOGGER, 'info.log', os.getcwd())
        convert(args)
    else:
        misc.raise_action_not_found(args.action, args.command)
