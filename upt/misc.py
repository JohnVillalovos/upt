"""Misc utility methods."""
from enum import Enum
import pathlib
import re

from cki_lib import misc
from rcdefinition.rc_data import SKTData

from restraint_wrap.restraint_file.simple_elements import RestraintFetch
from upt import const
from upt.logger import LOGGER

SESSION = misc.get_session(__name__)


class ActionNotFound(Exception):
    """Raised when an action is not found."""


def raise_action_not_found(action, command):
    """Raise the ActionNotFound exception."""
    raise ActionNotFound(
        f'Action: "{action}" not found in command "{command}"'
    )


class Monotonic:
    # pylint: disable=too-few-public-methods
    """Get monotonic number."""

    def __init__(self, start_i=1):
        """Create an object; the counter is initialized to 1 by default."""
        self.__i = start_i

    def get(self):
        """Increase counter and return it."""
        retval = self.__i
        self.__i += 1
        return retval


class OutputDirCounter:
    # pylint: disable=too-few-public-methods
    """A counter that helps ensure test results are put in the right place."""

    number: Monotonic = Monotonic()

    def __init__(self):
        """Create an object."""
        # Number <1..N> coresponding to the number of hosts in the testplan.
        self.counter_number = OutputDirCounter.number.get()

    @property
    def path(self):
        """Get the prefix of a path where to store test results."""
        return f'results_{self.counter_number:04d}'


class RET(Enum):
    """Wraps all UPT (provisioner) return codes."""

    # Provisioning done successfully.
    PROVISIONING_PASSED = 0

    # For whatever reason, we've failed to provision a resource.
    # Maybe we've run out of time to finish provisioning. Or maybe there were
    # too many failures while doing that.
    PROVISIONING_FAILED = 1

    # Provisioning isn't completed, restraint runner will poll until it is.
    PROVISIONING_WAITING = 2

    @classmethod
    def merge_retcodes(cls, retcodes):
        """Combine multiple provisioner retcodes into one.

        The most severe retcode from the list will be returned, such that
        a passing retcode is only returned if all retcodes are passing.
        """
        for retcode in retcodes:
            assert retcode in (RET.PROVISIONING_PASSED, RET.PROVISIONING_FAILED)

        return max(retcodes, key=lambda x: x.value, default=cls.PROVISIONING_PASSED)

    @classmethod
    def to_yaml(cls, representer, data):
        """Serialize yaml as a given class."""
        return representer.represent_str(data.name)


def recipe_not_provisioned(restraint_recipe):
    """Return status if recipe aborted and cannot run tasks."""
    status = restraint_recipe.status
    return status if status in ('Aborted', 'Cancelled') else False


def recipe_installing_or_waiting(restraint_recipe):
    """Return True if recipe is installing."""
    return restraint_recipe.status in ('Installing', 'Waiting')


def reservesys_task_problem(restraint_tasks):
    """Return cause if any task in tasks list aborted, cancelled or failed."""
    for task in restraint_tasks:
        if task.status in ('Aborted', 'Cancelled'):
            return task.status
        if task.result == 'Fail':
            return task.result

    return False


def fixup_or_delete_tasks_without_fetch(restraint_job):
    """Delete <task /> elements that do not have <fetch /> in <params />."""
    for recipeset in restraint_job.recipesets:
        for recipe in recipeset.recipes:
            valid_tasks = []
            for task in recipe.tasks:
                if task.fetch:
                    valid_tasks.append(task)
                elif task.name == '/distribution/command':
                    url = const.BKR_CORE_TASKS_URL.format(task='command')

                    task.fetch = RestraintFetch.create_from_string(f'<fetch url="{url}" />')
                    valid_tasks.append(task)

                else:
                    # Delete task
                    LOGGER.warning('%s removed from XML, no fetch element!', task.name)
            recipe.tasks = valid_tasks


def compute_tasks_duration(restraint_tasks):
    """Add-up KILLTIMEOVERRIDE of all tasks in a list."""
    tasks_duration = 0
    for task in restraint_tasks:
        value = task.get_param_value_by_name('KILLTIMEOVERRIDE')
        if value is None:
            continue
        try:
            tasks_duration += int(value)
        except ValueError:
            LOGGER.debug('task duration was invalid: %s', value)

    return tasks_duration


def is_task_waived(restraint_task):
    """Check if a Beaker task has CKI_WAIVED param that is set to true."""
    for param in restraint_task.params:
        if param.name == 'CKI_WAIVED' and misc.strtobool(param.value):
            return True
    return False


def setup_args(args):
    """Do the setup for the UPT args."""
    # Convert bool to yes/no for ssh
    args.keycheck = 'yes' if args.keycheck else 'no'

    # Open rc-file, deserialize it
    path2rc = pathlib.Path(args.rc)
    args.rc_data = SKTData.deserialize(path2rc.read_text(encoding='utf8')) \
        if path2rc.is_file() else None


def sanitize_link(link, output_path, kernel_version):
    """Save sanitized contents to a file."""
    content = SESSION.get(link).content
    path2file = output_path.joinpath(pathlib.Path(link).name)
    path2file.write_bytes(content)

    # Sanitize irrelevant logs from before the kernel under test booted.
    if path2file.name.endswith('console.log') and kernel_version is not None:
        kernel_banner = f'Linux version {kernel_version}'
        regex = f'{re.escape(kernel_banner)}.*'
        matches = re.compile(regex).split(path2file.read_text(errors='ignore',
                                                              encoding='utf8'))

        if len(matches) > 1:
            sanitized = kernel_banner + ''.join(matches[1:])
        else:
            sanitized = ''

        path2file.write_text(sanitized)

    return path2file
