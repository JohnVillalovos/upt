"""The "waiton" command."""
from plumbing.format import ProvisionData
from upt import cmd_misc
from upt.glue import ProvisionerGlue


def build(cmds_parser, common_parser):
    """Build the argument parser for the domain command."""
    cmd_parser, _ = cmd_misc.build(
        cmds_parser,
        common_parser,
        "waiton",
        help_message='Wait on already submitted Beaker jobid matching provisioning...',
        add_subparser=False,
    )
    cmd_parser.description = 'Wait on already submitted Beaker jobid matching provisioning request.'
    cmd_parser.add_argument('-r', '--request-file', default='c_req.yaml',
                            help='Path to provisioner data. Default: c_req.yaml.')


def main(args):
    """Wait on already submitted Beaker jobid matching provisioning request."""
    print('Waiting on existing provisioning request...')
    # load provisioning request, this has to match the job that was submitted
    provisioner_data = ProvisionData.deserialize_file(args.request_file)
    kwargs = vars(args)
    glue = ProvisionerGlue(provisioner_data, **kwargs)
    # create helper object
    # wait on Beaker job
    glue.do_wait()
