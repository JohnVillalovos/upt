# KPET format compatibility

UPT consumes `beaker.xml`, which is created by kpet project. This file explains what
that xml must contain, so any changes to kpet don't break UPT. 

To ensure xml is UPT compatible, do following:
* check that submitting given xml to Beaker using `bkr submit` produces no fatal errors and successfully provisions hosts
* ensure that every `<task>` element has valid `<fetch>` element, so restraint knows from where to download tests
* ensure that every test has `CKI_UNIVERSAL_ID`, or `CKI_NAME` or both set; otherwise it's considered to be a setup task, not a test and its result doesn't affect the kernel testing result, unless this task aborts and the kernel to test is installed (boot task ran)
* ensure that `CKI_WAIVED` parameter with value `true` is used to denote a waived test; if the parameter is missing or set to value 'false', that test is not waived
