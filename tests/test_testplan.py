"""Test cases for plan_of_tests module."""
import os
import unittest
from unittest import mock

from ruamel.yaml.scalarstring import PreservedScalarString

from plumbing.format import ProvisionData
from plumbing.objects import Host
from plumbing.objects import ResourceGroup
from restraint_wrap.restraint_file.job_element import RestraintJob
from restraint_wrap.restraint_file.task_element import RestraintTask
from restraint_wrap.task_result import TaskResult
from restraint_wrap.testplan import TestPlan
from tests.const import ASSETS_DIR
from upt.misc import OutputDirCounter


class TestTestPlan(unittest.TestCase):
    """Test cases for TestPlan module."""

    def setUp(self) -> None:
        mock1 = mock.patch('restraint_wrap.testplan.LOGGER.printc', mock.Mock())
        mock1.start()

        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.beaker = self.provision_data.get_provisioner('beaker')

        self.testplan = TestPlan(self.provision_data.provisioners,
                                 **{'output': 'whatever-dir'})

    @mock.patch('restraint_wrap.testplan.RestraintStandaloneTest', mock.Mock())
    @mock.patch('restraint_wrap.kcidb_adapter.KCIDBTestAdapter.dump')
    def test_dump_testplan(self, mock_dump):
        """Ensure dump_testplan works."""
        # NOTE: RestraintStandaloneTest is mocked and tested elsewhere, so this test is brief.

        self.testplan.dump_testplan()
        mock_dump.assert_called()

    @mock.patch('restraint_wrap.testplan.RestraintStandaloneTest', mock.Mock())
    @mock.patch('restraint_wrap.kcidb_adapter.KCIDBTestAdapter.dump')
    @mock.patch('restraint_wrap.testplan.pathlib.Path.mkdir', lambda *args, **kwargs: None)
    def test_on_task_result(self, mock_dump_test):
        """Ensure on_task_result works."""
        resource_group = ResourceGroup()
        task_content = '<task id="1" name="a3" status="Completed" result="PASS"> <fetch url="git://"/></task>'
        restraint_tasks = [RestraintTask.create_from_string(task_content)]
        host = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        host.counter = OutputDirCounter()
        task_result = TaskResult(host, restraint_tasks[0], 1, 'New', 'Running')

        self.testplan.on_task_result(self.beaker.rgs[0], task_result, 'job.01', 'w.json')
        mock_dump_test.assert_not_called()

        task_result = TaskResult(host, restraint_tasks[0], 1, 'FAIL', 'Aborted')
        self.testplan.on_task_result(resource_group, task_result, 'job.01', 'w.json')
        mock_dump_test.assert_called()

    def test_create_testplan(self):
        """Ensure create_testplan populates planned_tests correctly."""
        recipeset = self.beaker.rgs[0].recipeset
        self.testplan.create_testplan('whatever-dir', recipeset)

        target_host1 = recipeset.hosts[1]
        target_host2 = recipeset.hosts[2]

        # The recipe_id checks are to check the consistency of code and assets test data.
        self.assertEqual(target_host1.recipe_id, 456)
        self.assertEqual(target_host1.planned_tests[0].testname, "TestUsedToCheckTestPlanLogic1")
        self.assertEqual(target_host2.recipe_id, 789)
        self.assertEqual(target_host2.planned_tests[0].testname, "TestUsedToCheckTestPlanLogic2")

    @mock.patch('upt.logger.LOGGER.debug')
    def test_create_testplan_no_dump_setup(self, mock_debug):
        """Ensure that testplan doesn't dump setup tasks."""
        recipeset = self.beaker.rgs[0].recipeset
        restraint_job = RestraintJob.create_from_string(recipeset.restraint_xml)
        task_name = 'abc'
        task = next((task for recipeset in restraint_job.recipesets
                     for recipe in recipeset.recipes
                     for task in recipe.tasks
                     if task.name == task_name), None)
        # Remove the task parameters.  This removes CKI_NAME and
        # CKI_ID parameters, thus TaskResult.is_cki_test will return
        # False.
        task.params = []
        # Save the updated xml.
        recipeset.restraint_xml = str(restraint_job)
        self.testplan.create_testplan('whatever-dir', recipeset)
        mock_debug.assert_called_with('%s is a setup task - not part of testplan.', task_name)

    def test_task_ids(self):
        """Ensure the task_id matches restraint task order."""
        recipeset = self.beaker.rgs[0].recipeset
        with open(os.path.join(ASSETS_DIR, 'cki_restraint_xml'), 'r') as cki_restraint_xml_file:
            recipeset.restraint_xml = PreservedScalarString(
                cki_restraint_xml_file.read()
            )
        # We only have one XML to do testing with, so get rid of other hosts for this test
        recipeset.hosts = [recipeset.hosts[0]]

        TestPlan.create_testplan('whatever-dir', recipeset)
        for index, task in enumerate(recipeset.hosts[0].planned_tests, start=1):
            self.assertEqual(index, task.task_id)
