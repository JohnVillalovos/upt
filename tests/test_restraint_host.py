"""Test cases for restraint_host module."""
import unittest

from restraint_wrap.restraint_host import RestraintHost


class TestRestraintHost(unittest.TestCase):
    """Ensure RestraintHost filter lines as it should."""

    def test_rgx_host(self):
        """Ensure 'connecting to' regex works."""
        result = RestraintHost.rgx_host.match('Connecting to host: username@host.com, recipe id:1234567')
        self.assertEqual(result.groups(), ('host.com', '1234567'))

    def test_rgx_host_line(self):
        """Ensure that regex to capture host output works."""
        result = RestraintHost.rgx_host_line.match('[abcd-ef-01] T:       1 [test 1             ] Running: PASS')
        self.assertEqual(result.groups(), ('abcd-ef-01', 'T:       1 [test 1             ] Running: PASS'))

    def test_rgx_state_change(self):
        """Ensure that regex to capture state change works."""
        result = RestraintHost.rgx_state_change.match('T:       1 [test 1             ] Running: PASS')
        self.assertEqual(result.groups(), ('1', 'test 1             ', 'Running', 'PASS'))

    def test_from_line(self):
        """Ensure from_line return correct object."""
        hosts = RestraintHost.from_line('1234=root@hostname1.domain.com 5678=root@hostname2.domain.com ')
        self.assertEqual(hosts[0].recipe_id, 1234)
        self.assertEqual(hosts[0].hostname, 'hostname1.domain.com')

        self.assertEqual(hosts[1].recipe_id, 5678)
        self.assertEqual(hosts[1].hostname, 'hostname2.domain.com')

    def test_in_list(self):
        """Ensure from_line return correct object."""
        hosts = RestraintHost.from_line('1234=root@hostname1.domain.com 5678=root@hostname2.domain.com')

        self.assertEqual(RestraintHost.in_list('hostname1', hosts), 0)
        self.assertEqual(RestraintHost.in_list('hostnameINVALID', hosts), None)
