"""Test cases for RestraintWrap __main__ module."""
import os
import pathlib
import unittest
from unittest import mock

from plumbing.format import ProvisionData
from plumbing.objects import ResourceGroup
from restraint_wrap import misc
from restraint_wrap.__main__ import main
from restraint_wrap.cmd_test import get_prov_data
from tests.utils import create_temporary_files

# pylint: disable=no-self-use


class TestRestraintWrap(unittest.TestCase):
    """Test cases for RestraintWrap __main__ module."""

    @mock.patch('restraint_wrap.cmd_test.get_prov_data', lambda *_a: [1, [mock.MagicMock()]])
    @mock.patch('restraint_wrap.cmd_test.Runner')
    def test_api(self, mock_runner):
        # pylint: disable=no-self-use
        """Test basic function call flow."""
        with (
                create_temporary_files(['rcfile', 'ignore']) as tempdir,
                mock.patch('sys.argv',
                           ['__main__.py', 'test', '--rc', 'rcfile', '-i',
                            'ignore', '-o', tempdir,
                            '--no-dump', '--no-upload'])
        ):
            main()
            mock_runner.assert_called()

    @mock.patch('restraint_wrap.cmd_test.get_prov_data', lambda *_a: [1, [mock.MagicMock()]])
    @mock.patch('restraint_wrap.cmd_test.Runner', mock.Mock())
    @mock.patch('sentry_sdk.init')
    def test_main_sentry_init(self, sentry_init_mock):
        """Test main calls sentry_init."""
        with (
                create_temporary_files(['rcfile', 'ignore']) as tempdir,
                mock.patch('sys.argv',
                           ['__main__.py', 'test', '--rc', 'rcfile', '-i',
                            'ignore', '-o', tempdir,
                            '--no-dump', '--no-upload'])
        ):
            main()
            assert sentry_init_mock.called

    @mock.patch('restraint_wrap.cmd_test.ProvisionData.deserialize_file')
    def test_get_prov_data(self, mock_deserialize):
        """Ensure get_prov_data works."""
        rg_ready = ResourceGroup()
        mock_deserialize.return_value = provs = ProvisionData()
        bkr = provs.get_provisioner('beaker', create=True)
        bkr.rgs = [rg_ready]

        ret_instance_no, ret_provisioners = get_prov_data('whatever')

        self.assertEqual(ret_instance_no, 1)
        self.assertEqual(ret_provisioners, [bkr])

    @mock.patch('restraint_wrap.cmd_test.ProvisionData.deserialize_file')
    def test_get_prov_data_exit(self, mock_deserialize):
        """Ensure get_prov_data does sys.exit(1) if there are no resources."""
        mock_sec = mock.Mock()
        mock_sec.rgs = []
        mock_sec.provisioners = [mock_sec]

        mock_deserialize.side_effect = [mock_sec]

        with self.assertRaises(RuntimeError):
            get_prov_data('whatever')

    def test_convert_path_to_link(self) -> None:
        """Test the behavior of the convert_path_to_link function."""
        self.assertEqual(misc.convert_path_to_link(pathlib.Path('path'), True), 'path')
        self.assertEqual(misc.convert_path_to_link('path', True), 'path')
        self.assertEqual(misc.convert_path_to_link(pathlib.Path('path'), False), 'path')
        self.assertEqual(misc.convert_path_to_link('path', False), 'path')
        with mock.patch.dict(os.environ, {
                'UPT_ARTIFACT_FILE_URL_PREFIX': 'https://url/file/prefix/',
                'UPT_ARTIFACT_DIRECTORY_URL_PREFIX': 'https://url/dir/prefix/',
        }):
            self.assertEqual(misc.convert_path_to_link(pathlib.Path('path'), True),
                             'https://url/file/prefix/path')
            self.assertEqual(misc.convert_path_to_link('path', True),
                             'https://url/file/prefix/path')
            self.assertEqual(misc.convert_path_to_link(pathlib.Path('path'), False),
                             'https://url/dir/prefix/path')
            self.assertEqual(misc.convert_path_to_link('path', False),
                             'https://url/dir/prefix/path')
