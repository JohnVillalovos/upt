"""Test cases for misc module."""
import unittest
from unittest.mock import patch
import xml.etree.ElementTree as ET

from restraint_wrap.restraint_file import CommonXMLElement
from restraint_wrap.restraint_file import RestraintFetch
from restraint_wrap.restraint_file import RestraintJob
from restraint_wrap.restraint_file import RestraintJobDiff
from restraint_wrap.restraint_file import RestraintLog
from restraint_wrap.restraint_file import RestraintParam
from restraint_wrap.restraint_file import RestraintRecipe
from restraint_wrap.restraint_file import RestraintRecipeDiff
from restraint_wrap.restraint_file import RestraintRecipeSet
from restraint_wrap.restraint_file import RestraintRecipeSetDiff
from restraint_wrap.restraint_file import RestraintResult
from restraint_wrap.restraint_file import RestraintTask
from restraint_wrap.restraint_file import RestraintTaskDiff
from restraint_wrap.restraint_file import TestMaintainer
from restraint_wrap.restraint_file import XMLElementAttributesDiff


class TestCommonXMLElement(unittest.TestCase):
    """Test Common Element."""

    def setUp(self):
        self.xml_content = """<element job_id="2" path="/tmp/"></element>"""

    def test_get_xml_attributes(self):
        """Test valid attributes."""
        expected_id = "2"
        expected_path = "/tmp/"
        with patch.object(CommonXMLElement, "_attributes", ['job_id', 'path', 'result']):
            xml_element = ET.fromstring(self.xml_content)
            element = CommonXMLElement(xml_element)
            # Checking values attributes
            self.assertEqual(expected_id, element.job_id)
            self.assertEqual(expected_path, element.path)
            self.assertIsNone(element.result)
            with self.assertRaises(AttributeError):
                element.other_var

    def test_set_xml_attributes(self):
        """Test for writing xml attributes in xml backend."""
        with patch.object(CommonXMLElement, "_attributes", ['job_id', 'path', 'result']):
            xml_element = ET.fromstring(self.xml_content)
            element = CommonXMLElement(xml_element)
            element.result = 'PASS'
            self.assertEqual('PASS', element.element.get('result'))
            element.job_id = '3'
            self.assertEqual('3', element.element.get('job_id'))

    def test_create_from_string(self):
        """Create from string."""
        xml_string = '<element />'
        element = CommonXMLElement.create_from_string(xml_string)
        self.assertEqual(
            ET.canonicalize(xml_string),
            ET.canonicalize(str(element))
        )

    def test_create_from_scratch(self):
        """Create from scratch."""
        element = CommonXMLElement.create_from_scratch()
        self.assertEqual(
            ET.canonicalize('<None />'),
            ET.canonicalize(str(element))
        )

    def test_clear_with_element_to_remove(self):
        """Test xml backend to be sure which data should be stored, with some elements to be removed."""
        xml_string = '<element><delete_me/><other/></element>'
        expected = '<element><other/></element>'
        with patch.object(CommonXMLElement, "_delegated_items", ['delete_me']):
            element = CommonXMLElement.create_from_string(xml_string)
            self.assertEqual(
                ET.canonicalize(expected,  strip_text=True),
                ET.canonicalize(str(element),  strip_text=True)
            )

    def test_clear_withour_element_to_remove(self):
        """Test xml backend to be sure which data should be stored, with no elements to be removed."""
        xml_string = '<element><other/></element>'
        with patch.object(CommonXMLElement, "_delegated_items", ['delete_me']):
            element = CommonXMLElement.create_from_string(xml_string)
            self.assertEqual(
                ET.canonicalize(xml_string,  strip_text=True),
                ET.canonicalize(str(element),  strip_text=True)
            )

    def test_get_diff_attributes(self):
        """Test get_diff_attributes method."""
        with patch.object(CommonXMLElement, "_attributes", ['job_id', 'path']):
            element_1 = CommonXMLElement.create_from_string(self.xml_content)
            element_2 = CommonXMLElement.create_from_string(self.xml_content)
            not_common_xml_element = 'String'

            with self.assertRaisesRegex(TypeError, 'Both objects must be CommonXMLElement'):
                element_1.get_diff_attributes(not_common_xml_element)

            self.assertEqual([], element_1.get_diff_attributes(element_2))

            element_2.path = '/new_path'

            expected = [XMLElementAttributesDiff('path', '/tmp/', '/new_path')]

            self.assertEqual(expected, element_1.get_diff_attributes(element_2))


class TestSimpleElements(unittest.TestCase):
    """Test case to check basic elements."""

    def test_restraint_fetch(self):
        """Test RestraintFetch."""
        url = 'https://server/path'
        xml_expected = '<fetch url="https://server/path" />'
        fetch = RestraintFetch.create_from_scratch()
        fetch.url = url
        self.assertEqual(
            ET.canonicalize(xml_expected),
            ET.canonicalize(str(fetch))
        )

    def test_restraint_log(self):
        """Test RestrainLog."""
        filename = 'app.log'
        path = '/tmp/app.log'
        xml_expected = '<log filename="app.log" path="/tmp/app.log" />'
        log = RestraintLog.create_from_scratch()
        log.filename = filename
        log.path = path
        self.assertEqual(
            ET.canonicalize(xml_expected),
            ET.canonicalize(str(log))
        )

    def test_restraint_param(self):
        """Test RestrainParam."""
        name = 'os'
        value = 'RHEL'
        xml_expected = '<param name="os" value="RHEL" />'
        param = RestraintParam.create_from_scratch()
        param.name = name
        param.value = value
        self.assertEqual(
            ET.canonicalize(xml_expected),
            ET.canonicalize(str(param))
        )


class TestRestraintResult(unittest.TestCase):
    """Test case for result element."""

    def setUp(self):
        """Setup."""
        self.xml_content = """
        <result id="1" path="/distribution/check_install" result="PASS">
          <logs>
            <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
            <log path="recipes/1/tasks/1/results/1/logs/avc.log" filename="avc.log"/>
          </logs>
        </result>
        """

    def test_result(self):
        """Test result."""
        restraint_result = RestraintResult.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_result), strip_text=True)
        )

    def test_text_instance_variable(self):
        """Test instance variable text."""
        xml_with_text = '<result>External Watchdog Expired</result>'
        xml_no_text = '<result></result>'
        self.assertEqual(RestraintResult.create_from_string(
            xml_with_text
        ).text, 'External Watchdog Expired')
        self.assertEqual(RestraintResult.create_from_string(
            xml_no_text
        ).text, '')

    def test_result_from_scratch(self):
        """Test result from scratch."""
        restraint_result = RestraintResult.create_from_scratch()
        restraint_result.id = '1'
        restraint_result.path = '/distribution/check_install'
        restraint_result.result = 'PASS'
        logs = [
            {'path': 'recipes/1/tasks/1/results/1/logs/dmesg.log', 'filename': 'dmesg.log'},
            {'path': 'recipes/1/tasks/1/results/1/logs/avc.log', 'filename': 'avc.log'}
        ]
        for log in logs:
            restraint_log = RestraintLog.create_from_scratch()
            restraint_log.path = log.get('path')
            restraint_log.filename = log.get('filename')
            restraint_result.logs.append(restraint_log)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_result), strip_text=True)
        )
        str(restraint_result)


class TestTestMaintainer(unittest.TestCase):
    """Test TestMaintainer."""

    def test_good_maintainer_with_gitlab(self):
        """Test good maintainer with gitlab."""
        maintainer = TestMaintainer('John', 'john@redhat.com', 'john_gitlab')
        self.assertEqual('John', maintainer.name)
        self.assertEqual('john@redhat.com', maintainer.email)
        self.assertEqual('john_gitlab', maintainer.gitlab)
        self.assertEqual(
            'John <john@redhat.com> / john_gitlab',
            str(maintainer)
        )

    def test_good_maintainer_without_gitlab(self):
        """Test good maintainer without gitlab."""
        maintainer = TestMaintainer('John', 'john@redhat.com')
        self.assertEqual('John', maintainer.name)
        self.assertEqual('john@redhat.com', maintainer.email)
        self.assertIsNone(maintainer.gitlab)
        self.assertEqual(
            'John <john@redhat.com>',
            str(maintainer)
        )

    def test_email_address_maintainer(self):
        """Test bad address."""
        with self.assertRaises(ValueError):
            TestMaintainer('John', 'johnredhat.com', 'john_gitlab')


class TestRestraintTask(unittest.TestCase):
    """Test case for task element."""

    maxDiff = None

    def setUp(self):
        """setup."""
        self.xml_content = """
        <task name="Boot test" role="STANDALONE" id="1" status="Completed" result="PASS" start_time="2022-05-06T20:26:58+0000" end_time="2022-05-06T20:27:56+0000" duration="58">
          <logs>
            <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
            <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
            <log path="recipes/1/tasks/1/logs/io_perf_base_kernel.log" filename="io_perf_base_kernel.log"/>
            <log path="recipes/1/tasks/1/logs/kernel_config.log" filename="kernel_config.log"/>
            <log path="recipes/1/tasks/1/logs/io_perf_cki_kernel.log" filename="io_perf_cki_kernel.log"/>
            <log path="recipes/1/tasks/1/logs/test_console.log" filename="test_console.log"/>
          </logs>
          <fetch url="https://server/kernel-tests-main.zip#distribution/kpkginstall"/>
          <params>
            <param name="CKI_ID" value="3"/>
            <param name="CKI_NAME" value="Boot test"/>
            <param name="CKI_UNIVERSAL_ID" value="boot"/>
            <param name="KPKG_URL" value="https://server/arch#package_name=kernel"/>
            <param name="KILLTIMEOVERRIDE" value="3600"/>
            <param name="STANDALONE" value="host-abc.redhat.com"/>
            <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
            <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
            <param name="CKI_MAINTAINERS" value="Bruno Goncalves &lt;bgoncalv@redhat.com&gt; / bgoncalv, Jeff Bastian &lt;jbastian@redhat.com&gt; / jbastianrh"/>
          </params>
          <results>
            <result id="1" path="distribution/kpkginstall/kernel-in-place" result="PASS">
              <logs>
                <log path="recipes/1/tasks/4/results/1/logs/dmesg.log" filename="dmesg.log"/>
                <log path="recipes/1/tasks/4/results/1/logs/avc.log" filename="avc.log"/>
              </logs>
            </result>
            <result id="2" path="distribution/kpkginstall/reboot" result="PASS">
              <logs>
                <log path="recipes/1/tasks/4/results/2/logs/dmesg.log" filename="dmesg.log"/>
                <log path="recipes/1/tasks/4/results/2/logs/avc.log" filename="avc.log"/>
              </logs>
            </result>
          </results>
        </task>
        """  # noqa

    def test_task(self):
        """Test Task"""
        restraint_task = RestraintTask.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_task), strip_text=True)
        )
        self.assertEqual(6, len(restraint_task.logs))
        # Remember maintainers are not saved as a parameter
        self.assertEqual(8, len(restraint_task.params))
        self.assertEqual(2, len(restraint_task.maintainers))
        self.assertEqual(2, len(restraint_task.results))

    def test_task_from_scratch(self):
        """Test Task from scratch."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.name = 'Boot test'
        restraint_task.role = 'STANDALONE'
        restraint_task.id = '4'
        restraint_task.status = 'Completed'
        restraint_task.result = 'PASS'
        restraint_task.start_time = '2022-05-06T20:26:58+0000'
        restraint_task.end_time = '2022-05-06T20:27:56+0000'
        restraint_task.duration = '58'
        logs = [
            {'path': 'recipes/1/tasks/4/logs/harness.log', 'filename': 'harness.log'},
            {'path': 'recipes/1/tasks/4/logs/taskout.log', 'filename': 'taskout.log'},
            {'path': 'recipes/1/tasks/4/logs/io_perf_base_kernel.log', 'filename': 'io_perf_base_kernel.log'},
            {'path': 'recipes/1/tasks/4/logs/kernel_config.log', 'filename': 'kernel_config.log'},
            {'path': 'recipes/1/tasks/4/logs/io_perf_cki_kernel.log', 'filename': 'io_perf_cki_kernel.log'},
            {'path': 'recipes/1/tasks/4/logs/test_console.log', 'filename': 'test_console.log'}
        ]
        for log in logs:
            restraint_log = RestraintLog.create_from_scratch()
            restraint_log.path = log.get('name')
            restraint_log.filename = log.get('filename')
            restraint_task.logs.append(restraint_log)
        restraint_task.fetch = RestraintFetch.create_from_scratch()
        restraint_task.fetch.url = 'https://server/kernel-tests-main.zip#distribution/kpkginstall'
        params = [
            {'name': 'CKI_ID', 'value': '3'},
            {'name': 'CKI_NAME', 'value': 'Boot test'},
            {'name': 'CKI_UNIVERSAL_ID', 'value': 'boot'},
            {'name': 'KPKG_URL', 'value': 'https://server/arch#package_name=kernel'},
            {'name:' 'KILLTIMEOVERRIDE' 'value': '3600'},
            {'name': 'STANDALONE', 'value': 'host-abc.redhat.com'},
            {'name': 'JOB_MEMBERS', 'value': 'host-abc.redhat.com'},
            {'name': 'RECIPE_MEMBERS', 'value': 'host-abc.redhat.com'}
        ]
        for param in params:
            restraint_task.create_or_update_paramater(param.get('name'), param.get('value'))
        maintainers = [
            {'name': 'Bruno Goncalves', 'email': 'bgoncalv@redhat.com', 'gitlab': 'bgoncalv'},
            {'name': 'Jeff Bastian', 'email': 'jbastian@redhat.com', 'gitlab': 'jbastianrh'}
        ]
        for maintainer in maintainers:
            restraint_task.create_or_update_maintainer(
                maintainer.get('name'), maintainer.get('email'), maintainer.get('gitlab')
            )
        results = [
            {'id': '1', 'path': 'distribution/kpkginstall/kernel-in-place',
             'result': 'PASS', 'logs': [
                 {'path': 'recipes/1/tasks/4/results/1/logs/dmesg.log', 'filename': 'dmesg.log'},
                 {'path': 'recipes/1/tasks/4/results/1/logs/avc.log', 'filename': 'avc.log'}
             ]
             },
            {'id': '2', 'path': 'distribution/kpkginstall/reboot',
             'result': 'PASS', 'logs': [
                 {'path': 'recipes/1/tasks/4/results/2/logs/dmesg.log', 'filename': 'dmesg.log'},
                 {'path': 'recipes/1/tasks/4/results/2/logs/avc.log', 'filename': 'avc.log'}
             ]
             }
        ]

        for result in results:
            restraint_result = RestraintResult.create_from_scratch()
            restraint_result.id = result.get('id')
            restraint_result.path = result.get('path')
            restraint_result.result = result.get('result')
            for log in result.get('logs'):
                restraint_log = RestraintLog.create_from_scratch()
                restraint_log.path = log.get('path')
                restraint_log.file = log.get('filename')
                restraint_result.logs.append(restraint_log)
            restraint_task.results.append(restraint_result)

        restraint_task = RestraintTask.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_task), strip_text=True)
        )

    def test_creating_and_updating_a_parameter(self):
        """Test Task create or update a parameter."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.create_or_update_paramater('CKI_TEST', '1234')
        self.assertEqual('1234', restraint_task.get_param_value_by_name('CKI_TEST'))
        restraint_task.create_or_update_paramater('CKI_TEST', '5678')
        self.assertEqual('5678', restraint_task.get_param_value_by_name('CKI_TEST'))
        self.assertEqual(1, len(restraint_task.params))

    def test_creating_and_updating_a_log(self):
        """Test Task create or update a log."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.create_or_update_log('http://server/file.log', '1234')
        restraint_task.create_or_update_log('http://server/file.log', '1234')
        self.assertEqual(1, len(restraint_task.logs))

    def test_creating_and_updating_a_maintainer(self):
        """Test Task create or update a log."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.create_or_update_maintainer('Bruno', 'bgoncalv@redhat.com')
        restraint_task.create_or_update_maintainer('Bruno', 'bgoncalv@redhat.com', 'bgoncalv')
        self.assertEqual(1, len(restraint_task.maintainers))
        restraint_task.create_or_update_maintainer('Jeff Bastian', 'jbastian@redhat.com', 'jbastianrh')
        restraint_task.create_or_update_maintainer('Jeff Bastian', 'jbastian@redhat.com', 'jbastianrh')
        self.assertEqual(2, len(restraint_task.maintainers))

    def test_is_cki_test(self):
        """Test is a task is a cki test."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.create_or_update_paramater('CKI_UNIVERSAL_ID', '123')
        # self.assertFalse(restraint_task.is_cki_test)
        restraint_task.create_or_update_paramater('CKI_NAME', '123')
        self.assertTrue(restraint_task.is_cki_test)

    def test_is_boot_task(self):
        """Check if the task is a boot task."""
        restraint_task = RestraintTask.create_from_scratch()
        restraint_task.create_or_update_paramater('CKI_UNIVERSAL_ID', '123')
        self.assertFalse(restraint_task.is_cki_test)
        restraint_task.name = 'Boot Test'
        self.assertTrue(restraint_task.is_boot_task)
        restraint_task.name = 'Another name'
        restraint_task.create_or_update_paramater('CKI_UNIVERSAL_ID', 'boot')
        self.assertTrue(restraint_task.is_boot_task)

    def test_task_get_result_by_id(self):
        """Test get_result_by_id method."""
        restraint_task = RestraintTask.create_from_string(self.xml_content)
        self.assertEqual(restraint_task.get_result_by_id('1').path,
                         "distribution/kpkginstall/kernel-in-place")
        self.assertEqual(restraint_task.get_result_by_id('2').path,
                         "distribution/kpkginstall/reboot")
        self.assertIsNone(restraint_task.get_result_by_id('foobar'))

    def test_diff_tasks(self):
        """Get diffs between tasks."""
        restraint_task_1 = RestraintTask.create_from_scratch()
        restraint_task_2 = RestraintTask.create_from_scratch()
        restraint_log = RestraintLog.create_from_scratch()

        with self.assertRaisesRegex(TypeError, 'Both objects must be RestraintTask'):
            restraint_task_1.diff(restraint_log)

        with self.assertRaisesRegex(ValueError, 'id must be defined in both RestraintTasks'):
            restraint_task_1.diff(restraint_task_2)

        restraint_task_1.id = 1
        restraint_task_2.id = 2

        with self.assertRaisesRegex(ValueError, 'Tasks do not have the same id'):
            restraint_task_1.diff(restraint_task_2)

        restraint_task_2.id = 1

        self.assertIsNone(restraint_task_1.diff(restraint_task_2))

        restraint_task_2.status = 'Completed'

        changes = [XMLElementAttributesDiff('status', None, 'Completed')]

        expected = RestraintTaskDiff(1, changes)

        self.assertEqual(restraint_task_1.diff(restraint_task_2), expected)


class TestRestraintRecipe(unittest.TestCase):
    """Test case for recipe element."""

    maxDiff = None

    def setUp(self):
        """setup."""
        self.xml_content = """
        <recipe id="1" status="Completed" result="WARN" job_id="1">
          <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-06T20:14:07+0000" end_time="2022-05-06T20:14:23+0000" duration="16">
            <logs>
              <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
              <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
            </logs>
            <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
            <params>
              <param name="CKI_ID" value="1"/>
              <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
              <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
              <param name="CKI_WAIVED" value="True"/>
              <param name="None" value="host-abc.redhat.com"/>
              <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
              <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
            </params>
            <results>
              <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
                <logs>
                  <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
                </logs>
              </result>
            </results>
          </task>
          <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-06T20:14:23+0000" end_time="2022-05-06T20:14:41+0000" duration="18">
            <logs>
              <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
              <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
            </logs>
            <fetch url="https://git-server/archive/master.zip#check-install"/>
            <params>
              <param name="STANDALONE" value="host-abc.redhat.com"/>
              <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
              <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
            </params>
            <results>
              <result id="1" path="/distribution/check-install" result="PASS">
                <logs>
                  <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
                  <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
                </logs>
              </result>
              <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
                <logs>
                  <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
                </logs>
              </result>
            </results>
          </task>
        </recipe>
        """  # noqa

    def test_recipe(self):
        """Test Recipe"""
        restraint_recipe = RestraintRecipe.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_recipe), strip_text=True)
        )
        self.assertEqual(2, len(restraint_recipe.tasks))

    def test_recipe_from_scratch(self):
        """Test Recipe from scratch."""
        restraint_recipe = RestraintRecipe.create_from_scratch()
        restraint_recipe.id = '1'
        restraint_recipe.status = 'Completed'
        restraint_recipe.result = 'WARN'
        restraint_recipe.job_id = '1'
        restraint_recipe.tasks = RestraintRecipe.create_from_string(self.xml_content).tasks
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_recipe), strip_text=True)
        )
        self.assertEqual(2, len(restraint_recipe.tasks))

    def test_recipe_system_attribute(self):
        """Test Recipe has a system attribute."""
        rs = RestraintRecipe.create_from_string('<recipe />')
        self.assertIsNone(rs.system)
        rs = RestraintRecipe.create_from_string('<recipe system="foo@bar.com"/>')
        self.assertEqual('foo@bar.com', rs.system)

    def test_recipe_get_result_by_id(self):
        """Test get_result_by_id method."""
        restraint_recipe = RestraintRecipe.create_from_string(self.xml_content)
        self.assertEqual(restraint_recipe.get_result_by_id('1').path,
                         "/kernel/distribution/selinux-custom-modules")
        self.assertIsNone(restraint_recipe.get_result_by_id('foobar'))

    def test_diff_recipes(self):
        """Get diffs between recipes."""
        restraint_recipe_1 = RestraintRecipe.create_from_scratch()
        restraint_recipe_2 = RestraintRecipe.create_from_scratch()
        restraint_task_1 = RestraintTask.create_from_scratch()
        restraint_task_2 = RestraintTask.create_from_scratch()

        with self.assertRaisesRegex(TypeError, 'Both objects must be RestraintRecipe'):
            restraint_recipe_1.diff(restraint_task_1)

        with self.assertRaisesRegex(ValueError, 'id must be defined in both RestraintRecipes'):
            restraint_recipe_1.diff(restraint_recipe_2)

        restraint_recipe_1.id = '1'
        restraint_recipe_2.id = '2'

        with self.assertRaisesRegex(ValueError, 'RestraintRecipes do not have the same id'):
            restraint_recipe_1.diff(restraint_recipe_2)

        restraint_recipe_2.id = '1'

        self.assertIsNone(restraint_recipe_1.diff(restraint_recipe_2))

        restraint_recipe_2.status = 'Completed'

        expected = RestraintRecipeDiff('1',
                                       [XMLElementAttributesDiff('status', None, 'Completed')],
                                       [])

        self.assertEqual(restraint_recipe_1.diff(restraint_recipe_2), expected)

        restraint_task_1.id = '1'
        restraint_task_2.id = '1'

        restraint_recipe_1.tasks.append(restraint_task_1)
        restraint_recipe_2.tasks.append(restraint_task_2)

        self.assertEqual(restraint_recipe_1.diff(restraint_recipe_2), expected)

        restraint_task_2.status = 'Completed'

        diff = restraint_recipe_1.diff(restraint_recipe_2)

        self.assertIsInstance(diff, RestraintRecipeDiff)

        # I don't want to check the format of task diff, the format is checked in the task tests
        self.assertTrue(len(diff.tasks) > 0)


class TestRestraintRecipeSet(unittest.TestCase):
    """RecipeSet Tests."""

    maxDiff = None

    def setUp(self):
        """Setup."""
        self.xml_content = """
        <recipeSet>
          <recipe id="1" status="Completed" result="PASS" checkpoint_file="checkpoint.conf" job_id="1">
            <task name="SELinux Custom Module Setup" keepchanges="" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:40+0000" end_time="2022-05-17T14:56:02+0000" duration="22">
              <logs>
                <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
                <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
              </logs>
              <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
              <params>
                <param name="CKI_ID" value="33"/>
                <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
                <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
                <param name="CKI_WAIVED" value="True"/>
                <param name="None" value="host-abc.redhat.com"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
                  <logs>
                    <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
                  </logs>
                </result>
              </results>
            </task>
            <task name="/distribution/check-install" keepchanges="" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:56:03+0000" end_time="2022-05-17T14:56:21+0000" duration="18">
              <logs>
                <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
                <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
                <log path="recipes/1/tasks/2/logs/test_console.log" filename="test_console.log"/>
              </logs>
              <fetch url="https://git-server/archive/master.zip#check-install"/>
              <params>
                <param name="STANDALONE" value="host-abc.redhat.com"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="/distribution/check-install" result="PASS" score="850">
                  <logs>
                    <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
                <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
                  <logs>
                    <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
              </results>
            </task>
            <task name="/test/misc/machineinfo" keepchanges="" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:24+0000" end_time="2022-05-17T14:56:44+0000" duration="20">
              <logs>
                <log path="recipes/1/tasks/3/logs/harness.log" filename="harness.log"/>
                <log path="recipes/1/tasks/3/logs/taskout.log" filename="taskout.log"/>
                <log path="recipes/1/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
                <log path="recipes/1/tasks/3/logs/lshw.log" filename="lshw.log"/>
                <log path="recipes/1/tasks/3/logs/df.log" filename="df.log"/>
                <log path="recipes/1/tasks/3/logs/mount.log" filename="mount.log"/>
                <log path="recipes/1/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
                <log path="recipes/1/tasks/3/logs/test_console.log" filename="test_console.log"/>
              </logs>
              <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
              <params>
                <param name="CKI_ID" value="34"/>
                <param name="CKI_NAME" value="machineinfo"/>
                <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
                <param name="CKI_WAIVED" value="True"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="test/misc/machineinfo" result="PASS" score="0">
                  <logs>
                    <log path="recipes/1/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    <log path="recipes/1/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
              </results>
            </task>
          </recipe>
          <recipe id="2" status="Completed" result="FAIL" checkpoint_file="checkpoint.conf" job_id="1">
            <task name="SELinux Custom Module Setup" keepchanges="" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:38+0000" end_time="2022-05-17T14:55:55+0000" duration="17">
              <logs>
                <log path="recipes/2/tasks/1/logs/harness.log" filename="harness.log"/>
                <log path="recipes/2/tasks/1/logs/taskout.log" filename="taskout.log"/>
                <log path="recipes/2/tasks/1/logs/test_console.log" filename="test_console.log"/>
              </logs>
              <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
              <params>
                <param name="CKI_ID" value="39"/>
                <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
                <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
                <param name="CKI_WAIVED" value="True"/>
                <param name="None" value="host-abc.redhat.com"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
                  <logs>
                    <log path="recipes/2/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
                  </logs>
                </result>
              </results>
            </task>
            <task name="/distribution/check-install" keepchanges="" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:55:56+0000" end_time="2022-05-17T14:56:13+0000" duration="17">
              <logs>
                <log path="recipes/2/tasks/2/logs/harness.log" filename="harness.log"/>
                <log path="recipes/2/tasks/2/logs/taskout.log" filename="taskout.log"/>
                <log path="recipes/2/tasks/2/logs/test_console.log" filename="test_console.log"/>
              </logs>
              <fetch url="https://git-server/archive/master.zip#check-install"/>
              <params>
                <param name="STANDALONE" value="host-abc.redhat.com"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="/distribution/check-install" result="PASS" score="851">
                  <logs>
                    <log path="recipes/2/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    <log path="recipes/2/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
                <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
                  <logs>
                    <log path="recipes/2/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
              </results>
            </task>
            <task name="/test/misc/machineinfo" keepchanges="" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:17+0000" end_time="2022-05-17T14:56:36+0000" duration="19">
              <logs>
                <log path="recipes/2/tasks/3/logs/harness.log" filename="harness.log"/>
                <log path="recipes/2/tasks/3/logs/taskout.log" filename="taskout.log"/>
                <log path="recipes/2/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
                <log path="recipes/2/tasks/3/logs/lshw.log" filename="lshw.log"/>
                <log path="recipes/2/tasks/3/logs/df.log" filename="df.log"/>
                <log path="recipes/2/tasks/3/logs/mount.log" filename="mount.log"/>
                <log path="recipes/2/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
                <log path="recipes/2/tasks/3/logs/test_console.log" filename="test_console.log"/>
              </logs>
              <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
              <params>
                <param name="CKI_ID" value="40"/>
                <param name="CKI_NAME" value="machineinfo"/>
                <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
                <param name="CKI_WAIVED" value="True"/>
                <param name="JOB_MEMBERS" value="host-abc.redhat.com"/>
                <param name="RECIPE_MEMBERS" value="host-abc.redhat.com"/>
              </params>
              <results>
                <result id="1" path="test/misc/machineinfo" result="PASS" score="0">
                  <logs>
                    <log path="recipes/2/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    <log path="recipes/2/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
                  </logs>
                </result>
              </results>
            </task>
          </recipe>
        </recipeSet>
        """  # noqa

    def test_recipeset(self):
        """Test RecipeSet."""

        restraint_recipeset = RestraintRecipeSet.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_recipeset), strip_text=True)
        )
        self.assertEqual(2, len(restraint_recipeset.recipes))

    def test_recipeset_from_scratch(self):
        """Test RecipeSet from scratch."""

        restraint_recipeset = RestraintRecipeSet.create_from_scratch()
        restraint_recipeset.recipes = RestraintRecipeSet.create_from_string(self.xml_content).recipes
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_recipeset), strip_text=True)
        )
        self.assertEqual(2, len(restraint_recipeset.recipes))

    def test_recipeset_id_attribute(self):
        """Test RecipeSet has an id attribute."""
        rs = RestraintRecipeSet.create_from_string('<recipeSet />')
        self.assertIsNone(rs.id)
        rs = RestraintRecipeSet.create_from_string('<recipeSet id="456"/>')
        self.assertEqual('456', rs.id)

    def test_recipeset_get_recipe_by_id(self):
        """Test get_recipe_by_id method."""
        restraint_recipeset = RestraintRecipeSet.create_from_string(self.xml_content)
        self.assertIsNotNone(restraint_recipeset.get_recipe_by_id(1))
        self.assertIsNone(restraint_recipeset.get_recipe_by_id(999))

    def test_diff_recipesets(self):
        """Get diffs between recipesets."""
        restraint_recipeset_1 = RestraintRecipeSet.create_from_scratch()
        restraint_recipeset_2 = RestraintRecipeSet.create_from_scratch()
        restraint_recipe_1 = RestraintRecipe.create_from_scratch()
        restraint_recipe_2 = RestraintRecipe.create_from_scratch()

        with self.assertRaisesRegex(TypeError, 'Both objects must be RestraintRecipeSet'):
            restraint_recipeset_1.diff(restraint_recipe_1)

        self.assertIsNone(restraint_recipeset_1.diff(restraint_recipeset_2))

        restraint_recipe_1.id = '1'
        restraint_recipe_2.id = '1'

        restraint_recipeset_1.recipes.append(restraint_recipe_1)
        restraint_recipeset_2.recipes.append(restraint_recipe_2)

        self.assertIsNone(restraint_recipeset_1.diff(restraint_recipeset_2))

        restraint_recipe_2.status = 'Completed'

        diff = restraint_recipeset_1.diff(restraint_recipeset_2)

        self.assertIsInstance(diff, RestraintRecipeSetDiff)

        self.assertIsNone(diff.id)
        # RecipeSet only has id attribute, so changes always should be empty
        self.assertEqual(len(diff.changes), 0)
        # I don't want to check the format of recipe diff, the format is checked in the recipe tests
        self.assertTrue(len(diff.recipes) > 0)
        self.assertIsInstance(diff.recipes[0], RestraintRecipeDiff)


class TestRestraintJob(unittest.TestCase):
    """Jobs Tests."""

    maxDiff = None

    def setUp(self):
        """Setup."""
        self.xml_content = """
        <job>
          <recipeSet>
            <recipe id="1" status="Completed" result="PASS" job_id="1">
              <task name="SELinux Custom Module Setup" keepchanges="" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:40+0000" end_time="2022-05-17T14:56:02+0000" duration="22">
                <logs>
                  <log path="recipes/1/tasks/1/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/1/tasks/1/logs/taskout.log" filename="taskout.log"/>
                </logs>
                <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
                <params>
                  <param name="CKI_ID" value="33"/>
                  <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
                  <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
                  <param name="CKI_WAIVED" value="True"/>
                  <param name="None" value="rhost-abc.redhat.com"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
                    <logs>
                      <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    </logs>
                  </result>
                </results>
              </task>
              <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:56:03+0000" end_time="2022-05-17T14:56:21+0000" duration="18">
                <logs>
                  <log path="recipes/1/tasks/2/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/1/tasks/2/logs/taskout.log" filename="taskout.log"/>
                  <log path="recipes/1/tasks/2/logs/test_console.log" filename="test_console.log"/>
                </logs>
                <fetch url="https://git-server/archive/master.zip#check-install"/>
                <params>
                  <param name="STANDALONE" value="rhost-abc.redhat.com"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="/distribution/check-install" result="PASS">
                    <logs>
                      <log path="recipes/1/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
                      <log path="recipes/1/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                  <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
                    <logs>
                      <log path="recipes/1/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                </results>
              </task>
              <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:24+0000" end_time="2022-05-17T14:56:44+0000" duration="20">
                <logs>
                  <log path="recipes/1/tasks/3/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/1/tasks/3/logs/taskout.log" filename="taskout.log"/>
                  <log path="recipes/1/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
                  <log path="recipes/1/tasks/3/logs/lshw.log" filename="lshw.log"/>
                  <log path="recipes/1/tasks/3/logs/df.log" filename="df.log"/>
                  <log path="recipes/1/tasks/3/logs/mount.log" filename="mount.log"/>
                  <log path="recipes/1/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
                  <log path="recipes/1/tasks/3/logs/test_console.log" filename="test_console.log"/>
                </logs>
                <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
                <params>
                  <param name="CKI_ID" value="34"/>
                  <param name="CKI_NAME" value="machineinfo"/>
                  <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
                  <param name="CKI_WAIVED" value="True"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="test/misc/machineinfo" result="PASS" score="0">
                    <logs>
                      <log path="recipes/1/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
                      <log path="recipes/1/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                </results>
              </task>
            </recipe>
            <recipe id="2" status="Completed" result="FAIL" job_id="1">
              <task name="SELinux Custom Module Setup" role="None" id="1" status="Completed" result="None" start_time="2022-05-17T14:55:38+0000" end_time="2022-05-17T14:55:55+0000" duration="17">
                <logs>
                  <log path="recipes/2/tasks/1/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/2/tasks/1/logs/taskout.log" filename="taskout.log"/>
                  <log path="recipes/2/tasks/1/logs/test_console.log" filename="test_console.log"/>
                </logs>
                <fetch url="https://server/kernel-tests-main.zip#distribution/selinux-custom-modules"/>
                <params>
                  <param name="CKI_ID" value="39"/>
                  <param name="CKI_NAME" value="SELinux Custom Module Setup"/>
                  <param name="CKI_UNIVERSAL_ID" value="selinux_custom_module_setup"/>
                  <param name="CKI_WAIVED" value="True"/>
                  <param name="None" value="rhost-abc.redhat.com"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="/kernel/distribution/selinux-custom-modules" result="SKIP">
                    <logs>
                      <log path="recipes/2/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
                    </logs>
                  </result>
                </results>
              </task>
              <task name="/distribution/check-install" role="STANDALONE" id="2" status="Completed" result="PASS" start_time="2022-05-17T14:55:56+0000" end_time="2022-05-17T14:56:13+0000" duration="17">
                <logs>
                  <log path="recipes/2/tasks/2/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/2/tasks/2/logs/taskout.log" filename="taskout.log"/>
                  <log path="recipes/2/tasks/2/logs/test_console.log" filename="test_console.log"/>
                </logs>
                <fetch url="https://git-server/archive/master.zip#check-install"/>
                <params>
                  <param name="STANDALONE" value="rhost-abc.redhat.com"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="/distribution/check-install" result="PASS">
                    <logs>
                      <log path="recipes/2/tasks/2/results/1/logs/dmesg.log" filename="dmesg.log"/>
                      <log path="recipes/2/tasks/2/results/1/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                  <result id="2" path="/distribution/check-install/Sysinfo" result="PASS">
                    <logs>
                      <log path="recipes/2/tasks/2/results/2/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                </results>
              </task>
              <task name="/test/misc/machineinfo" id="3" status="Completed" result="PASS" start_time="2022-05-17T14:56:17+0000" end_time="2022-05-17T14:56:36+0000" duration="19">
                <logs>
                  <log path="recipes/2/tasks/3/logs/harness.log" filename="harness.log"/>
                  <log path="recipes/2/tasks/3/logs/taskout.log" filename="taskout.log"/>
                  <log path="recipes/2/tasks/3/logs/machinedesc.log" filename="machinedesc.log"/>
                  <log path="recipes/2/tasks/3/logs/lshw.log" filename="lshw.log"/>
                  <log path="recipes/2/tasks/3/logs/df.log" filename="df.log"/>
                  <log path="recipes/2/tasks/3/logs/mount.log" filename="mount.log"/>
                  <log path="recipes/2/tasks/3/logs/installedpkgs.log" filename="installedpkgs.log"/>
                  <log path="recipes/2/tasks/3/logs/test_console.log" filename="test_console.log"/>
                </logs>
                <fetch url="https://server/kernel-tests-main.zip#test/misc/machineinfo"/>
                <params>
                  <param name="CKI_ID" value="40"/>
                  <param name="CKI_NAME" value="machineinfo"/>
                  <param name="CKI_UNIVERSAL_ID" value="machineinfo"/>
                  <param name="CKI_WAIVED" value="True"/>
                  <param name="JOB_MEMBERS" value="rhost-abc.redhat.com"/>
                  <param name="RECIPE_MEMBERS" value="rhost-abc.redhat.com"/>
                </params>
                <results>
                  <result id="1" path="test/misc/machineinfo" result="PASS">
                    <logs>
                      <log path="recipes/2/tasks/3/results/1/logs/dmesg.log" filename="dmesg.log"/>
                      <log path="recipes/2/tasks/3/results/1/logs/avc.log" filename="avc.log"/>
                    </logs>
                  </result>
                </results>
              </task>
            </recipe>
          </recipeSet>
        </job>
        """  # noqa

    def test_job(self):
        """Test Job."""

        restraint_job = RestraintJob.create_from_string(self.xml_content)
        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_job), strip_text=True)
        )
        self.assertEqual(1, len(restraint_job.recipesets))

    def test_job_from_scratch(self):
        """Test Job from scratch."""

        restraint_job = RestraintJob.create_from_scratch()
        restraint_job.recipesets = RestraintJob.create_from_string(self.xml_content).recipesets

        self.assertEqual(
            ET.canonicalize(self.xml_content, strip_text=True),
            ET.canonicalize(str(restraint_job), strip_text=True)
        )
        self.assertEqual(1, len(restraint_job.recipesets))

    def test_job_group_attribute(self):
        """Test Job has a group attribute."""
        job = RestraintJob.create_from_string('<job />')
        self.assertIsNone(job.group)
        job = RestraintJob.create_from_string('<job group="cki" />')
        self.assertEqual('cki', job.group)

    def test_job_get_recipe_by_id(self):
        """Test get_recipe_by_id method."""
        restraint_job = RestraintJob.create_from_string(self.xml_content)
        self.assertIsNotNone(restraint_job.get_recipe_by_id(1))
        self.assertIsNone(restraint_job.get_recipe_by_id(999))

    def test_job_get_all_recipes(self):
        """Test get_all_recipes method."""
        restraint_job = RestraintJob.create_from_string(self.xml_content)
        self.assertEqual(2, len(restraint_job.get_all_recipes()))

    def test_diff_recipesets(self):
        """Get diffs between jobs."""
        restraint_job_1 = RestraintJob.create_from_scratch()
        restraint_job_2 = RestraintJob.create_from_scratch()
        restraint_recipeset_1 = RestraintRecipeSet.create_from_scratch()
        restraint_recipeset_2 = RestraintRecipeSet.create_from_scratch()
        restraint_recipe_1 = RestraintRecipe.create_from_scratch()
        restraint_recipe_2 = RestraintRecipe.create_from_scratch()

        with self.assertRaisesRegex(TypeError, 'Both objects must be RestraintJob'):
            restraint_job_1.diff(restraint_recipeset_1)

        self.assertIsNone(restraint_job_1.diff(restraint_job_2))

        restraint_recipe_1.id = '1'
        restraint_recipe_2.id = '1'

        restraint_recipeset_1.recipes.append(restraint_recipe_1)
        restraint_recipeset_2.recipes.append(restraint_recipe_2)

        restraint_job_1.recipesets.append(restraint_recipeset_1)
        restraint_job_2.recipesets.append(restraint_recipeset_2)

        self.assertIsNone(restraint_job_1.diff(restraint_job_2))

        restraint_recipe_2.status = 'Completed'

        diff = restraint_job_1.diff(restraint_job_2)

        self.assertIsInstance(diff, RestraintJobDiff)

        self.assertIsNone(diff.group)
        # RecipeSet only has group attribute, so changes always should be empty
        self.assertEqual(len(diff.changes), 0)
        # I don't want to check the format of recipeset diff, the format is checked in the recipeset tests
        self.assertTrue(len(diff.recipesets) > 0)
        self.assertIsInstance(diff.recipesets[0], RestraintRecipeSetDiff)
