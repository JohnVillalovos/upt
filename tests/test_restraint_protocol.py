"""Test cases for restraint_wrap/restraint_protocol.py module."""
import copy
import itertools
import os
import pathlib
import shlex
import unittest
from unittest import mock

from twisted.internet.error import ProcessExitedAlready
from twisted.internet.error import ReactorNotRunning

from plumbing.format import ProvisionData
from restraint_wrap.actions import ActionOnResult
from restraint_wrap.restraint_file.task_element import RestraintTask
from restraint_wrap.restraint_host import RestraintHost
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.task_result import TaskResult
from tests.const import ASSETS_DIR
from tests.utils import create_temporary_files
from upt.logger import COLORS

# pylint: disable=no-self-use,protected-access


class TestRestraintProtocol(unittest.TestCase):
    # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """Ensure RestraintClientProcessProtocol works."""

    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        self.std_kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                           'dump': True}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'restraint_xml')
        self.xml = self.xmlpath.read_text()

        self.mock_rs_open = mock.patch('restraint_wrap.restraint_shell.open', create=True)
        self.mock_rs_open.start()

        self.beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a', 'dump': False}
        self.proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                                    **kwargs)
        for i, host in enumerate(self.proto.resource_group.recipeset.hosts, 1):
            host.counter = mock.Mock()
            host.counter.path = f'results_{i:04d}'

        self.proto.run_suffix = 'job.01'

    def tearDown(self) -> None:
        self.mock_rs_open.stop()

    @mock.patch('upt.logger.LOGGER.error')
    def test_get_restraint_xml_task(self, mock_error):
        """Ensure get_restraint_xml_task works."""
        proto = self.proto
        result = proto.get_restraint_xml_task(123, 1)
        self.assertIsNotNone(result)

        # Task that doesn't exist -> None + error print.
        self.assertIsNone(proto.get_restraint_xml_task(1, 5000))
        mock_error.assert_called_with('task T:%i not found in recipe %s!', 5000, 1)

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('restraint_wrap.restraint_protocol.misc.enter_dir')
    def test_start_all(self, mock_enter, mock_reactor):
        """Ensure start_all calls what it should."""
        mock_enter.return_value.__enter__ = mock.Mock()
        proto = self.proto
        mock_reactor.spawnProcess.return_value = 'spawned'
        mock_reactor.addSystemEventTrigger.return_value = 'trigger-set'

        proto.start_all()

        mock_reactor.spawnProcess.assert_called_with(proto, proto.restraint_binary,
                                                     shlex.split(proto.wrap.restraint_commands),
                                                     env=os.environ)
        mock_reactor.addSystemEventTrigger.assert_called()

    @mock.patch('restraint_wrap.watcher.Semaphore', mock.Mock())
    def test_filter_output(self):
        """Ensure filter_output works."""
        # Check that identify_host extracts hostname + recipe_id.
        self.assertIsNone(self.proto.filter_output('Disconnected..'))
        self.assertIsNone(self.proto.filter_output('Connecting to host: root@hostname1, recipe id:123'))
        rst_host = self.proto.rst_hosts[0]
        self.assertEqual(rst_host.hostname, 'hostname1')
        self.assertEqual(rst_host.recipe_id, 123)

        self.assertIsNone(self.proto.filter_output('Connecting to host: root@a.b.com, recipe id:123'))

    def test_identify_host(self):
        """Ensure identify_host works."""
        self.assertTrue(self.proto.identify_host('Connecting to host: root@a.b.com, recipe id:1234'))
        self.assertFalse(self.proto.identify_host('invalid line'))

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.twisted_task')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_out_received(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived works."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data\n')
            mock_debug.assert_called_with('some data\n')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_called_with('some data')
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.twisted_task')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_out_received_incomplete(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived doesn't process incomplete lines."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data')
            mock_debug.assert_called_with('some data')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_not_called()
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.twisted_task')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_out_received_extra_part(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived handles extra partial lines correctly."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data\nextra')
            mock_debug.assert_called_with('some data\nextra')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_called_with('some data')
            mock_defer.Deferred.assert_called()
            self.assertEqual(self.proto.unprocessed_out_data, 'extra')

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.twisted_task')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_out_received_disconnect(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived detects disconnects and calls host_heartbeat."""
        with mock.patch.object(self.proto, 'host_heartbeat') as mock_heartbeat:
            self.proto.outReceived(b'Disconnected.. delaying 60 seconds. Retry 15/15.\n')
            mock_debug.assert_called_with('Disconnected.. delaying 60 seconds. Retry 15/15.\n')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_heartbeat.assert_called()
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_handle_failure(self, mock_warn):
        """Ensure handle_failure works."""
        mock_failure = mock.Mock()

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            self.proto.handle_failure(mock_failure)
            mock_proc.signalProcess.assert_called_with('TERM')
            mock_warn.assert_called_with("Fatal exception caught %s", mock_failure.getTraceback())

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            mock_proc.signalProcess.side_effect = itertools.chain([ProcessExitedAlready()],
                                                                  itertools.cycle([None]))
            self.proto.handle_failure(mock_failure)

        # Host died
        with mock.patch.object(self.proto, 'proc') as mock_proc:
            self.proto.handle_failure()
            mock_warn.assert_called_with('Host died, killing restraint process!')

    @mock.patch('upt.logger.LOGGER.print_result_in_color')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_check_stderr(self, mock_debug, mock_color):
        """Ensure _check_stderr detects EWD hit and executes an action that matches such conditions."""
        host = self.proto.resource_group.recipeset.hosts[0]
        with mock.patch.object(self.proto.watcher, 'get_subtask_results', lambda *args: set()):
            with mock.patch.object(self.proto.watcher, 'get_lwd_hits', lambda *args: set()):
                with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
                    # The mock below is pretty important: with no prior results, we assume the task aborted.
                    with mock.patch.object(host, 'task_results', []):
                        mock_sync.return_value.__enter__.return_value.name = 'whatever'
                        self.proto._check_stderr('Recipe 123 exceeded lab watchdog timer')

        self.assertIn(123, self.proto.recipe_ids_dead)
        mock_color.assert_any_call('*** Recipe 123 hit lab watchdog!')

        msg2 = ActionOnResult.format_msg(123, 1,
                                         'recipe is done processing [mark_recipe_done]', '*', 'abc')
        mock_debug.assert_called_with(msg2)

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error')
    def test_check_stderr_fatal(self, mock_error, mock_reactor):
        """Ensure _check_stderr works."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        self.proto._check_stderr(err_indication)
        mock_error.assert_called_with("Exiting because: %s", err_indication)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_no_reactor(self, mock_reactor):
        """Ensure _check_stderr doesn't stop reactor when it's not running."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = False
        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_not_called()

    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_exc_handled(self, mock_reactor):
        """Ensure _check_stderr handles reactor exception."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        mock_reactor.callFromThread.side_effect = itertools.chain([ReactorNotRunning()], itertools.cycle([None]))

        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.defer')
    def test_err_received(self, mock_defer, mock_debug):
        """Ensure errReceived works."""
        self.proto.errReceived(b'some error')
        mock_debug.assert_called_with('some error')
        mock_defer.Deferred.assert_called()

        self.proto.errReceived(b'')

    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.identify_host', lambda *a: False)
    @mock.patch('restraint_wrap.restraint_protocol.reactor')
    def test_filter_output_with_path(self, mock_reactor):
        """Ensure filter_output works."""
        result = self.proto.filter_output('Using ./job.01 for job run')

        path2watch = os.path.join(f'{os.path.abspath(self.proto.kwargs["output"])}',
                                  os.path.relpath(self.proto.run_suffix))

        mock_reactor.callInThread.assert_called_with(self.proto.watcher.run, path2watch)
        self.assertIsNone(result)

    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.identify_host', lambda *a: False)
    @mock.patch('restraint_wrap.restraint_protocol.reactor', mock.Mock())
    def test_filter_output_host_line(self):
        """Ensure filter_output process host lines."""
        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        result = self.proto.filter_output(line)

        self.assertEqual(result, [('aaaa-bbbbbb-01.ccccc', 'T:       1 [test1             ] Running')])

        self.assertIsNone(self.proto.filter_output('err'))

    def test_find_host_lines(self):
        """Ensure find_host_lines works."""
        # Create new protocol without watcher mocked.
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'

        self.assertIsNone(proto.find_host_lines('blah'))
        proto.rst_hosts.append(RestraintHost('aaaa-bbbbbb-01.ccccc', 123))
        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        self.assertIsNone(proto.find_host_lines(line))

    @mock.patch('upt.logger.LOGGER.info')
    def test_find_host_lines_fail(self, mock_info):
        """Ensure find_host_lines warns on suspicious output."""
        self.proto.rst_hosts.append(RestraintHost('hh', 1234))
        line = '[aa] T:       1 [test1             ] Running'
        self.proto.find_host_lines(line)
        mock_info.assert_any_call('* line: "%s"', 'T:       1 [test1             ] Running')
        mock_info.assert_any_call('!!! Received line from unknown host %s', 'aa')

    def test_find_host_lines_abort(self):
        """Ensure find_host_lines ignores Aborted from restraint-client over panic from Beaker."""
        self.proto.rst_hosts.append(RestraintHost('aa', 123))
        line = '[aa] T:       1 [test1             ] Running\n' \
               '[aa] T:       2 [test1             ] Aborted\n'
        with mock.patch.object(self.proto, 'recipe_ids_dead', {123}):
            with mock.patch.object(self.proto, 'host_heartbeat', return_value={123}):
                with mock.patch.object(self.proto, 'process_task') as mock_process_task:
                    self.proto.find_host_lines(line)
                    # find_host_lines has to avoid processing that 'Aborted' output from restraint
                    # client to prioritize issue found by host_heartbeat, thus we get only one call
                    # instead of two.
                    assert mock_process_task.call_count == 1

    def test_split_hostline_match_invalid(self):
        """Ensure split_hostline_match handles invalid data."""
        result = self.proto.split_hostline_match('hostname1', '[hostname1] T:       1a [test1 ] Running: PASS')

        self.assertIsNone(result)

    @mock.patch('builtins.print')
    def test_split_hostline_match_port(self, mock_print):
        """Ensure split_hostline_match prints restraint port."""
        self.proto.split_hostline_match('hostname1', '[hostname1] Listening on '
                                                     'http://localhost:36351')
        mock_print.assert_called_with('hostname1                                '
                                      '[hostname1] Listening on http://localhost:36351')

    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_no_repeat(self, mock_debug):
        """Ensure cleanup_handler works and isn't called twice."""
        with mock.patch.object(self.proto, 'cleanup_done', True):
            self.proto.cleanup_handler()
            mock_debug.assert_not_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('restraint_wrap.restraint_protocol.Watcher')
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.attempt_heartbeat_stop', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.pathlib.Path', mock.Mock())
    def test_cleanup_handler(self, mock_recipe, mock_watcher, mock_debug):
        """Ensure cleanup_handler works and calls all it should."""
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                  'dump': False}
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               None, **kwargs)
        with mock.patch.object(proto.resource_group.recipeset.hosts[0], 'done_processing', True):
            with mock.patch.object(self.beaker, 'release_resources') as mock_release:
                proto.cleanup_handler()

        mock_watcher.return_value.observer.stop.assert_called()
        mock_debug.assert_called_with('restraint protocol cleanup runs (rid: %s)...',
                                      proto.resource_group.resource_id)

        mock_recipe.assert_called()
        mock_release.assert_called()

        self.assertTrue(proto.resource_group.recipeset.tests_finished)
        self.assertTrue(proto.cleanup_done)

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.attempt_heartbeat_stop')
    def test_cleanup_handler_rerun(self, mock_att_hrt_stop, mock_copy):
        """Ensure cleanup_handler handles reruns and adds protocols."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'

        proto.resource_group.recipeset.waiting_for_rerun = True

        with mock.patch.object(proto, 'add_protocol') as mock_add_protocol:
            proto.cleanup_handler()

            mock_add_protocol.assert_called()
            mock_copy.assert_called()

        self.assertTrue(proto.resource_group.recipeset.tests_finished)
        self.assertTrue(proto.cleanup_done)
        mock_att_hrt_stop.assert_called()

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.attempt_heartbeat_stop', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.traceback.print_exc')
    @mock.patch('restraint_wrap.restraint_protocol.RestraintClientProcessProtocol.'
                'dump_remaining_tasks_with_skip_status')
    def test_cleanup_handler_exception(self, mock_dump, mock_trace, mock_copy):
        """Ensure cleanup_handler handles exception from copy_results_and_logs."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)

        mock_copy.side_effect = itertools.chain([FileNotFoundError()])
        with mock.patch.object(self.beaker, 'release_resources'):
            proto.cleanup_handler()
        mock_trace.assert_called()
        mock_dump.assert_called()

    @mock.patch('upt.logger.LOGGER.info')
    @mock.patch('builtins.print')
    def test_process_ended(self, mock_print, mock_info):
        """Ensure processEnded works."""
        with mock.patch.object(self.proto, 'cleanup_handler') as mock_cleanup_handler:
            reason = mock.MagicMock()
            self.proto.processEnded(reason)
            mock_cleanup_handler.assert_called()
            recipes = ['R:123', 'R:456', 'R:789']
            mock_info.assert_called_with('* restraint protocol (%s) %s retcode: %s reason: %s', recipes, 'ended.',
                                         reason.value.exitCode, reason.value)
            reason.value.__str__.return_value = 'ended by signal 13'
            reason.value.exitCode = 13
            self.proto.processEnded(reason)

            msg = f'{COLORS.RED}restraint protocol was killed by SIGPIPE, the test may be' \
                  f' misbehaving{COLORS.RESET}'
            mock_print.assert_any_call(msg)

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task(self, mock_info):
        """Ensure rerun_from_task works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)
        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        recipeset = self.beaker.rgs[0].recipeset
        host = recipeset.hosts[0]
        task_result = TaskResult(host, restraint_task, 1, 'PASS', 'Completed')

        rerun_kwargs = {'host': host, 'task_result': task_result, 'task_id': 1}
        proto.rerun_from_task(**rerun_kwargs)

        self.assertEqual(host.rerun_recipe_tasks_from_index, 1)
        self.assertEqual(recipeset.attempts_made, 0)

        msg = ActionOnResult.format_msg(host.recipe_id, 1,
                                        'Making a re-run attempt 1/3 for the entire recipeSet',
                                        '*', 'abc')
        mock_info.assert_called_with(msg)

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task_not_called(self, mock_info):
        """Ensure rerun_from_task doesn't do anything when the host is already waiting for a rerun."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)

        host = self.beaker.rgs[0].recipeset.hosts[0]
        with mock.patch.object(self.beaker.rgs[0].recipeset, 'waiting_for_rerun', True):
            proto.rerun_from_task(**{'host': host})
            mock_info.assert_not_called()

    def test_add_task_result(self):
        """Ensure add_task_result works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'
        host = proto.resource_group.recipeset.hosts[0]
        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]

        # Add a matching subtassk result that must link.
        proto.unprocessed_subtask_results.add((host.recipe_id, 1, 'Test1', 'SKIP', '0'))
        # Add a matching subtassk result that must link, but isn't SKIP.
        proto.unprocessed_subtask_results.add((host.recipe_id, 1, 'Test1', 'WARN', '0'))
        # Add a matching subtassk result that must not link.
        subtask_result_no_match = (1000, 1, 'Test1', 'PASS', '100')
        proto.unprocessed_subtask_results.add(subtask_result_no_match)

        proto.add_task_result(host, restraint_task, 1, '', 'Completed')

        self.assertEqual(proto.unprocessed_subtask_results, {subtask_result_no_match})

        task_boot_done = ('<task name="Boot test" ><params><param name="CKI_NAME" '
                          'value="Boot test"/></params></task>')
        proto.add_task_result(host, RestraintTask.create_from_string(task_boot_done), 2, 'PASS', 'Completed')

    @mock.patch('upt.logger.LOGGER.debug')
    def test_mark_recipe_done(self, mock_debug):
        """Ensure mark_recipe_done works."""
        proto = copy.deepcopy(self.proto)
        host = proto.resource_group.recipeset.hosts[0]
        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        task_result = TaskResult(host, restraint_task, 1, 'PASS', 'Completed')

        with mock.patch.object(host, 'done_processing', True):
            proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            mock_debug.assert_not_called()
        with mock.patch.object(host, 'done_processing', False):
            proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            msg = ActionOnResult.format_msg(123, 1,
                                            'recipe is done processing [mark_recipe_done]',
                                            '*', 'abc')
            mock_debug.assert_called_with(msg)

    def test_mark_recipe_done_raises(self):
        """Ensure mark_recipe_done raises error on invalid data."""
        proto = copy.deepcopy(self.proto)
        host = proto.resource_group.recipeset.hosts[0]
        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        task_result = TaskResult(host, restraint_task, 1, 'PASS', 'Completed')

        with self.assertRaises(RuntimeError):
            proto.mark_recipe_done(**{'recipe_id': 5678, 'task_result': task_result})

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    def test_is_ewd_hit(self):
        """Ensure is_ewd_hit works."""
        with mock.patch.object(self.proto, 'recipe_ids_dead', {1234}):
            self.assertTrue(self.proto.is_ewd_hit(1234, 'Aborted'))

        self.assertFalse(self.proto.is_ewd_hit(1234, 'Aborted'))

    def test_mark_lwd_hit(self):
        """Ensure mark_lwd_hit works."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None, None,
                                               **self.std_kwargs)
        proto.unprocessed_lwd_hits.add((123, 1))
        host = proto.resource_group.recipeset.hosts[0]

        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        task_result = TaskResult(host, restraint_task, 1, 'PASS', 'Completed')
        proto.mark_lwd_hit(**{'task_result': task_result})

        self.assertEqual(proto.unprocessed_lwd_hits, set())

    def test_prepare_actions(self):
        """Ensure prepare_actions works."""
        for item in self.proto._prepare_actions():
            self.assertIsInstance(item, ActionOnResult)

    def test_is_lwd_hit(self):
        """Ensure is_lwd_hit works."""
        proto = copy.deepcopy(self.proto)
        host = proto.resource_group.recipeset.hosts[0]

        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {(123, 1), (123, 2)}):
            self.assertFalse(proto.is_lwd_hit(123, 200))
            restraint_task1 = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
            restraint_task2 = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[1]
            task_result1 = TaskResult(host,
                                      restraint_task1,
                                      1, 'WARN', 'Aborted',
                                      lwd_hit=proto.is_lwd_hit(123, 1))
            task_result2 = TaskResult(host,
                                      restraint_task2,
                                      1, 'WARN', 'Aborted',
                                      lwd_hit=proto.is_lwd_hit(123, 2))

            with mock.patch.object(host, 'task_results', [task_result1, task_result2]):
                self.assertTrue(proto.is_lwd_hit(123, 1))
                self.assertTrue(proto.is_lwd_hit(123, 2))
                # TaskResult objects are marked as cause of the LWD hit. This can be used for further processing
                # (e.g. to create accurate kcidb data) later on.
                self.assertTrue(task_result1.lwd_hit)
                self.assertTrue(task_result2.lwd_hit)

    def test_is_last_host_task(self):
        """Ensure is_last_host_task works."""
        proto = RestraintClientProcessProtocol(self.beaker, copy.deepcopy(self.beaker.rgs[0]), None,
                                               lambda *args: None, **self.std_kwargs)
        copied_recipeset = copy.deepcopy(proto.resource_group.recipeset)

        self.assertTrue(proto.is_last_host_task(123, 2))

        with self.assertRaises(AssertionError):
            proto.resource_group.recipeset = copied_recipeset
            self.assertFalse(proto.is_last_host_task(756, 200))

    def test_evaluate_task_result(self):
        """Ensure evaluate_task_result works."""
        resource_group = self.beaker.rgs[0]
        host0 = resource_group.recipeset.hosts[0]
        host1 = copy.deepcopy(host0)
        host1.done_processing = True
        host1.recipe_id = 456

        resource_group.recipeset.hosts = [host1, host0]
        proto = RestraintClientProcessProtocol(self.beaker, resource_group, lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        task_result = TaskResult(host0, restraint_task, 1, 'WARN', 'Aborted')

        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {}):
            proto.evaluate_task_result(task_result, host0)
            proto.evaluate_task_result(task_result, host1)

    def test_evaluate_task_result_adapter(self):
        """Ensure evaluate_task_result works and calls ."""
        resource_group = self.beaker.rgs[0]
        host0 = resource_group.recipeset.hosts[0]
        host1 = copy.deepcopy(host0)
        host1.done_processing = True
        host1.recipe_id = 456

        resource_group.recipeset.hosts = [host1, host0]
        proto = RestraintClientProcessProtocol(self.beaker, resource_group, lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'

        restraint_task = proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        task_result = TaskResult(host0, restraint_task, 1, 'Panic', 'Aborted')
        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {}):
            with mock.patch.object(proto, 'is_last_host_task', lambda *args: True):
                proto.evaluate_task_result(task_result, host0)

                proto.evaluate_task_result(task_result, host1)
                # Don't dump on re-runs.
                proto.on_task_result.assert_called()

    @mock.patch('builtins.print')
    def test_evaluate_task_result_no_eval_setup(self, mock_print):
        """Ensure that evaluate_task_result will not try to evaluate setup task as test."""
        restraint_task = RestraintTask.create_from_string('<task name="Setup task" ></task>')
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               mock.Mock(), **self.std_kwargs)
        with mock.patch.object(proto.watcher, 'get_lwd_hits', lambda: {}):
            host = proto.resource_group.recipeset.hosts[0]
            task_result = TaskResult(host, restraint_task, 1, 'Panic', 'Aborted')
            proto.evaluate_task_result(task_result, host)
            mock_print.assert_called_with('Setup task is a setup task - not evaluating as test.')

    @mock.patch('upt.logger.LOGGER.error')
    def test_process_task(self, mock_error):
        """Ensure process_task handles a corner case right."""
        host = self.proto.resource_group.recipeset.hosts[0]
        self.assertIsNone(self.proto.process_task(host, 999, 'Aborted', 'Panic'))
        mock_error.assert_called()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_host_heartbeat_noop(self):
        """Ensure host_heartbeat does nothing when no host have issues."""
        with mock.patch.object(self.proto, 'watcher'):
            with mock.patch.object(self.proto.provisioner, 'heartbeat') as mock_heartbeat:
                with mock.patch.object(self.proto, 'recipe_ids_dead', {}):
                    self.proto.host_heartbeat()
                    mock_heartbeat.assert_called()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_host_heartbeat(self):
        """Ensure host_heartbeat works."""
        with (mock.patch.object(self.proto, 'watcher'),
              mock.patch.object(self.proto.provisioner, 'heartbeat'),
              mock.patch.object(self.proto, 'recipe_ids_dead', {456, 789}),
              mock.patch.object(self.proto, 'heartbeat_loop') as mock_loop,
              mock.patch.object(self.proto, 'process_task'),
              mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync,
              mock.patch.object(self.proto, 'handle_failure') as mock_failure):
            mock_sync.return_value.__enter__.return_value.name = 'whatever'
            mock_loop.stop.side_effect = itertools.chain([AssertionError()],
                                                         itertools.cycle([None]))
            self.proto.host_heartbeat()

            host = self.proto.resource_group.recipeset.hosts[1]
            self.assertEqual(host.task_results[0].status, 'Aborted')
            mock_loop.stop.assert_called()
            mock_failure.assert_called()

    @mock.patch('upt.logger.LOGGER.error')
    def test_evaluate_host_abort(self, mock_error):
        """Ensure evaluate_host_abort works."""
        with mock.patch.object(self.proto.watcher, 'get_subtask_results', lambda *args: set()):
            with mock.patch.object(self.proto.watcher, 'get_lwd_hits', lambda *args: set()):
                with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
                    mock_sync.return_value.__enter__.return_value.name = 'whatever'
                    with mock.patch.object(self.proto.resource_group.recipeset.hosts[0], 'task_results', []):
                        with mock.patch.object(self.proto, 'get_restraint_xml_task', lambda *args: None):
                            self.proto.evaluate_host_abort(self.proto.resource_group.recipeset.hosts[0])
                            mock_error.assert_called_with('💀 Unhandled case: cannot find task_id 1!')

    def test_evaluate_host_abort_prior_tasks(self):
        """Ensure evaluate_host_abort works for cases when some tasks were run."""
        task = self.proto.get_restraint_xml_task(self.proto.resource_group.recipeset.hosts[0].recipe_id, 1)
        with mock.patch.object(self.proto.watcher, 'get_subtask_results', lambda *args: set()):
            with mock.patch.object(self.proto.watcher, 'get_lwd_hits', lambda *args: set()):
                with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
                    mock_sync.return_value.__enter__.return_value.name = 'whatever'
                    host = self.proto.resource_group.recipeset.hosts[0]
                    with mock.patch.object(host, 'task_results', [TaskResult(host, task, 1,
                                                                             'Warn', 'Aborted')]):
                        with mock.patch.object(self.proto, 'get_restraint_xml_task', lambda *args: None):
                            self.proto.evaluate_host_abort(self.proto.resource_group.recipeset.hosts[0])

    @mock.patch('upt.logger.LOGGER.debug')
    def test_boot_failure(self, mock_debug):
        """Ensure EWD during Boot test is marked as such."""
        # We have to use a task named "Boot test".
        xml_content = """
        <task name="Boot test" >
          <params>
            <param name="CKI_NAME" value="Boot test"/>
            <param name="CKI_UNIVERSAL_ID" value="Boot test"/>
          </params>
        </task>
        """
        restraint_task = RestraintTask.create_from_string(xml_content)
        host = self.proto.resource_group.recipeset.hosts[0]
        with mock.patch.object(self.proto.watcher, 'get_lwd_hits', lambda: {}):
            # Ensure recipe_id 123 is believed to hit EWD.
            with mock.patch.object(self.proto, 'recipe_ids_dead', {123}):
                # It has to be a task of recipe_id 123, which hits EWD.
                task_result = TaskResult(host, restraint_task, 1, 'Panic', 'Aborted',
                                         ewd_hit=self.proto.is_ewd_hit(123, 'Panic'))
                with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
                    mock_sync.return_value.__enter__.return_value.name = 'whatever'
                    host = self.proto.resource_group.recipeset.hosts[0]
                    self.proto.evaluate_task_result(task_result, host)

                    msg = ActionOnResult.format_msg(host.recipe_id, task_result.task_id,
                                                    'recipe is done processing [mark_recipe_done]',
                                                    '*',
                                                    task_result.testname)
                    mock_debug.assert_called_with(msg)

    def test_cond_print_notification(self):
        """Ensure no notification of failure and maintainer info is printed on pass/complete."""
        restraint_task = RestraintTask.create_from_string('<task name="Boot test" />')
        host = self.proto.resource_group.recipeset.hosts[0]

        task_result = TaskResult(host, restraint_task, 1, 'PASS', 'Completed')
        RestraintClientProcessProtocol.cond_print_notification(task_result, [None, None])

    @mock.patch('restraint_wrap.restraint_protocol.pathlib.Path.read_text', mock.MagicMock())
    @mock.patch('restraint_wrap.restraint_protocol.RestraintJob', mock.MagicMock())
    @mock.patch('restraint_wrap.restraint_protocol.ET.tostring', lambda *args, **kwargs: 'foo')
    @mock.patch('restraint_wrap.restraint_protocol.ET', mock.MagicMock())
    def test_copy_results_and_logs(self):
        """Ensure copy_results_and_logs works and prints a warning when a directory is missing."""
        with create_temporary_files(
                [],
                # Create dirs that are present and method under test moves files to them.
                ['output/results_0001/',
                 'output/results_0002/',
                 'output/job.01/recipes/123/tasks/1/logs',
                 'output/job.01/recipes/456/tasks/1/logs',
                 'output/job.01/recipes/456/tasks/1/results/9876/logs',
                 'output/job.01/recipes/456/tasks/1/results/9876/results'
                 ]
        ):
            kwargs = self.std_kwargs.copy()
            kwargs['output'] = 'output'
            proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                                   lambda *args: None, None, **kwargs)
            proto.run_suffix = 'job.01'
            hosts = proto.resource_group.recipeset.hosts[:2]
            hosts[0].task_results.append(
                TaskResult(hosts[0], RestraintTask.create_from_string('<task name="a1" />'), 1, 'Panic',
                           'Aborted'))
            task_result = TaskResult(hosts[1], RestraintTask.create_from_string('<task name="a2" />'), 1,
                                     'Panic', 'Aborted')
            task_result.subtask_results.append(('a2 subtask', '9876', 'PASS'))
            hosts[1].task_results.append(task_result)
            proto.copy_results_and_logs(hosts)
            # Finally, check that files are actually there...
            self.assertTrue(pathlib.Path('output/results_0001/index.html').is_file())
            self.assertTrue(pathlib.Path('output/results_0002/index.html').is_file())
            self.assertTrue(pathlib.Path(
                'output/results_0001/LOGS_J:1234_0_R_123_T_1_test-redhat_a1').is_dir())
            self.assertTrue(pathlib.Path(
                'output/results_0002/LOGS_J:1234_0_R_456_T_1_test-redhat_a2').is_dir())
            self.assertTrue(pathlib.Path(
                'output/results_0002/RESULTS_J:1234_0_R_456_T_1_test-redhat_a2').is_dir())

            with mock.patch.object(proto, 'run_suffix', 'xxx'):
                proto.copy_results_and_logs(hosts)

    def test_copy_results_and_logs_exception(self):
        """Ensure copy_results_and_logs handles exception from create_fixed_index."""
        with create_temporary_files([]):
            # Create dirs that are present and method under test moves files to them.
            proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                                   lambda *args: None, None, **self.std_kwargs)
            proto.run_suffix = 'job.01'
            hosts = proto.resource_group.recipeset.hosts[:1]
            hosts[0].task_results.append(TaskResult(hosts[0],
                                                    RestraintTask.create_from_string('<task name="a1" />'),
                                                    1, 'Panic', 'Aborted'))

            with mock.patch.object(proto, 'create_fixed_index') as mock_index:
                mock_index.side_effect = itertools.chain([FileNotFoundError()])

                proto.copy_results_and_logs(hosts)

    def test_dump_remaining_tasks_with_skip_status(self):
        """Ensure the remainining tasks have SKIP status after host processing."""
        restraint_task = RestraintTask.create_from_string('<task name="Boot test" ><params>'
                                                          '<param name="CKI_NAME" value="Boot test"/>'
                                                          '</params></task>')
        restraint_second_task = RestraintTask.create_from_string('<task name="Boot test" ><params>'
                                                                 '<param name="CKI_NAME" value="foo test"/>'
                                                                 '</params></task>')
        proto = self.proto
        host = proto.resource_group.recipeset.hosts[0]
        # Force an EWD in the Boot test.
        task_result = TaskResult(host, restraint_task, 1, 'Panic', 'Aborted',
                                 ewd_hit=True)
        task_result.kcidb_status = 'FAIL'
        second_task_result = TaskResult(host, restraint_second_task, 2, None, None)
        host.planned_tests = [task_result, second_task_result]
        host.task_results = [task_result]
        with (
            mock.patch.object(proto, 'get_dumpfile_name', lambda *_, **__: ''),
            mock.patch.object(proto, 'on_task_result'),
        ):
            proto.dump_remaining_tasks_with_skip_status(host)
            self.assertEqual('Cancelled', second_task_result.status)
            self.assertEqual('SKIP', second_task_result.kcidb_status)

            # Ensure we didn't change status of tasks that already had it
            self.assertEqual('FAIL', task_result.kcidb_status)
