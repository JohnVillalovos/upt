"""Test cases for misc module."""
import pathlib
import unittest
from unittest import mock

from ruamel.yaml.representer import RoundTripRepresenter
from ruamel.yaml.representer import ScalarNode

from restraint_wrap.restraint_file.job_element import RestraintJob
from restraint_wrap.restraint_file.recipe_element import RestraintRecipe
from restraint_wrap.restraint_file.recipeset_element import RestraintRecipeSet
from restraint_wrap.restraint_file.task_element import RestraintTask
from tests.utils import create_temporary_files
from upt import const
from upt import misc


class TestMisc(unittest.TestCase):
    """Test cases for misc module."""

    def setUp(self) -> None:
        self.test_recipe = """<recipe status="Aborted">
            <task name="a1" status="Aborted">
                <params>
                    <param name="KILLTIMEOVERRIDE" value= "10" />
                </params>
            </task>
            <task name="/distribution/command" status="Cancelled">
                <params>
                    <param name="KILLTIMEOVERRIDE" value= "20" />
                </params>
            </task>
            <task name="a3" status="Cancelled" >
                <fetch />
                <params>
                    <param name="DONTCARE" value= "20" />
                </params>
            </task>
            <task name="a4" status="Cancelled" >
                <params>
                    <param name="KILLTIMEOVERRIDE" value="INV" />
                </params>
            </task>
         </recipe>"""
        self.cancelled_recipe = """<recipe status="Cancelled" />"""
        self.no_status_recipe = """<recipe />"""
        self.invalid_status_recipe = """<recipe status="Blah"/>"""

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_fixup_or_delete_tasks_without_fetch(self):
        """Ensure fixup_or_delete_tasks_without_fetch works."""
        job = RestraintJob.create_from_scratch()
        job.recipesets = [RestraintRecipeSet.create_from_scratch()]
        job.recipesets[0].recipes = [RestraintRecipe.create_from_string(self.test_recipe)]
        copy_task = job.recipesets[0].recipes[0].tasks[-3]

        misc.fixup_or_delete_tasks_without_fetch(job)
        # Task a1 is removed, task /distribution/command is kept and url
        # is inserted. Task a3 is kept as well.
        left_tasks = job.recipesets[0].recipes[0].tasks
        self.assertEqual(len(left_tasks), 2)

        left_task1 = left_tasks[0]
        self.assertEqual(left_task1.name, copy_task.name)
        self.assertEqual(left_task1.fetch.url,
                         const.BKR_CORE_TASKS_URL.format(task='command'))

        left_task2 = left_tasks[1]
        self.assertEqual(left_task2.name, 'a3')

    def test_recipe_not_provisioned(self):
        """Ensure recipe_not_provisioned works."""
        restraint_recipe1 = RestraintRecipe.create_from_string(self.test_recipe)
        restraint_recipe2 = RestraintRecipe.create_from_string(self.cancelled_recipe)
        restraint_recipe3 = RestraintRecipe.create_from_string(self.no_status_recipe)
        restraint_recipe4 = RestraintRecipe.create_from_string(self.invalid_status_recipe)

        recipe1_status = misc.recipe_not_provisioned(restraint_recipe1)
        recipe2_status = misc.recipe_not_provisioned(restraint_recipe2)
        recipe3_status = misc.recipe_not_provisioned(restraint_recipe3)
        recipe4_status = misc.recipe_not_provisioned(restraint_recipe4)

        self.assertEqual(recipe1_status, 'Aborted')
        self.assertEqual(recipe2_status, 'Cancelled')
        self.assertEqual(recipe3_status, False)
        self.assertEqual(recipe4_status, False)

    def test_reservesys_task_problem(self):
        """Ensure reservesys_task_problem works."""
        tasks = RestraintRecipe.create_from_string(self.test_recipe).tasks
        self.assertEqual(misc.reservesys_task_problem([tasks[0]]), 'Aborted')
        self.assertEqual(misc.reservesys_task_problem([tasks[1]]), 'Cancelled')
        self.assertEqual(misc.reservesys_task_problem(
            [RestraintTask.create_from_string('<task name="a3" status="None" result="Pass"/>')]), False)
        self.assertEqual(misc.reservesys_task_problem(
            [RestraintTask.create_from_string('<task name="a4" status="None" result="Fail"/>')]), 'Fail')

    def test_recipe_installing_or_waiting(self):
        """Ensure recipe_installing_or_waiting works."""
        recipe = RestraintRecipe.create_from_string(self.test_recipe)
        self.assertFalse(misc.recipe_installing_or_waiting(recipe))

        recipe_inst = RestraintRecipe.create_from_string('<recipe status="Installing" />')
        self.assertTrue(misc.recipe_installing_or_waiting(recipe_inst))

        recipe_inst = RestraintRecipe.create_from_string('<recipe status="Waiting" />')
        self.assertTrue(misc.recipe_installing_or_waiting(recipe_inst))

    def test_merge_retcodes(self):
        """Ensure merge_retcodes works."""
        # It is assumed that highest retcode is the "worst".
        result = misc.RET.merge_retcodes([misc.RET.PROVISIONING_PASSED, misc.RET.PROVISIONING_FAILED])
        self.assertEqual(result, misc.RET.PROVISIONING_FAILED)

        # Empty list -> RET.PROVISIONING_PASSED
        result = misc.RET.merge_retcodes([])
        self.assertEqual(result, misc.RET.PROVISIONING_PASSED)

    def test_is_task_waived(self):
        """Ensure is_task_waived works."""
        task_waived = RestraintTask.create_from_string(
            '<task name="a4"><params><param name="CKI_WAIVED" value="true" />'
            '</params></task>')
        task_not_waived = RestraintTask.create_from_string(
            '<task name="a4"><params><param name="CKI_WAIVED" value="False" />'
            '</params></task>')

        # Test with CKI_WAIVED being true
        self.assertTrue(misc.is_task_waived(task_waived))

        # Test with CKI_WAIVED being False
        self.assertFalse(misc.is_task_waived(task_not_waived))

    def test_monotonic(self):
        """Ensure monotonic.get() works."""
        mono = misc.Monotonic()
        self.assertEqual(mono.get(), 1)
        self.assertEqual(mono.get(), 2)

    @mock.patch('upt.misc.LOGGER.debug')
    @mock.patch('upt.misc.SESSION')
    def test_sanitize(self, mock_session, mock_debug):
        """Ensure sanitize works."""
        mock_session.get.return_value.content = b"""
[  128.167001] dracut: Disassembling device-mapper devices
Rebooting.
[  128.188007] reboot: Restarting system
[  128.188312] reboot: machine restart
[    0.000000] Linux version 5.6.11-4eaced5.cki (cki@runner-wbxpsiz-project-2-concurrent-0ch6tg)
[    0.000000] Command line: BOOT_IMAGE=(hd0,msdos1)/vmlinuz-5.6.11-4eaced5.cki
[    0.000000] Disabled fast string operations
[    0.000000] x86/fpu: x87 FPU will use FXSAVE
[    0.000000] BIOS-provided physical RAM map:
"""

        with create_temporary_files([]) as tmpdir:
            tmpdir_pathlib = pathlib.Path(tmpdir)
            console_path = misc.sanitize_link('console.log', tmpdir_pathlib, '5.6.11-4eaced5.cki')

            new_console = console_path.read_text()
            self.assertNotIn('reboot: Restarting system', new_console)
            self.assertIn('Command line: BOOT_IMAGE', new_console)

        mock_session.get.assert_called_with('console.log')

    def test_ret_to_yaml(self):
        """Ensure RET serializes correctly."""
        result = misc.RET.to_yaml(RoundTripRepresenter(), misc.RET.PROVISIONING_FAILED)
        self.assertIsInstance(result, ScalarNode)
        # Both node and RET have value attributes, but are different objects.
        self.assertEqual(misc.RET.PROVISIONING_FAILED.name, result.value)
