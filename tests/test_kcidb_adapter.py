"""Test cases for kcidb_adapter module."""
from contextlib import contextmanager
from datetime import timedelta
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock

from dateutil.parser import parse as date_parse
from freezegun import freeze_time
from rcdefinition.rc_data import SKTData

from plumbing.objects import Host
from restraint_wrap.kcidb_adapter import KCIDBTestAdapter
from restraint_wrap.kcidb_adapter import RestraintStandaloneTest
from restraint_wrap.restraint_file.recipe_element import RestraintRecipe
from restraint_wrap.task_result import TaskResult
from upt.misc import OutputDirCounter

DEF_ENV_MOCK_DICT = {'CI_JOB_ID': '1234', 'CI_PIPELINE_ID': '5678', 'CI_PROJECT_PATH': 'path',
                     'CI_PROJECT_ID': '40', 'CI_SERVER_URL': 'https://', 'CI_JOB_STAGE': 'test',
                     'CI_JOB_URL': 'https://gitlab/job/1234', 'BEAKER_URL': 'https://beaker',
                     'CI_JOB_NAME': 'test x86_64', 'job_created_at': '2019-10-24T13:40:46.632298Z',
                     'CI_JOB_YMD': '2018/11/30',
                     'CI_COMMIT_SHA': 'deadbeef',
                     'CI_PROJECT_DIR': 'whatever',
                     'GITLAB_CI': 'true',
                     'KCIDB_DUMPFILE_NAME': 'kcidb_all.json',
                     'commit_hash': 'deadbeefdeadbeefdeadbeefdeadbeefdeadbeef',
                     'CI_COMMIT_REF_NAME': 'block',
                     'REVISION_PATH': 'whatever-revision-path',
                     'KCIDB_CHECKOUT_ID': 'redhat:5678',
                     'KCIDB_BUILD_ID': 'redhat:1234'}


RC_DATA = SKTData({'state': {'debug_kernel': False,
                             'test_hash': 'deadbeef',
                             'tag': 'tag',
                             'kernel_arch': 'x86_64',
                             'commit_message_title': 'title',
                             'kernel_version': '3.18',
                             'targeted_tests': 0},
                   'revision': {'start_time': '2021-01-23 13:49:53 -0500'},
                   'build': {'job_id': 1234}})


@contextmanager
def mocked_kcidb_all():
    """Mock kcidb_all.json file as context manager."""
    kcidb_all_json = {
        'version': {'major': 4, 'minor': 0},
        'checkouts': [
            {'id': 'redhat:5678', 'origin': 'redhat'},
        ],
        'builds': [
            {'id': 'redhat:1234', 'checkout_id': 'redhat:5678', 'origin': 'redhat',
             'architecture': 'x86_64'},
        ]
    }
    tempdir = tempfile.TemporaryDirectory()
    pathlib.Path(tempdir.name, 'kcidb_all.json').write_text(json.dumps(kcidb_all_json))
    with mock.patch.dict(os.environ, {'CI_PROJECT_DIR': tempdir.name}):
        yield


class TestKCIDBTestAdapter(unittest.TestCase):
    """Test cases for KCIDBTestAdapter class."""

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_artifacts_path(self):
        """Ensure artifacts_path works."""
        with mocked_kcidb_all():
            adapter = KCIDBTestAdapter(
                **{'output': 'whatever-dir', 'rc_data': RC_DATA, 'upload': False, 'instance_no': 1})

        expected = '2018/11/30/redhat:5678/build_x86_64_redhat:1234/tests/'
        self.assertEqual(expected, adapter.artifacts_path)


class TestRestraintStandaloneTest(unittest.TestCase):
    """Test cases for RestraintStandaloneTest class."""

    # pylint: disable=too-many-instance-attributes
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @freeze_time('2020-11-13T16:34:45.912589Z', tz_offset=1)
    def setUp(self) -> None:
        recipe_xml = (
            '<recipe>'
            '  <tasks>'
            '    <task id="1" name="a3" status="Completed" result="PASS">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="1"/>'
            '        <param name="CKI_NAME" value="a3"/>'
            '      </params>'
            '    </task>'
            '    <task id="2" name="a4" status="Completed">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="2"/>'
            '        <param name="CKI_UNIVERSAL_ID" value="some/path"/>'
            '      </params>'
            '    </task>'
            '  </tasks>'
            '</recipe>'
        )
        self.restraint_tasks = RestraintRecipe.create_from_string(recipe_xml).tasks
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234, 'done_processing': True})
        self.host.counter = OutputDirCounter()
        self.host_no_retcode = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        self.host_no_retcode.counter = OutputDirCounter()
        self.task_result = TaskResult(self.host, self.restraint_tasks[0], 1, 'PASS', 'Completed')
        self.task_result.kcidb_status = 'PASS'
        self.mock_adapter = mock.Mock()
        self.mock_adapter.checkout_id = '92b8f402aa964f209772e30190af5de818af996c'
        self.mock_adapter.rc_data = RC_DATA
        self.mock_adapter.job_id = '1234'
        self.mock_adapter.build = {'id': 'redhat:1'}
        self.mock_adapter.upload = False
        self.mock_adapter.instance_no = 1

        self.standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                       run_suffix='job.01', testplan=False)
        task_result_no_retcode = TaskResult(self.host_no_retcode, self.restraint_tasks[0], 1, 'PASS',
                                            'Completed')
        task_result_no_retcode.kcidb_status = 'PASS'
        self.standalone_test_no_retcode = RestraintStandaloneTest(self.mock_adapter,
                                                                  task_result_no_retcode,
                                                                  run_suffix='job.01',
                                                                  testplan=False)

        task_result_normalize = TaskResult(self.host, self.restraint_tasks[0], 1, 'WARN', 'Completed')
        task_result_normalize.kcidb_status = 'ERROR'
        self.standalone_test_normalize = RestraintStandaloneTest(self.mock_adapter,
                                                                 task_result_normalize,
                                                                 run_suffix='job.01',
                                                                 testplan=False)

    @mock.patch('restraint_wrap.kcidb_adapter.upload_file')
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_uploading_files(self, mock_upload_file):
        """Ensure RestraintStandaloneTest output_files works when uploading files."""
        output_path = 'run/run.done'
        job_suffix = 'job.01'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://my-dst-bucket/{path}'
        mock_adapter = mock.Mock()
        mock_adapter.checkout_id = '92b8f402aa964f209772e30190af5de818af996c'
        mock_adapter.rc_data = RC_DATA
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.upload = True
        mock_adapter.instance_no = '1'
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_upload_file.side_effect = [f'{full_path}/file1', f'{full_path}/file2']

        with mock.patch.object(self.task_result, 'output_files_from_path',
                               lambda *args: [{'name': 'file1', 'path': path},
                                              {'name': 'file2', 'path': path}]):
            ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result,
                                              run_suffix=job_suffix, testplan=True)
            # If test plan we don't get any files
            self.assertListEqual(
                ret_obj.output_files,
                []
            )
            # If no test plan, we get all the files
            ret_obj.testplan = False
            self.assertListEqual(
                ret_obj.output_files,
                [
                    {'name': 'file1', 'url': f'{full_path}/file1'},
                    {'name': 'file2', 'url': f'{full_path}/file2'},
                ]
            )

    @mock.patch('restraint_wrap.kcidb_adapter.convert_path_to_link')
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_wihout_uploading_files(self, mock_convert_link):
        """Ensure RestraintStandaloneTest output_files works when not uploading files."""
        output_path = 'run/run.done'
        job_suffix = 'job.01'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://mybucket/{path}'
        mock_adapter = mock.Mock()
        mock_adapter.checkout_id = '92b8f402aa964f209772e30190af5de818af996c'
        mock_adapter.rc_data = RC_DATA
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.upload = False
        mock_adapter.instance_no = '1'
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_convert_link.side_effect = [f'{full_path}/file1', f'{full_path}/file2']

        with mock.patch.object(self.task_result, 'output_files_from_path',
                               lambda *args: [{'name': 'file1', 'path': path},
                                              {'name': 'file2', 'path': path}]):
            ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result,
                                              run_suffix=job_suffix, testplan=True)
            # If test plan we don't get any files
            self.assertListEqual(
                ret_obj.output_files,
                []
            )
            # If no test plan, we get all the files
            ret_obj.testplan = False
            self.assertListEqual(
                ret_obj.output_files,
                [
                    {'name': 'file1', 'url': f'{full_path}/file1'},
                    {'name': 'file2', 'url': f'{full_path}/file2'},
                ]
            )

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_duration(self):
        """Ensure _duration and finish_time work."""
        mock_start_time = "2020-11-09T18:06:48.872084Z"
        task_result = TaskResult(self.host, self.restraint_tasks[0], 1, 'PASS', 'Completed')
        delta = 5
        with mock.patch.object(task_result, 'start_time', mock_start_time):
            time_frozen_at = (date_parse(mock_start_time) + timedelta(seconds=delta)).isoformat()
            with freeze_time(time_frozen_at, tz_offset=1):
                standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result, 'job.01')
                self.assertEqual(standalone_test.duration, delta)
                self.assertEqual(standalone_test.finish_time, '2020-11-09T18:06:53.872084+00:00')

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render(self):
        """Ensure render works."""
        self.mock_adapter.output = 'run/run.done'

        expected = {'build_id': 'redhat:1',
                    'id': 'redhat:1_upt_1',
                    'comment': 'a3',
                    'origin': 'redhat',
                    'status': 'PASS',
                    'start_time': self.standalone_test.start_time}
        misc = {'debug': False,
                'targeted': False,
                'fetch_url': 'git://',
                'beaker': {'task_id': 1,
                           'recipe_id': 1234},
                'maintainers': [],
                'rerun_index': 1,
                'provenance': [
                    {
                        'environment': {
                            'commit_hash': 'deadbeefdeadbeefdeadbeefdeadbeefdeadbeef'
                        },
                        'function': 'coordinator',
                        'url': 'https://gitlab/job/1234',
                        'service_name': 'gitlab',
                    },
                    {
                        'function': 'provisioner',
                        'url': 'https://beaker/recipes/1234',
                        'service_name': 'beaker',
                    }
                ]}

        with mock.patch.object(self.standalone_test, 'testplan', True):
            result = self.standalone_test.render()
            self.assertEqual(expected, result)

        with mock.patch.object(self.standalone_test, 'testplan', False):
            expected['misc'] = misc
            expected['waived'] = False
            expected['environment'] = {'comment': 'hostname1'}

            result = self.standalone_test.render()
            self.assertEqual(expected, result)

        with mock.patch.dict(os.environ, {'GITLAB_CI': 'false', 'BEAKER_URL': ''}):
            expected['misc']['provenance'] = []
            result = self.standalone_test.render()
            self.assertEqual(expected, result)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_standalone_test_finish_time(self):
        """Ensure finish_time is set to beaker key."""
        task_result = TaskResult(self.host, self.restraint_tasks[1], 1, None, 'Completed')
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result,
                                                  run_suffix='job.01', testplan=False)
        standalone_test.finish_time = 'some'

        self.assertEqual('some', standalone_test.misc['beaker']['finish_time'])

    @mock.patch('restraint_wrap.kcidb_adapter.RestraintStandaloneTest.output_files',
                new_callable=mock.PropertyMock)
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render_path(self, mock_files):
        """Ensure render sets path from univesal_id."""
        task_result = TaskResult(self.host, self.restraint_tasks[1], 1, None, 'Completed')
        task_result.kcidb_status = 'PASS'
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result,
                                                  run_suffix='job.01', testplan=False)
        self.assertEqual('some.path', standalone_test.render()['path'])

    def test_targeted_tests_false(self):
        """Test targeted field. targeted_tests false."""
        self.assertEqual([], self.standalone_test.targeted_tests_list)

    def test_targeted_tests_empty(self):
        """Test targeted field. targeted_tests undefined."""
        self.mock_adapter.rc_data = SKTData(
            {'state': {},
             'revision': {'start_time': '2021-01-23 13:49:53 -0500'},
             'build': {'job_id': 1234}}
        )

        standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                  run_suffix='job.01', testplan=False)
        self.assertEqual([], standalone_test.targeted_tests_list)

    @mock.patch('restraint_wrap.kcidb_adapter.RestraintStandaloneTest.output_files',
                mock.PropertyMock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_targeted_tests_set_but_mismatch(self):
        """Test targeted field. Test is not in targeted tests list."""
        targeted_tests_list = ['test_1', 'test_2']
        self.mock_adapter.rc_data = SKTData(
            {'state': {'targeted_tests': 1,
                       'targeted_tests_list': 'targeted_tests.txt'},
             'revision': {'start_time': '2021-01-23 13:49:53 -0500'},
             'build': {'job_id': 1234}}
        )

        tempdir = tempfile.TemporaryDirectory()
        pathlib.Path(tempdir.name, 'targeted_tests.txt').write_text('\n'.join(targeted_tests_list))
        with mock.patch.dict(os.environ, {'CI_PROJECT_DIR': tempdir.name}):
            standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                      run_suffix='job.01', testplan=False)

        # Ensure test name is not in targeted_tests_list
        self.assertNotIn(standalone_test.task_result.testname, targeted_tests_list)

        self.assertEqual(targeted_tests_list, standalone_test.targeted_tests_list)
        self.assertFalse(standalone_test.render()['misc']['targeted'])

    @mock.patch('restraint_wrap.kcidb_adapter.RestraintStandaloneTest.output_files',
                mock.PropertyMock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_targeted_tests_set(self):
        """Test targeted field. Test is in targeted tests list."""
        targeted_tests_list = ['test_1', 'test_2']

        self.mock_adapter.rc_data = SKTData(
            {'state': {'targeted_tests': 1,
                       'targeted_tests_list': 'targeted_tests.txt'},
             'revision': {'start_time': '2021-01-23 13:49:53 -0500'},
             'build': {'job_id': 1234}}
        )

        tempdir = tempfile.TemporaryDirectory()
        pathlib.Path(tempdir.name, 'targeted_tests.txt').write_text('\n'.join(targeted_tests_list))
        with mock.patch.dict(os.environ, {'CI_PROJECT_DIR': tempdir.name}):
            standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                      run_suffix='job.01', testplan=False)

        standalone_test.task_result.testname = targeted_tests_list[0]
        self.assertTrue(standalone_test.render()['misc']['targeted'])
