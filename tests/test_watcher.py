"""Test cases for upt __main__ module."""
import pathlib
import re
import tempfile
from threading import Thread
import time
import unittest
from unittest import mock

from plumbing.objects import Host
from plumbing.objects import ResourceGroup
from restraint_wrap.watcher import WatchedFile
from restraint_wrap.watcher import Watcher
from tests.utils import create_temporary_files
from upt.logger import COLORS


def do_test(write_content, post_test=lambda x: None):
    """Prepare a watcher and run a test against it."""
    with create_temporary_files(
            files=[],
            directories=[
                'recipes/1234/tasks/1',
                'recipes/1234/tasks/2',
                'recipes/1234/tasks/3'
            ]) as tempdir:

        resource_group = ResourceGroup()
        resource_group.recipeset.hosts = [Host({'recipe_id': 1234, 'hostname': 'host1'})]
        watcher = Watcher(resource_group)

        proc = Thread(target=write_content, args=(watcher, tempdir,))
        proc.start()
        watcher.run(tempdir)
        proc.join()

    # Test some assertions at the end.
    post_test(watcher)


class TestWatcher(unittest.TestCase):
    """Test cases for upt __main__ module."""

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.printc')
    def test_watcher(self, mock_printc):
        # pylint: disable=no-self-use
        """Test that watcher works."""

        def write_content(watcher, tempdir):
            time.sleep(1)

            pathlib.Path(tempdir, 'recipes/1234/tasks/1/', 'taskout.log').write_text('** /10_localwatchdog PASS '
                                                                                     'Score:0\n')
            pathlib.Path(tempdir, 'recipes/1234/tasks/1/', 'harness.log').write_text('Localwatchdog task: 1\n')
            pathlib.Path(tempdir, 'recipes/1234/tasks/2/', 'taskout.log').write_text('** /some/task PASS Score:xx\n'
                                                                                     '** /some/task PASS Score:N/A\n'
                                                                                     '** /some/task PASS Score:120\n')
            pathlib.Path(tempdir, 'recipes/1234/tasks/2/', 'ignored.file').write_text('** /non/task PASS Score:0\n')
            pathlib.Path(tempdir, 'recipes/1234/tasks/3/', 'taskout.log').write_text(
                '** /some/task with multi word name SKIP Score:0\n'
            )
            time.sleep(0.2)

            watcher.observer.stop()

        def post_test(watcher):
            results = watcher.get_subtask_results()
            self.assertIn((1234, 1, '/10_localwatchdog', 'PASS', 0), results)
            self.assertIn((1234, 2, '/some/task', 'PASS', 0), results)
            self.assertIn((1234, 2, '/some/task', 'PASS', 'N/A'), results)
            self.assertNotIn((1234, 2, '/non/task', 'PASS', 0), results)
            self.assertIn((1234, 3, '/some/task with multi word name', 'SKIP', 0), results)
            self.assertIn((1234, 1), watcher.get_lwd_hits())
            mock_printc.assert_any_call('test /some/task reports "120" instead of 0 - 99!',
                                        color=COLORS.YELLOW)

        do_test(write_content, post_test)

    @mock.patch('builtins.print', mock.Mock())
    def test_watched_file(self):
        """Ensure WatchedFile works."""
        with tempfile.TemporaryDirectory() as tempdir:
            watched_file = WatchedFile(pathlib.Path(tempdir, 'blah-must-not-exist'))
            self.assertIsNone(watched_file.fhandle)

    @mock.patch('restraint_wrap.watcher.Handler.rgx_lwd', new=re.compile(rb'Localwatchdog task: (.*?)$'))
    @mock.patch('upt.logger.LOGGER.error')
    def test_rgx_exceptions(self, mock_error):
        # pylint: disable=no-self-use
        """Test that .decode('utf-8') is in try/catch block for both regexes."""

        def write_content(watcher, tempdir):
            time.sleep(1)

            pathlib.Path(tempdir, 'recipes/1234/tasks/1/', 'harness.log').write_bytes(b'Localwatchdog task: ' +
                                                                                      bytes([0x7F, 0xFF, 0xFF, 0xFF]))
            pathlib.Path(tempdir, 'recipes/1234/tasks/1/', 'taskout.log').write_bytes(b'** ' +
                                                                                      bytes([0x7F, 0xFF, 0xFF, 0xFF])
                                                                                      + b' blah Score:100')

            time.sleep(0.2)

            watcher.observer.stop()

        do_test(write_content)

        mock_error.assert_any_call('** Failed to process subtask result!')
        mock_error.assert_any_call('** Failed to process LWD hit!')

    def test_watcher_watched_file_no_handle(self):
        # pylint: disable=no-self-use
        """Test that Watcher doesn't break if there's no handle."""

        class FakeEvent:
            # pylint: disable=too-few-public-methods
            """Fake of an event."""
            is_directory = False
            src_path = 'taskout.log'
            event_type = 'created'

        watcher = Watcher([ResourceGroup()])
        watcher.event_handler.watched_files.add(WatchedFile('/tmp/blah-must-not-exist'))
        watcher.event_handler.on_any_event(FakeEvent())
