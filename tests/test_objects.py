"""Test cases for provisioners/objects module."""
import os
import unittest

from plumbing.format import ProvisionData
from plumbing.objects import Host
from tests.const import ASSETS_DIR


class TestObjects(unittest.TestCase):
    """Test cases for objects module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')
        self.prov_data = ProvisionData.deserialize_file(self.req_asset)

    def test_hosts_method(self):
        """Ensure .hosts property returns all (3) hosts of our asset file."""
        hosts = self.prov_data.get_provisioner('beaker').rgs[0].recipeset.hosts
        self.assertIsInstance(hosts[0], Host)
        self.assertEqual(len(hosts), 3)

    def test_rg_hosts(self):
        """Ensure hosts works."""
        provisioners = [{'name': 'aws', 'rgs': [{'recipeset': {'hosts': [], 'restraint_xml': ''}}]}]
        tmp = ProvisionData({'provisioners': provisioners, 'instance_no': 1})
        aws = tmp.get_provisioner('aws')
        aws.rgs[0].recipeset.hosts.append(Host())
        aws.rgs[0].recipeset.hosts.append(Host())
        self.assertEqual(len(list(aws.hosts())), 2)
