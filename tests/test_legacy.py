"""Test cases for legacy module."""
import pathlib
import unittest
from unittest import mock

from cki_lib.misc import tempfile_from_string

from plumbing.objects import RecipeSet
from restraint_wrap.restraint_file.job_element import RestraintJob
from tests.const import ASSETS_DIR
from tests.utils import create_temporary_files
from upt.__main__ import main
from upt.cmd_legacy import adjust_job_element
from upt.cmd_legacy import convert_xml


class TestLegacy(unittest.TestCase):
    """Test cases for legacy module."""

    def setUp(self) -> None:
        self.xmlpath = pathlib.Path(ASSETS_DIR, 'beaker_xml')
        self.xml = self.xmlpath.read_text()

        self.mock_logger_add_fhandler = mock.patch('upt.__main__.logger_add_fhandler', mock.Mock())
        self.mock_logger_add_fhandler2 = mock.patch('upt.cmd_legacy.logger_add_fhandler', mock.Mock())

        self.mock_logger_add_fhandler.start()
        self.mock_logger_add_fhandler2.start()

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_convert_xml_no_exclude(self):
        """Ensure convert_xml works without excluding tests."""
        # Don't exclude any tasks from xml
        kwargs = {'excluded_hosts': '', 'force_host_duration': False, 'provisioner': 'beaker',
                  'priority': 'normal'}
        # Do the conversion`
        provision_data = convert_xml(self.xml, **kwargs)
        bkr = provision_data.get_provisioner('beaker')

        # Exclude file not used, task_with_fetch should be present
        self.assertIn('task id="1" name="task_with_fetch"', bkr.rgs[0].recipeset.restraint_xml)
        # task_without_fetch has no fetch element, so it is removed
        self.assertNotIn('task name="task_without_fetch"', bkr.rgs[0].recipeset.restraint_xml)

    @mock.patch('upt.logger.LOGGER.warning')
    @mock.patch('builtins.print', mock.Mock())
    def test_convert_xml(self, mock_err):
        # pylint: disable=too-many-locals
        """Ensure convert_xml works with excluding tests."""
        with (
                tempfile_from_string(b'hostname1') as excluded_hosts,
                tempfile_from_string(b'task_with_fetch\n') as exclude_file
        ):
            # Exclude 1 task from xml
            kwargs = {'exclude': exclude_file, 'force_host_duration': False,
                      'excluded_hosts': excluded_hosts, 'provisioner': 'beaker',
                      'priority': 'normal'}
            # Do the conversion
            provision_data = convert_xml(self.xml, **kwargs)

        bkr = provision_data.get_provisioner('beaker')

        # Check objects count. 3 recipeSets -> 3 resource_groups, 1 recipeSet each.
        self.assertEqual(len(bkr.rgs), 3)
        rg0_recipeset = bkr.rgs[0].recipeset
        rg1_recipeset = bkr.rgs[1].recipeset
        rg2_recipeset = bkr.rgs[2].recipeset
        self.assertIsInstance(rg0_recipeset, RecipeSet)
        self.assertIsInstance(rg1_recipeset, RecipeSet)
        self.assertIsInstance(rg2_recipeset, RecipeSet)
        # First rg: 2 hosts, second rg: 4 hosts, 3rd rg: 1 host
        rg0_hosts = rg0_recipeset.hosts
        rg1_hosts = rg1_recipeset.hosts
        rg2_hosts = rg2_recipeset.hosts
        self.assertEqual(len(rg0_hosts), 2)
        self.assertEqual(len(rg1_hosts), 4)
        self.assertEqual(len(rg2_hosts), 1)

        # Test recipe_fill split.
        rg0_host0, rg0_host1 = rg0_hosts
        rg1_host0, rg1_host1, rg1_host2, rg1_host3 = rg1_hosts

        # Task name from exclude file was deleted
        self.assertNotIn('task name="task_with_fetch"', rg0_recipeset.restraint_xml)

        self.assertIn('RHEL-4', rg0_host0.recipe_fill)
        self.assertIn('RHEL-5', rg0_host1.recipe_fill)
        self.assertIn('RHEL-6', rg1_host0.recipe_fill)
        self.assertIn('RHEL-7', rg1_host1.recipe_fill)
        self.assertIn('RHEL-8', rg1_host2.recipe_fill)
        self.assertIn('RHEL-9', rg1_host3.recipe_fill)

        for host in rg0_hosts + rg1_hosts:
            self.assertIn('<hostname op="!=" value="hostname1"></hostname>', host.recipe_fill)

        mock_err.assert_called()

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_convert_xml_no_exclude_capability(self):
        """Ensure convert_xml works if the provisioner can't exclude hosts."""
        with tempfile_from_string(b'hostname1') as excluded_hosts:
            kwargs = {'force_host_duration': False,
                      'excluded_hosts': excluded_hosts,
                      'provisioner': 'aws',
                      'priority': 'high'}
            provision_data = convert_xml(self.xml, **kwargs)

        aws = provision_data.get_provisioner('aws')

        rg0_recipeset = aws.rgs[0].recipeset
        rg1_recipeset = aws.rgs[1].recipeset
        self.assertIsInstance(rg0_recipeset, RecipeSet)
        self.assertIsInstance(rg1_recipeset, RecipeSet)

        rg0_hosts = rg0_recipeset.hosts
        rg1_hosts = rg1_recipeset.hosts
        for host in rg0_hosts + rg1_hosts:
            self.assertNotIn('<hostname op="!=" value="hostname1"/>', host.recipe_fill)

    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_convert_api(self):
        """Test that legacy convert works."""
        with (
                create_temporary_files(['rcfile', 'ignore']),
                mock.patch('sys.argv',
                           ['__main__.py', '--rc', 'rcfile', 'legacy', 'convert',
                            '-i', str(self.xmlpath), '-r', 'ignore'])
        ):
            main()

    def test_adjust_job_element(self):
        """Ensure adjust_job_element works."""
        # Test removing group.
        job = RestraintJob.create_from_string('<job group="cki" />')
        adjust_job_element(job, '')
        self.assertIsNone(job.group)

        # Test preserving group.
        job = RestraintJob.create_from_string('<job group="cki" />')
        adjust_job_element(job, None)
        self.assertEqual('cki', job.group)

        # Test setting group.
        job = RestraintJob.create_from_string('<job group="cki" />')
        adjust_job_element(job, 'whale')
        self.assertEqual('whale', job.group)
