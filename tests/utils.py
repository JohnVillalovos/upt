"""Utils class."""
from contextlib import contextmanager
import os
import pathlib
import shutil
import tempfile
import typing


def remove_spaces_and_new_lines(string: str) -> str:
    """Helpers to remove spaces and new lines from a string."""
    return string.replace('\n', '').replace(' ', '')


@contextmanager
def create_temporary_files(files: typing.Iterable[str],
                           directories: typing.Iterable[str] = None
                           ) -> typing.Generator:
    """Create empty temporary files and/or directories."""
    old_path = os.getcwd()
    temp_path = tempfile.mkdtemp()
    if directories is not None:
        for cur_dir in directories:
            path = pathlib.Path(temp_path, cur_dir)
            path.mkdir(parents=True, exist_ok=True)
    for file in files:
        path = pathlib.Path(temp_path, file)
        path.parent.mkdir(parents=True, exist_ok=True)
        path.touch()
    try:
        os.chdir(temp_path)
        yield temp_path
    finally:
        os.chdir(old_path)
        shutil.rmtree(temp_path)
