"""Test constants."""
import os

ASSETS_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets')

AWS_UPT_LAUNCH_TEMPLATE_NAME = 'arr-cki.staging.lt.upt'
AWS_UPT_INSTANCE_PREFIX = 'arr-cki.staging.i.upt'
