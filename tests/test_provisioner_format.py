"""Test cases for provisioner format module."""
import os
import unittest
from unittest import mock

from plumbing.format import ProvisionData
from tests.const import ASSETS_DIR
from tests.utils import create_temporary_files
from tests.utils import remove_spaces_and_new_lines


class TestProvisionerFormat(unittest.TestCase):
    """Test cases for executable module."""

    def setUp(self) -> None:
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.raw_data = {'provisioners': [{'name': 'beaker',
                                           'rgs': [
                                               {
                                                   'job': None,
                                                   'resource_id': 'J:1234',
                                                   'recipeset':
                                                   {'restraint_xml': '<xml />',
                                                    'hosts': [{'hostname': 'hostname1',
                                                               'recipe_id': 123,
                                                               'duration': 2880,
                                                               'recipe_fill': '<hostRequires force="abc" />'
                                                               }]
                                                    }
                                               }
                                           ]}],
                         'instance_no': 1}

    def test_derialize(self):
        """Ensure deserialization works."""
        prov_data = ProvisionData.deserialize_file(self.req_asset)
        recipeset0 = prov_data.get_provisioner('beaker').rgs[0].recipeset
        xml_content = """
        <job>
          <recipeSet>
            <recipe id="123" ks_meta="harness='restraint-rhtsbeakerlib'">
              <task name="abc" >
                <params>
                  <param name="CKI_NAME" value="abc"/>
                  <param name="CKI_UNIVERSAL_ID" value="abc"/>
                </params>
              </task>
              <task name="/distribution/check-install" role="STANDALONE" >
                <params>
                  <param name="CKI_NAME" value="/distribution/check-install"/>
                  <param name="CKI_UNIVERSAL_ID" value="/distribution/check-install"/>
                </params>
              </task>
            </recipe>
            <recipe id="456" ks_meta="harness='restraint-rhts beakerlib'">
              <task name="TestUsedToCheckTestPlanLogic1" role="STANDALONE" >
                <params>
                  <param name="CKI_NAME" value="TestUsedToCheckTestPlanLogic1"/>
                  <param name="CKI_UNIVERSAL_ID" value="TestUsedToCheckTestPlanLogic1"/>
                </params>
              </task>
            </recipe>
            <recipe id="789" ks_meta="harness='restraint-rhts beakerlib'">
              <task name="TestUsedToCheckTestPlanLogic2" role="STANDALONE" >
                <params>
                  <param name="CKI_NAME" value="TestUsedToCheckTestPlanLogic2"/>
                  <param name="CKI_UNIVERSAL_ID" value="TestUsedToCheckTestPlanLogic2"/>
                </params>
              </task>
            </recipe>
          </recipeSet>
        </job>
        """
        expected = remove_spaces_and_new_lines(xml_content)
        result = remove_spaces_and_new_lines(recipeset0.restraint_xml)
        self.assertEqual(expected, result)

        self.assertEqual('hostname1', recipeset0.hosts[0].hostname)
        self.assertEqual(123, recipeset0.hosts[0].recipe_id)
        self.assertEqual('<recipe ks_meta="re"><distroRequires><distro_name value="RHEL"/>'
                         '</distroRequires><hostRequires /></recipe>', recipeset0.hosts[0].recipe_fill)

    def test_serialize(self):
        """Ensure serialization works with dict."""
        with create_temporary_files(['fname']):
            ProvisionData(self.raw_data).serialize2file('fname')

    def test_provisioners_raise(self):
        """Ensure each provisioner has resource groups."""
        with self.assertRaises(RuntimeError):
            ProvisionData(self.raw_data).get_provisioner('invalid-provisioner')

    @mock.patch('plumbing.format.inspect.getmembers')
    def test_get_prov_class(self, mock_get_members):
        """Ensure get_prov_class catches provisioners with bad interface."""
        mock_get_members.return_value = [('breakit', object())]
        with self.assertRaises(RuntimeError):
            ProvisionData(self.raw_data).get_prov_class('beaker')
