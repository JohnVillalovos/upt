"""Test cases for restraint_wrap/runner module."""
import itertools
import json
import os
import pathlib
import unittest
from unittest import mock

from rcdefinition.rc_data import SKTData
from twisted.internet.error import ReactorNotRunning

from plumbing.format import ProvisionData
from plumbing.objects import Host
from restraint_wrap.restraint_protocol import RestraintClientProcessProtocol
from restraint_wrap.runner import Runner
from tests.const import ASSETS_DIR

# pylint: disable=no-self-use,too-many-public-methods


class TestRunner(unittest.TestCase):
    """Test cases for runner module."""

    def tearDown(self) -> None:
        self.mock_prov.stop()

    @mock.patch('restraint_wrap.restraint_protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        rc_data = SKTData()
        rc_data.state.kernel_version = '4.18.0-000.el8.test.dt2'
        self.std_kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                           'input': None, 'dump': True, 'rc_data': rc_data, 'upload': False}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.provision_data = ProvisionData.deserialize_file(self.req_asset)
        self.beaker = self.provision_data.get_provisioner('beaker')
        for host in self.beaker.rgs[0].recipeset.hosts:
            host.counter = mock.Mock()

        self.mock_prov = mock.patch('restraint_wrap.runner.ProvisionData.deserialize_file',
                                    lambda *x: self.provision_data)
        self.mock_prov.start()
        mock1 = mock.patch('restraint_wrap.testplan.LOGGER.printc', mock.Mock())
        mock1.start()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol')
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('builtins.print', mock.Mock())
    def test_init(self, mock_add_protocol):
        """Ensure runner creates protocols."""
        Runner([self.beaker], **self.std_kwargs)
        mock_add_protocol.assert_called()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_start_protocols(self, mock_debug):
        """Ensure start_protocols works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        mock1 = mock.MagicMock()
        mock1.proc = None

        mock2 = mock.MagicMock()

        mock3 = mock.MagicMock()
        mock3.proc = 'process-created'
        mock3.run_suffix = ''

        with mock.patch.object(runner, 'protocols', []):
            runner.start_protocols()
            mock_debug.assert_not_called()

        with mock.patch.object(runner, 'protocols', [mock1]):
            runner.start_protocols()
            mock_debug.assert_called_with('Starting protocol for resource_id %s...', mock1.resource_group.resource_id)
            mock1.start_all.assert_called()

        with mock.patch.object(runner, 'protocols', [mock2]):
            runner.start_protocols()
            mock2.start_all.assert_not_called()

        with mock.patch.object(runner, 'protocols', [mock3]):
            runner.start_protocols()
            mock3.start_all.assert_not_called()

    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.runner.reactor')
    @mock.patch('restraint_wrap.runner.Runner.start_protocols')
    def test_wait_on_protocols(self, mock_start_protocols, mock_reactor):
        """Ensure wait_on_protocols works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        mock_reactor.running = True
        with mock.patch('plumbing.interface.ProvisionerCore.all_recipes_finished', lambda *x: False):
            runner.wait_on_protocols()
            mock_reactor.callFromThread.assert_not_called()
            assert mock_start_protocols.call_count == 1

        with mock.patch('plumbing.interface.ProvisionerCore.all_recipes_finished', lambda *x: True):
            with mock.patch.object(runner, 'protocols', [None]):
                mock_reactor.running = False
                runner.wait_on_protocols()
                mock_reactor.callFromThread.assert_not_called()
                assert mock_start_protocols.call_count == 2

                mock_reactor.running = True
                runner.wait_on_protocols()
                assert mock_start_protocols.call_count == 3
                mock_reactor.callFromThread.assert_called()

                mock_reactor.callFromThread.side_effect = itertools.chain([ReactorNotRunning()],
                                                                          itertools.cycle([None]))
                runner.wait_on_protocols()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('restraint_wrap.runner.TestPlan', mock.Mock())
    @mock.patch('restraint_wrap.runner.reactor')
    @mock.patch('restraint_wrap.runner.task')
    @mock.patch('restraint_wrap.runner.Runner.download_logs')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_run(self, mock_debug, mock_download, mock_task, mock_reactor):
        """Ensure run works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        runner.run()

        mock_reactor.addSystemEventTrigger.assert_called_with('before', 'shutdown', runner.cleanup_handler)
        mock_debug.assert_called_with('Runner waiting for processes to finish...')
        mock_download.assert_called()

        mock_task.LoopingCall.assert_called_with(runner.wait_on_protocols)

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('restraint_wrap.runner.TestPlan', mock.Mock())
    @mock.patch('restraint_wrap.runner.reactor', mock.Mock())
    @mock.patch('restraint_wrap.runner.task', mock.Mock())
    @mock.patch('restraint_wrap.runner.Runner.download_logs')
    @mock.patch('restraint_wrap.runner.Runner.prepare_logs')
    @mock.patch('restraint_wrap.runner.Runner.update_kcidb_dumpfiles')
    @mock.patch('builtins.print', mock.Mock())
    def test_run_upload(self, mock_upload, mock_update, mock_download):
        """Ensure run calls prepare_logs and update_kcidb_dumpfiles on --upload."""
        kwargs = self.std_kwargs.copy()
        kwargs['upload'] = True
        runner = Runner([self.beaker], **kwargs)

        runner.run()

        mock_download.assert_called()
        mock_upload.assert_called()
        mock_update.assert_called()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.runner.Runner.download_logs')
    @mock.patch('restraint_wrap.runner.reactor', mock.Mock())
    @mock.patch('restraint_wrap.runner.task', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('builtins.print', mock.Mock())
    def test_run_adapter(self, mock_download):
        """Ensure run calls testplan dump method when adapter is used."""
        mock_download.return_value = []
        new_args = self.std_kwargs.copy()
        new_args['dump'] = False
        runner = Runner([self.beaker], **new_args)
        with mock.patch.object(runner.testplan, 'dump_testplan') as mock_testplan:
            runner.run()
            mock_testplan.assert_not_called()

        runner = Runner([self.beaker], **self.std_kwargs)
        with mock.patch.object(runner.testplan, 'dump_testplan') as mock_testplan:
            runner.run()
            mock_testplan.assert_called()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.error')
    def test_handle_runner_failure(self, mock_error):
        """Ensure handle_runner_failure works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        fail_mock = mock.Mock()
        runner.handle_runner_failure(fail_mock)

        mock_error.assert_called_with('FAILURE in deferred: %s', fail_mock)

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('builtins.print')
    def test_cleanup_handler(self, mock_print, mock_debug):
        # pylint: disable=no-member
        """Ensure cleanup_handler works."""
        runner = Runner([self.beaker], **self.std_kwargs)

        mock0 = mock.MagicMock()
        mock1 = mock.MagicMock()
        mock2 = mock.MagicMock()
        with mock.patch.object(runner, 'protocols', [mock0, mock1, mock2]):
            with mock.patch.object(self.beaker, 'release_resources'):
                mock0.proc = None

                mock1.proc.status = 'whatever'
                mock1.proc._getReason.return_value = 'ended'

                mock2.proc.status = 'whatever'
                mock2.proc._getReason.return_value = 'ended by signal 2'

                runner.cleanup_handler()

                mock_print.assert_any_call('* Abnormal exit!')
                mock_debug.assert_any_call('* Runner cleanup handler runs...')
                self.assertTrue(runner.cleanup_done)

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_pass(self, mock_print):
        """Ensure cleanup_handler works."""
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a', 'input': None,
                  'dump': False, 'rc_data': SKTData()}
        runner = Runner([self.beaker], **kwargs)

        mock0 = mock.MagicMock()
        mock0.kernel_failed_testing = False
        with mock.patch.object(runner.provisioners[0].rgs[0].recipeset.hosts[1], 'done_processing', True):
            with mock.patch.object(runner, 'protocols', [mock0]):
                runner.cleanup_handler()
                mock_print.assert_any_call('* Abnormal exit!')

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('builtins.print')
    def test_cleanup_handler_normal_exit(self, mock_print):
        """Ensure cleanup_handler works and that 'abnormal exit' warning isn't printed when all hosts finished."""
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a', 'input': None,
                  'dump': False, 'rc_data': None}

        mock0 = mock.MagicMock()
        mock0.kernel_failed_testing = False

        runner = Runner([self.beaker], **kwargs)

        host = Host()
        host.done_processing = True
        with mock.patch.object(runner.provisioners[0].rgs[0].recipeset, 'hosts', [host]):
            with mock.patch.object(runner, 'protocols', [mock0]):
                runner.cleanup_handler()

    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_done(self, mock_debug):
        """Ensure cleanup_handler works."""
        runner = Runner([self.beaker], **self.std_kwargs)
        runner.cleanup_done = True

        runner.cleanup_handler()

        mock_debug.assert_not_called()

    @mock.patch('restraint_wrap.runner.TestPlan.on_task_result')
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock())
    def test_on_task_result_pass_to_testplan(self, mock_on_task_result_testplan):
        """Ensure on_task_result call is passed to testplan."""
        runner = Runner([self.beaker], **self.std_kwargs)
        runner.on_task_result(1, 2, 3, 'w.json')

        mock_on_task_result_testplan.assert_called_with(1, 2, 3, 'w.json')

    @mock.patch('restraint_wrap.runner.TestPlan.create_testplan', mock.Mock())
    @mock.patch('restraint_wrap.testplan.KCIDBTestAdapter', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.ShellWrap', mock.Mock())
    @mock.patch('restraint_wrap.restraint_protocol.RestraintHost', mock.Mock())
    def test_add_protocol(self):
        """Ensure add_protocol works."""
        with mock.patch('restraint_wrap.runner.Runner.add_protocol', mock.Mock()):
            runner = Runner([self.beaker], **self.std_kwargs)

        with mock.patch.object(self.beaker, 'set_reservation_duration') as mock_reserve:
            mock_reserve.return_value = True
            result = runner.add_protocol(self.beaker, self.beaker.rgs[0], None, **self.std_kwargs)
            self.assertIsInstance(result, RestraintClientProcessProtocol)

            mock_reserve.return_value = False
            runner.add_protocol(self.beaker, self.beaker.rgs[0], None, **self.std_kwargs)

    def test_part_fdir(self):
        """Ensure part_fdir works."""
        self.assertEqual(('J:1234', '123', '1'),
                         Runner.part_fdir('LOGS_J:1234_0_R_123_T_1_test-redhat_a1'))

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('provisioners.beaker.Beaker.get_links2logs')
    @mock.patch('provisioners.beaker.pathlib.Path')
    @mock.patch('provisioners.beaker.sanitize_link')
    def test_download_logs(self, mock_sanitize, mock_path, mock_links, mock_debug):
        """Ensure download_logs works."""
        mock_links.return_value = ['url1', 'url2']

        Runner.download_logs([self.beaker, None], 'output', None)
        mock_sanitize.assert_any_call('url1', mock_path.return_value, None)
        mock_sanitize.assert_any_call('url2', mock_path.return_value, None)

        Runner.download_logs([self.beaker, None], 'output', '4.18.0-000.el8.test.dt2')
        mock_debug.assert_called_with('Downloading Beaker logs...')
        mock_sanitize.assert_any_call('url1', mock_path.return_value, '4.18.0-000.el8.test.dt2')
        mock_sanitize.assert_any_call('url2', mock_path.return_value, '4.18.0-000.el8.test.dt2')

    @mock.patch('restraint_wrap.runner.glob.glob')
    @mock.patch('restraint_wrap.runner.pathlib.Path')
    @mock.patch('cki_lib.kcidb.file.KCIDBFile.save')
    def test_update_kcidb_dumpfiles(self, mock_save, mock_path, mock_glob):
        """Ensure update_kcidb_dumpfiles works."""
        mock_glob.return_value = [pathlib.Path('test.json')]
        data = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [],
            'builds': [],
            'tests': [{
                'build_id': 'redhat:1234',
                'id': 'redhat:3',
                'origin': 'redhat',
                'output_files': [{'name': 'f1', 'url': 'https://file1'}]
            }]
        }
        mock_path.return_value.read_text.return_value = json.dumps(data, indent=4)
        mock_path.return_value.is_file.return_value = True
        mock_path.return_value.name = 'test.json'

        Runner.update_kcidb_dumpfiles('output', [('/1', {'name': 'f1', 'url': 'https://file1'})])
        mock_glob.assert_called_with('output/results_*/*.json')
        mock_save.assert_called()

    @mock.patch('restraint_wrap.runner.upload_file')
    @mock.patch('restraint_wrap.runner.pathlib.Path')
    def test_prepare_logs(self, mock_path, mock_upload_file):
        """Ensure prepare_logs works."""
        mock_adapter = mock.Mock()
        mock_adapter.artifacts_path = 'path/a'
        mock_adapter.output = 'path'
        mock_path_passed = mock.Mock()
        mock_path.return_value = 'aaa'
        Runner.prepare_logs(mock_adapter, [('results_0001', mock_path_passed)], upload=True)

        mock_upload_file.assert_called()
