"""Test cases for task_result module."""
import unittest

from plumbing.objects import Host
from restraint_wrap.restraint_file.task_element import RestraintTask
from restraint_wrap.task_result import TaskResult
from tests.utils import create_temporary_files


class TestTaskResult(unittest.TestCase):
    """Test cases for task_result module."""

    def setUp(self) -> None:
        self.restraint_task = RestraintTask.create_from_string(
            '<task id="1" name="a3" status="Completed" result="PASS"> <fetch url="git://"/>'
            '<params> <param name="CKI_MAINTAINERS" '
            'value="abc de &lt;abcde@redhat.com&gt; / abcde, aaa aaaaaaaa &lt;aaaaaaaa'
            '@redhat.com&gt; / aaaaaaaa, mmmmmm mmmmmmmm &lt;mmmmm@redhat.com&gt;"/> '
            '<param name="CKI_NAME" value="a3"/> </params></task>')
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        self.task_result = TaskResult(self.host, self.restraint_task, 1, 'Pass', 'Completed')
        self.host.task_results.append(self.task_result)

    def test_test_maintainers(self):
        """Ensure TaskResult is created OK and test_maintainers are parsed."""
        self.assertEqual(self.task_result.recipe_id, 1234)
        expected = [{'name': 'abc de', 'email': 'abcde@redhat.com', 'gitlab': 'abcde'},
                    {'name': 'aaa aaaaaaaa', 'email': 'aaaaaaaa@redhat.com', 'gitlab': 'aaaaaaaa'},
                    {'name': 'mmmmmm mmmmmmmm', 'email': 'mmmmm@redhat.com', 'gitlab': ''}]
        self.assertEqual(expected, self.task_result.test_maintainers)

    def test_output_files_from_path(self):
        """Ensure output_files_from_path can really find files in expected directories."""
        task_result = TaskResult(self.host, self.restraint_task, 1, 'PASS', 'Completed')

        path_prefix = 'run/run.done'
        full_path = f'{path_prefix}/recipes/{task_result.recipe_id}/tasks/{task_result.task_id}'
        mocked_logs = [f'{full_path}/file1', f'{full_path}/file2']

        expected = [
            {'name': 'file1', 'path': full_path},
            {'name': 'file2', 'path': full_path},
        ]

        with create_temporary_files(mocked_logs):
            result = task_result.output_files_from_path(path_prefix)

        self.assertCountEqual(expected, result)

    def test_is_cki_test_with_no_cki_test(self):
        """Ensure is_cki_test works when is not a cki test."""
        xml_content_without_anything = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_MAINTAINERS" value="abc de &lt;abcde@redhat.com&gt;"/>
            </params>
        </task>
        """

        xml_with_cki_name = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="a3"/>
            </params>
        </task>
        """

        xml_with_universal_id = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_UNIVERSAL_ID" value="1"/>
            </params>
        </task>
        """
        for xml_content in [xml_content_without_anything, xml_with_cki_name,
                            xml_with_universal_id]:
            restraint_task = RestraintTask.create_from_string(xml_content)
            task_result = TaskResult(self.host, restraint_task, 1, 'Pass', 'Completed')
            self.assertFalse(task_result.is_cki_test)

    def test_is_cki_test_with_a_cki_test(self):
        """Ensure is_cki_test works when is a cki test."""

        xml_content = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="a3"/>
              <param name="CKI_UNIVERSAL_ID" value="1"/>
            </params>
        </task>
        """

        restraint_task = RestraintTask.create_from_string(xml_content)
        task_result = TaskResult(self.host, restraint_task, 1, 'Pass', 'Completed')
        self.assertTrue(task_result.is_cki_test)

    def test_task_result_is_boot_task(self):
        """Ensure task is_boot_task works when is a boot test."""
        xml_with_universal_id = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_UNIVERSAL_ID" value="boot"/>
            </params>
        </task>
        """

        xml_with_task_name = """
        <task id="1" name="Boot Test" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="boot"/>
            </params>
        </task>
        """
        for xml_content in [xml_with_universal_id, xml_with_task_name]:
            restraint_task = RestraintTask.create_from_string(xml_content)
            task_result = TaskResult(self.host, restraint_task, 1, 'Pass', 'Completed')
            self.assertTrue(task_result.is_boot_task)

    def test_task_result_is_not_boot_task(self):
        """Ensure task is_boot_task works when is not a boot test."""
        xml_content = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="OTHER_VALUE" value="boot"/>
            </params>
        </task>
        """
        restraint_task = RestraintTask.create_from_string(xml_content)
        task_result = TaskResult(self.host, restraint_task, 1, 'Pass', 'Completed')
        self.assertFalse(task_result.is_boot_task)
