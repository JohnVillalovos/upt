# UPT

Unified Provisioning Tool

## Description

UPT is a Python-3 project for provisioning and testing in CKI Pipelines.

Beaker is quite closely tied with restraint. To run restraint, one needs to
know what tests to run, a "fake recipe id" and a hostname. The first two are no
issue. The fake recipe id is used to determine what host (identified by this
recipe id) will run what tests. However, with Beaker, we don't necessarily know
right away what hostname will be provisioned. Because of this, UPT waits until
the resource is provisioned and only then it can output restraint commands to
run tests.

Currently, a wrapper / test runner around `restraint` is part of this project.
It uses restraint standalone mode to run tests on a provisioned resource(s) We
realize it may need to be moved away. The design of the modules is made with
that in mind.

## Installation & dependencies

* The project needs `bkr` and `restraint` executables. Install the packages by:

```bash
dnf install -y beaker-client restraint-client`
```

* Optional: ensure your pip and setuptools are up-to-date:

```bash
pip3 install setuptools --upgrade --user
pip3 install pip --upgrade --user
```

* Install Python libraries & UPT:

```bash
$ cd upt && pip3 install --user --editable .`
```

* (`upt` and `rcclient` links are created automatically)

## Provisioners status:

* Beaker:
    * full support: consumes any Beaker XML job to provide provisioning and
      test running
* AWS:
    * machine configuration given by launch template, with limited override
      capabilities

You can consider provisioning a machine without UPT and using just the test
runner. Refer to the YAML input format further below.

## Usage and provisioner format

### Basic usage

* Drop `beaker.xml` file to `upt` directory.
* Run `upt legacy convert`. This will convert `beaker.xml` to `c_req.yaml`.
* Run `upt provision` with any extra options you may need.

If you wish to wait for all provisioning to complete before running tests, you
can wait for the provisioning to complete and run `rcclient test` afterwards.

If you wish to run testing right away as resources are available
(asynchronously from other resources in the request), you can use `--pipeline
"<rcclient options>"` with the `upt provision` command.

### Basic usage explained + provisioner format

Below is an example yaml file content (provisioning request) to provision 1
job/recipe/host and run 1 task.

```
!<ProvisionData>
provisioners:
  - !<AWS>
    rgs:
      - !<ResourceGroup>
        resource_id: J:1234
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: host_to_run_on
              recipe_id: 1
              recipe_fill: |-
                <recipe> ... </recipe>
              duration: 2880
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" job_id="1">
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
              </recipe>
             </recipeSet>
            </job>
        job: ''
    name: beaker
```

Here are some basic rules:
* A file may use/contain multiple provisioners.
* Each `ResourceGroup` object has 1 `RecipeSet`.
* There can be multiple `ResourceGroup` objects in the file.
* Each `RecipeSet` may have multiple hosts.
* Each `recipe_fill` explains how to provision one (1) host.
* Each `restraint_xml` explains what tests to run for one (1) `RecipeSet`.
* `recipeid` / `hostname` fields explicitly tell what tests to run on what
  host. See `id` attribute of a recipe in `resraint_xml`.
* Beaker only: `duration` specifies for how long (in seconds) will the host
  stay provisioned. Each time we start running tests on the host, we renew this
  duration, because we certainly don't want the machine to disappear in the
  middle of testing.

The `!<text>` labels are yaml labels for Python classes, so yaml library
automatically knows how to deserialize into objects.

Running `upt --help` or `rcclient --help` is your friend. The parameters should
be reasonably documented there. If not, then let's fix that.

### AWS provisioner

To use the AWS provisioner, you need to have an AWS account and your system has to
be configured to use it. This usually includes setting AWS access key id, AWS
secret access key and AWS region in config file. Refer to
https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/setup-credentials.html
for more info.

#### General setup

| Name                                 | Secret | Required | Description                                                    |
|--------------------------------------|--------|----------|----------------------------------------------------------------|
| `AWS_UPT_ACCESS_KEY_ID`              | no     | no       | Access key of the AWS service account used to create instances |
| `AWS_UPT_SECRET_ACCESS_KEY`          | yes    | no       | Secret key of the AWS service account used to create instances |
| `AWS_UPT_INSTANCE_PREFIX`            | no     | yes      | Prefix for the EC2 instance name                               |
| `AWS_UPT_LAUNCH_TEMPLATE_NAME`       | no     | yes      | Name of the launch template to use for creating instances      |

#### Statically overriding the AMI and instance type

The following variables allow to override the AMI and instance type from the
launch template.

| Name                    | Description                                       |
|-------------------------|---------------------------------------------------|
| `AWS_UPT_IMAGE_ID`      | Override the image ID in the launch template      |
| `AWS_UPT_INSTANCE_TYPE` | Override the instance type in the launch template |

#### Overriding the AMI and instance type

The following variables allow to dynamically determine the AMI and instance type.

| Name                                 | Description                                              |
|--------------------------------------|----------------------------------------------------------|
| `AWS_UPT_IMAGE_OWNER`                | Override the image ID with the latest matching image     |
| `AWS_UPT_IMAGE_ARCHITECTURE_FILTER`  | Override the image ID with the latest matching image     |
| `AWS_UPT_IMAGE_NAME_FILTER`          | Override the image ID with the latest matching image     |
| `AWS_UPT_IMAGE_ARCHITECTURE_MAPPING` | YAML dictionary with architecture→instance type mappings |

The latest matching image is used. To set the instance type depending on the
image, a mapping from architecture to instance type can be provided in
`AWS_UPT_IMAGE_ARCHITECTURE_MAPPING` (e.g. `{amd64: t2.medium, x86_64:
t4g.medium}`). If the architecture is found in the mapping, the instance type
is overridden.

#### Overriding the subnet

| Name                                 | Description                                                      |
|--------------------------------------|------------------------------------------------------------------|
| `AWS_UPT_NETWORK_SUBNET_ID`          | Override the network from the template                           |
| `AWS_UPT_NETWORK_SECURITY_GROUP_IDS` | Override the security groups from the template (space-delimited) |

When overriding the subnet, suitable security groups have to be provided as well.

#### Details

Otherwise, using AWS provisioner is very similar to using Beaker. The biggest
difference is that the node is named `aws`.

When an instance is provisioned, `misc` field and `instance_id` is created in
`Host` node. This allows UPT to use existing running instances using `upt
waiton` command, instead of provisioning them. This is useful especially during
development; you don't have to keep launching and terminating instances.

Notes:
* Currently, the provisioner provisions only `t2.micro` instances (hardcoded).
* `duration` and `resource_id` fields are currently ignored.

```xml
!<ProvisionData>
provisioners:
  - !<AWS>
    rgs:
      - !<ResourceGroup>
        resource_id: '1'
        ssh_opts: ''
        recipeset: !<RecipeSet>
          hosts:
            - !<Host>
              hostname: hostname
              recipe_id: 1
              recipe_fill:
              duration: 28800
              misc:
                instance_id: i-00000000000000001
          restraint_xml: |-
            <?xml version="1.0" encoding="utf-8"?>
            <job id="1">
             <recipeSet>
              <recipe id="1" >
               <task name="/distribution/command">
                <fetch url="https://github.com/beaker-project/beaker-core-tasks/archive/master.tar.gz#command"/>
                <params>
                 <param name="CKI_NAME" value="test1"/>
                 <param name="CMDS_TO_RUN" value="exit 0"/>
                </params>
               </task>
             </recipeSet>
            </job>
          id:
        job: <job group retention_tag="60days"><whiteboard>UPT@gitlab:123456 3.18@rhel x86_64</whiteboard></job>
    name: aws
```

### Notes

Currently, machine has 15 minutes to reboot. If it's longer, it's considered an
issue.

### Even more info

See `docs` directory, it contains useful info about some design decisions and
explains why UPT works the way it does.
