"""Provisioner interface and common methods."""

import time

from upt.logger import LOGGER
from upt.misc import RET


class ProvEndConds:
    # pylint: disable=too-many-instance-attributes,too-many-arguments
    """Implements provisioning timeouts and checks whether it's finished."""

    def __init__(self, resource_group, is_provisioned, reprovision_aborted, evaluate_wait_time,
                 evaluate_confirm_time, provisioning_watchdog_update_time):
        """Create the object."""
        # One of resource group of provisioning request
        self.resource_group = resource_group
        # Method to check if resource is provisioned
        self.is_provisioned = is_provisioned
        # Method to provision a resource again, if provisioning failed
        self.reprovision_aborted = reprovision_aborted

        # Time when we've started provisioning
        self.start = time.time()

        # initially, we assume no resources are provisioned; breaking execution
        # means we've failed to provision
        self.provisioned = [False]
        self.retcode = RET.PROVISIONING_FAILED

        # A list of methods to evaluate to see if we should end provisioning.
        self.end_conditions = [self.provisioning_success,
                               self.is_aborted_count_exceeded, self.timeout]
        self.evaluate_wait_time = evaluate_wait_time
        self.evaluate_confirm_time = evaluate_confirm_time
        self.provisioning_watchdog_update_time = provisioning_watchdog_update_time

        self.timeout_occurred = False

    def is_aborted_count_exceeded(self, **kwargs):
        """Check if too many resources "aborted" or "canceled"."""
        max_aborted_count = kwargs['max_aborted_count']

        return self.get_abort_count() >= max_aborted_count

    def get_abort_count(self):
        """Get count of hosts that "aborted" or "canceled"."""
        return len(self.resource_group.erred_rset_ids)

    @classmethod
    def _timeout_eval(cls, start_time, time_cap):
        """Evaluate if we've exceeeded time_cap minutes."""
        return int((time.time() - start_time) / 60.0) >= time_cap

    def timeout(self, **kwargs):
        """Check if any timeout occurred."""
        time_cap = kwargs['time_cap']
        if self.timeout_occurred:
            # Timeout is 'sticky' we don't reevaluate it once it happens.
            return True

        if time_cap and self._timeout_eval(self.start, time_cap):
            msg = f'* Provisioning canceled after {time_cap} minutes.'
            LOGGER.error(msg)
            self.timeout_occurred = True
            return True

        return False

    def provisioning_success(self, **kwargs):
        # pylint: disable=unused-argument
        """Check if a resource group is successfully provisioned.

        This method is evaluated first, so if all resources are successfully
        provisioned, then we will stop execution (break out of
        provisioning/timeout/max-abort check.
        """
        if not self.resource_group.provisioning_done:
            return False

        self.retcode = RET.PROVISIONING_PASSED

        return True

    def update_provisioning_state(self, force_recheck=False):
        """Update data for the purposes of tracking resources state."""
        if (not self.resource_group.provisioning_done or force_recheck) and self.is_provisioned(self.resource_group):
            msg = 'Checking provisioning status' if force_recheck else 'Provisioning done'
            print(f'{msg} for resource_group {self.resource_group.resource_id}')
            # We can leave this resource group alone now.
            self.resource_group.provisioning_done = True

    def evaluate(self, force_recheck, **kwargs):
        """Check if we're done provisioning and get return code."""
        # Always refresh info about resources
        self.update_provisioning_state(force_recheck=force_recheck)
        # Reprovision aborted recipes and mark them as such
        if not self.is_aborted_count_exceeded(**kwargs):
            self.reprovision_aborted(self.resource_group)
        else:
            self.retcode = RET.PROVISIONING_FAILED
            return True

        for condition in self.end_conditions:
            # Check conditions until we find one that ends loop
            if condition(**kwargs):
                return True

        return False
