"""Provisioner interface."""

from abc import ABCMeta
from abc import abstractmethod
from itertools import chain
from typing import List

from plumbing.objects import RecipeSet
from plumbing.objects import ResourceGroup
from plumbing.serializer import DeserializerBase
from upt.misc import Monotonic


class ProvisionerCore(DeserializerBase, metaclass=ABCMeta):
    # pylint: disable=too-many-instance-attributes
    """Provisioner interface."""

    rgs: List[ResourceGroup]
    name: str = None

    fields_to_serialize = ['name', 'rgs']

    def __init__(self, dict_data=None):
        """Construct instances after deserialization."""
        super().__init__(dict_data=dict_data)

        self.name = str(self.__class__.__name__).lower()
        # Initialize with a reasonable default, provisioners may override this.
        self.keycheck = 'no'
        self.host_recipe_id_monotonic = Monotonic()
        self.resource_id_monotonic = Monotonic()

        # How many seconds to wait between re-checking provisioning status to wait. Provisioners
        # may override this, but very few will or should.
        self.evaluate_wait_time = 60
        # How many seconds to wait after system has been provisioned to see if it will abort right
        # away.
        self.evaluate_confirm_time = 120
        # Each 10 minutes, set host watchdog to duration field of the host, ensuring that the
        # system will not abort because of a timeout during provisioning.
        self.provisioning_watchdog_update_time = 600

    @abstractmethod
    def provision(self, **kwargs):
        """Provision all hosts."""

    def provision_host(self, host):
        """Provision a single host."""

    @classmethod
    def set_reservation_duration(cls, host, strict_keycheck='no'):
        # pylint: disable=unused-argument
        """Override this to ensure host remains provisioned for expected duration.

        Returns: True on ssh connection problems, otherwise False.
        """
        return False

    @abstractmethod
    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""

    @abstractmethod
    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed."""

    def release_resources(self):
        """Release all resources."""
        for resource_group in self.rgs:
            self.release_rg(resource_group)

    def exclude_hosts(self, hostnames):
        """Ensure that we do not provision specific hosts."""

    @abstractmethod
    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""

    @abstractmethod
    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""

    @abstractmethod
    def heartbeat(self, resource_group, recipe_ids_dead):
        """Check if resource is OK (provisioned, not broken/aborted)."""

    @abstractmethod
    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning.

        This method is to be called at the end, when we know we've successfully
        provisioned resources we need. This method will skip resources that
        failed provisioning.
        """

    @classmethod
    def find_host_object(cls, rgs, recipe_id):
        """Find host by recipe_id."""
        result = cls.find_objects(rgs, lambda obj: obj if hasattr(obj, 'recipe_id') else None,
                                  filter_hosts=lambda host: int(host.recipe_id) == int(recipe_id))

        assert len(result) == 1, "Invalid number of results"

        return result[0]

    @classmethod
    def all_recipes_finished(cls, resource_groups):
        """Determine if all hosts are finished processing."""
        for host in cls.find_objects(resource_groups, lambda obj: obj if hasattr(obj, 'recipe_id') else None):
            if not host.done_processing:
                return False

        return True

    @classmethod
    def get_all_hosts(cls, resource_groups):
        """Return all hosts of resource_groups."""
        return list(chain(*cls.find_objects(resource_groups, lambda x: x.hosts if isinstance(x, RecipeSet) else None)))

    @staticmethod
    def single_filter(lst, filter_obj=None):
        """Return a list filtered using filter_obj or the full list."""
        return filter(filter_obj, lst) if filter_obj else lst

    @staticmethod
    def conditional_append(getter_func, src_obj, dest_obj):
        """Append result of getter_func(src_obj) to dest_obj if the result is not None."""
        result_of_get = getter_func(src_obj)
        if result_of_get is not None:
            dest_obj.append(result_of_get)

    @classmethod
    def find_objects(cls, resource_groups, getter_func, filter_rgs=None, filter_recipesets=None, filter_hosts=None):
        # pylint: disable=too-many-arguments
        """Iterate objects filtered using respective filters and return results matched by getter_func.

        This is meant to retrieve objects or their attributes from resource_groups. Resource_groups/recipesets/hosts
        are filtered using respective filters and getter_func is called for each single of these objects. If it
        evaluates non-None, the result is appended to the final result list.

        Arguments:
            resource_groups    - list of all resource_groups to iterate through
            getter_func        - function that is called in each loop on (resource_group/recipeset/host), returns what
                                 should be appended to results
            filter_rgs         - filter object to use on resource_groups
            filter_recipesets  - filter object to use on recipesets
            filter_hosts       - filter object to use on hosts

        Returns: all objects getter_func returned
        """
        results = []

        # Iterate through pre-filtered resource_groups
        for resource_group in cls.single_filter(resource_groups, filter_rgs):
            # Conditionally append resource_group to results, if getter_func evaluates non-None
            cls.conditional_append(getter_func, resource_group, dest_obj=results)

            # Iterate through pre-filtered resource_groups
            for recipeset in cls.single_filter([resource_group.recipeset], filter_recipesets):
                # Conditionally append recipeset to results, if getter_func evaluates non-None
                cls.conditional_append(getter_func, recipeset, dest_obj=results)

                # Iterate through pre-filtered hosts
                for host in cls.single_filter(recipeset.hosts, filter_hosts):
                    # Conditionally append host to results, if getter_func evaluates non-None
                    cls.conditional_append(getter_func, host, dest_obj=results)

        return results
