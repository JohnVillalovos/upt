"""Datastructure definition and validation for provisioner data."""
import importlib
import inspect
import pathlib

import ruamel.yaml

from plumbing.serializer import DeserializerBase
import provisioners
from upt.misc import RET


class ProvisionData:
    """Provisioner data interface."""

    def __init__(self, provisioners_dict=None):
        """Construct instances after deserialization."""
        self.provisioners = []
        # If there are multiple instance of UPT running at the same, this number has to be unique
        # to distinguish them to ensure correct behavior.
        self.instance_no = 1
        if provisioners_dict is not None:
            self.instance_no = provisioners_dict.get('instance_no', self.instance_no)
            for old_prov in provisioners_dict['provisioners']:
                clsobj = ProvisionData.get_prov_class(old_prov['name'])
                actual_obj = clsobj(old_prov)
                actual_obj.instance_no = self.instance_no
                self.provisioners.append(actual_obj)

    @classmethod
    def to_yaml(cls, representer, data):
        """Serialize yaml as a given class."""
        return representer.represent_mapping(cls.__name__,
                                             {'provisioners': tuple(data.provisioners),
                                              'instance_no': data.instance_no})

    @classmethod
    def get_prov_class(cls, name):
        """Return provisioner class like Beaker for 'beaker' name."""
        module = importlib.import_module(f'{provisioners.__name__}.{name}')
        for class_name, tmp_class in inspect.getmembers(module, inspect.isclass):
            if class_name.lower() == name:
                return tmp_class

        raise RuntimeError(f'cannot find class for provisioner {name}')

    def get_provisioner(self, name, create=False):
        """Return a specific provisioner from the list."""
        name = name.lower()
        for prov in self.provisioners:
            if prov.name == name and len(prov.rgs) != 0:
                return prov

        if create:
            new_obj = self.get_prov_class(name)()
            self.provisioners.append(new_obj)
            return new_obj

        raise RuntimeError(f'no provisioner "{name}" with implementation found!')

    @classmethod
    def prepare(cls, provs, add_protocol, on_task_result, **kwargs):
        """Deserialize yaml, create provisioners, prepare them for use and add their protocols."""
        for provisioner in provs:
            # Hack: override keycheck value. Constructor of provisioner objects doesn't allow us to pass params,
            # because it would complicate (de)serializing the object. Instead of doing that or completely revising
            # a working API, override this one value.
            provisioner.keycheck = kwargs.get('keycheck')

            for resource_group in provisioner.rgs:
                add_protocol(provisioner, resource_group, on_task_result, **kwargs)

        return provs

    @classmethod
    def _register_classes(cls, classes):
        """Register classes for yaml (de)serialization."""
        yaml = ruamel.yaml.YAML()
        yaml.preserve_quotes = True

        # Register subclasses of DeserializerBase, so ruaml knows what classes
        # to (de)serialize.
        for class_name in set(classes):
            yaml.register_class(class_name)

        return yaml

    def serialize2file(self, fpath):
        """Serialize ProvisionData instance to a file using yaml."""
        with open(fpath, 'w', encoding='utf-8') as fhandle:
            # Make sure serializer knows what classes to use, including dynamically imported
            # classes. Gah.

            yaml = self._register_classes(DeserializerBase.__subclasses__() + [self.__class__] +
                                          [prov.__class__ for prov in self.provisioners] + [RET])
            yaml.indent(mapping=2, sequence=4, offset=2)
            yaml.dump(self, fhandle)

    @classmethod
    def deserialize_file(cls, path):
        """Deserialize a file into a ProvisionData instance."""
        # make sure deserializer knows what classes to use
        yaml = cls._register_classes(DeserializerBase.__subclasses__() + [RET])
        return ProvisionData(yaml.load(pathlib.Path(path).read_bytes()))
